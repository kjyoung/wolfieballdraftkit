package wdk.file;

import wdk.data.DraftModel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import static wdk.WDK_StartupConstants.PATH_DRAFTS;
import wdk.data.DraftEvent;
import wdk.data.DraftModelDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.data.Team;

/**
 *
 * @author Kevin
 */
public class JsonDraftFileManager implements DraftFileManager {
    
    // .json file reading and writing constants
    String JSON_PITCHERS = "Pitchers";
    String JSON_HITTERS = "Hitters";
    String JSON_HITTER = "hitter";
    String JSON_FANTASY_TEAMS = "Fantasy_Teams";
    String JSON_FREE_AGENTS = "Free_Agents";
    String JSON_DRAFT_ORDER = "Draft_Order";
    String JSON_EXT = ".json";
    String SLASH = "/";
    String JSON_FANTASY_TEAM_NAME = "TEAM_NAME";
    String JSON_FANTASY_TEAM_OWNER = "TEAM_OWNER";
    String JSON_FANTASY_TEAM_PURSE = "PURSE";
    String JSON_FANTASY_TEAM_ROSTER = "ROSTER";
    String JSON_FANTASY_TEAM_TAXI_SQUAD = "TAXI_SQUAD";
    String JSON_DRAFT_EVENT_PICK_NUMBER = "PICK";
    String JSON_DRAFT_EVENT_CONTRACT = "CONTRACT";
    String JSON_DRAFT_EVENT_SALARY = "SALARY";
    String JSON_DRAFT_EVENT_FIRST_NAME = "FIRST";
    String JSON_DRAFT_EVENT_LAST_NAME = "LAST";
    String JSON_DRAFT_EVENT_TEAM_NAME = "TEAM";
    
    // All players have these attributes
    String JSON_PLAYER_IS_HITTER = "HITTER";
    String JSON_PLAYER_IS_PLACEHOLDER = "PLACEHOLDER";
    String JSON_PLAYER_ROLE = "ROLE";
    String JSON_PLAYER_POSITION = "POSITION";
    String JSON_PLAYER_PRO_TEAM = "TEAM";
    String JSON_PLAYER_LAST_NAME = "LAST_NAME";
    String JSON_PLAYER_FIRST_NAME = "FIRST_NAME";
    String JSON_PLAYER_H = "H";
    String JSON_PLAYER_NOTES = "NOTES";
    String JSON_PLAYER_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_PLAYER_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    String JSON_PLAYER_QUALIFIED_POS = "QP";
    
    // All drafted players have these attributes
    String JSON_PLAYER_CONTRACT = "CONTRACT";
    String JSON_PLAYER_ESTIMATED_VALUE = "ESTIMATED_VALUE";
    String JSON_PLAYER_SALARY = "SALARY";
    
    // Strictly Hitter attributes
    String JSON_HITTER_AB = "AB";
    String JSON_HITTER_R = "R";
    String JSON_HITTER_HR = "HR";
    String JSON_HITTER_RBI = "RBI";
    String JSON_HITTER_SB = "SB";
    
    // Strictly Pitcher attributes
    String JSON_PITCHER_IP = "IP";
    String JSON_PITCHER_ER = "ER";
    String JSON_PITCHER_W = "W";
    String JSON_PITCHER_SV ="SV";
    String JSON_PITCHER_BB = "BB";
    String JSON_PITCHER_K = "K";
    
    DraftModelDataManager dataManager;
    HashMap<String, Image> profileImagesMap;
     
    public JsonDraftFileManager (DraftModelDataManager dataManager,
                                HashMap<String, Image> profileImagesMap) {
        this.dataManager = dataManager;
        this.profileImagesMap = profileImagesMap;
    }
    
    @Override
    public void saveDraft(DraftModel draftToSave) throws IOException {
        
        // Build the file path
        String draftListing = draftToSave.getDraftName();
        String jsonFilePath = PATH_DRAFTS + SLASH + draftListing + JSON_EXT;
        
        // Init the writer
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);  
        
        // Make a json array for the fantasy teams array
        JsonArray fantasyTeamsJsonArray = makeFantasyTeamsJsonArray(draftToSave.getFantasyTeams());
        
        // Make a json array for the free agents array
        JsonArray freeAgentsJsonArray = makeFreeAgentsJsonArray(draftToSave.getPlayerPool());
        
        JsonArray draftOrderJsonArray = makeDraftOrderJsonArray(draftToSave.getDraftOrder());
        
        // Now build the draft using everything we just made
        JsonObject draftJsonObject = Json.createObjectBuilder()
                                    .add(JSON_FANTASY_TEAMS, fantasyTeamsJsonArray)
                                    .add(JSON_FREE_AGENTS, freeAgentsJsonArray)
                                    .add(JSON_DRAFT_ORDER, draftOrderJsonArray)
                                    .build();
        
        // And save everything at once
        jsonWriter.writeObject(draftJsonObject);
    }    
    
    /**
     * Loads the draftToLoad argument using the data found in the json file.
     * 
     * @param draftToLoad
     * @param jsonFilePath File containing the data to load.
     * 
     * @throws IOException Thrown when IO fails.
     */
    @Override
    public void loadDraft(DraftModel draftToLoad, String jsonFilePath) throws IOException {
        
        // Load the json file with all the data
        JsonObject json = loadJSONFile(jsonFilePath);
        
        // First we load the fantasy teams and their roster to our program
        draftToLoad.clearFantasyTeams();
        JsonArray jsonFantasyTeamsArray = json.getJsonArray(JSON_FANTASY_TEAMS);
        
        // Iterate through the array, grabbing each team and each team's roster
        for (int i = 0; i < jsonFantasyTeamsArray.size(); i++) {
            Team fantasyTeam = new Team();            
            JsonObject teamObj = jsonFantasyTeamsArray.getJsonObject(i);
            initFantasyTeamFromJsonFile(fantasyTeam, teamObj);
            JsonArray rosterArray = teamObj.getJsonArray(JSON_FANTASY_TEAM_ROSTER);
            JsonArray taxiSquadArray = teamObj.getJsonArray(JSON_FANTASY_TEAM_TAXI_SQUAD);
            
            // Iterate through each player of the roster and add it to this newly created
            // team's roster
            for (int j = 0; j < rosterArray.size(); j++) {                
                JsonObject playerObj = rosterArray.getJsonObject(j);
                String position = playerObj.getString(JSON_PLAYER_POSITION);
                
                // Determine if the player is a placeholder and/or a hitter
                boolean playerIsPlaceholder = playerObj.getBoolean(JSON_PLAYER_IS_PLACEHOLDER);
                boolean playerIsHitter = playerObj.getBoolean(JSON_PLAYER_IS_HITTER);
                
                // If the player is a hitter or a pitcher, add to the roster,
                // else if the player is a placeholder, we make sure to add it
                // as merely a placeholder
                if (playerIsPlaceholder) {
                    fantasyTeam.addPlayerPlaceholder(position, j);
                } else if (playerIsHitter) {
                    Hitter hitter = initHitterFromJsonFile(playerObj);
                    setDraftedPlayerStats(playerObj, hitter, position);                    
                    
                    fantasyTeam.decrementPurse(hitter.getSalary());
                    fantasyTeam.addToRoster(hitter);
                } else {
                    Pitcher pitcher = initPitcherFromJsonFile(playerObj);
                    setDraftedPlayerStats(playerObj, pitcher, position);
                    
                    fantasyTeam.decrementPurse(pitcher.getSalary());
                    fantasyTeam.addToRoster(pitcher); 
                }                
            }
            
            for (int k = 0; k < taxiSquadArray.size(); k++) {
                JsonObject playerObj = taxiSquadArray.getJsonObject(k);
                String position = playerObj.getString(JSON_PLAYER_POSITION);
                boolean playerIsHitter = playerObj.getBoolean(JSON_PLAYER_IS_HITTER);
                
                // If the player is a hitter or a pitcher, add to the roster,
                // else if the player is a placeholder, we make sure to add it
                // as merely a placeholder
                if (playerIsHitter) {
                    Hitter hitter = initHitterFromJsonFile(playerObj);
                    setDraftedPlayerStats(playerObj, hitter, position);                    
                    
                    fantasyTeam.addToTaxiSquad(hitter);
                } else {
                    Pitcher pitcher = initPitcherFromJsonFile(playerObj);
                    setDraftedPlayerStats(playerObj, pitcher, position);
                    
                    fantasyTeam.addToTaxiSquad(pitcher); 
                }           
                
            }
            
            // Finally add this team to our loaded draft
            draftToLoad.addFantasyTeam(fantasyTeam);
        }
        
        // Now we load the the free agents into our program
        draftToLoad.clearPlayerPool();
        
        JsonArray jsonPlayerPoolArray = json.getJsonArray(JSON_FREE_AGENTS);
        
        // Iterate through the array, grabbing each player and its data 
        for (int i = 0; i < jsonPlayerPoolArray.size(); i++) {
            JsonObject playerObj = jsonPlayerPoolArray.getJsonObject(i);
            boolean playerIsHitter = playerObj.getBoolean(JSON_PLAYER_IS_HITTER);
                    
            // A player is either a hitter or a pitcher, and we must know
            // in order to properly initialize the object
            if (playerIsHitter) {
                Hitter hitter = initHitterFromJsonFile(playerObj);        
                draftToLoad.getPlayerPool().add(hitter);
            } else {
                Pitcher pitcher = initPitcherFromJsonFile(playerObj);
                draftToLoad.getPlayerPool().add(pitcher);
            }         
        }
        
        // Finally we load the draft order
        draftToLoad.clearDraftOrder();
        
        JsonArray jsonDraftOrderArray = json.getJsonArray(JSON_DRAFT_ORDER);
        
        // Iterate through the array, grabbing each player and its data 
        for (int i = 0; i < jsonDraftOrderArray.size(); i++) {            
            JsonObject draftEventObj = jsonDraftOrderArray.getJsonObject(i);
            DraftEvent draftEvent = new DraftEvent();
            initDraftEventFromJsonFile(draftEvent, draftEventObj);
            draftToLoad.addToDraftOrder(draftEvent);   
            
            if (i == jsonDraftOrderArray.size() - 1) {
                draftToLoad.setPickCount(draftEvent.getPickNumber() + 1);
            }
        }
        
    }
    
    // Builds and returns a JsonArray containing all the fantasy teams for this draft
    public JsonArray makeFantasyTeamsJsonArray(List<Team> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Team team : data) {
           jsb.add(makeFantasyTeamJsonObject(team));
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    // Builds and returns a JsonArray containing all the free agents for this draft
    private JsonArray makeFreeAgentsJsonArray(List<Player> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        
        // A Player is either a hitter or a pitcher, and we must determine which
        // type the Player to properly initialize the object
        for (Player player : data) {
             if (player instanceof Hitter) {
                jsb.add(makeHitterJsonObject((Hitter)player));
            } else if (player instanceof Pitcher) {
                jsb.add(makePitcherJsonObject((Pitcher)player));
            }
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    // Builds and returns a JsonArray containing all the events of the draft
    // order
    private JsonArray makeDraftOrderJsonArray(List<DraftEvent> data) {
        System.out.println(data);
        JsonArrayBuilder jsb = Json.createArrayBuilder();        
        for (DraftEvent draftEvent : data) {
            jsb.add(makeDraftEventJsonObject(draftEvent));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    /**
     * Makes and returns a JsonObject for the provided fantasy team. A Team has 
     * a name, an owner, and a roster of players
     * 
     * @param team The fantasy team to save 
     * 
     * @return A JsonObject for team
     */
    private JsonObject makeDraftEventJsonObject(DraftEvent draftEvent) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_DRAFT_EVENT_PICK_NUMBER, draftEvent.getPickNumber())
                                                    .add(JSON_DRAFT_EVENT_FIRST_NAME, draftEvent.getFirstName())
                                                    .add(JSON_DRAFT_EVENT_LAST_NAME, draftEvent.getLastName())
                                                    .add(JSON_DRAFT_EVENT_TEAM_NAME, draftEvent.getTeamName())
                                                    .add(JSON_DRAFT_EVENT_CONTRACT, draftEvent.getContract() )
                                                    .add(JSON_DRAFT_EVENT_SALARY, draftEvent.getSalary())
                                                    .build();
        return jso;
    }
    
    private void initDraftEventFromJsonFile(DraftEvent draftEvent, JsonObject eventObj) {
        draftEvent.setPickNumber(eventObj.getInt(JSON_DRAFT_EVENT_PICK_NUMBER));
        draftEvent.setFirstName(eventObj.getString(JSON_DRAFT_EVENT_FIRST_NAME));
        draftEvent.setLastName(eventObj.getString(JSON_DRAFT_EVENT_LAST_NAME));
        draftEvent.setTeamName(eventObj.getString(JSON_DRAFT_EVENT_TEAM_NAME));
        draftEvent.setContract(eventObj.getString(JSON_DRAFT_EVENT_CONTRACT));
        draftEvent.setSalary(eventObj.getInt(JSON_DRAFT_EVENT_SALARY));
    }
    
    /**
     * Makes and returns a JsonObject for the provided fantasy team. A Team has 
     * a name, an owner, and a roster of players
     * 
     * @param team The fantasy team to save 
     * 
     * @return A JsonObject for team
     */
    private JsonObject makeFantasyTeamJsonObject(Team team) {
        JsonArray roster = makeRosterJsonArray(team.getRoster());
        JsonArray taxiSquad = makeTaxiSquadJsonArray(team.getTaxiSquad());
        JsonObject jso = Json.createObjectBuilder().add(JSON_FANTASY_TEAM_NAME, team.getName())
                                                    .add(JSON_FANTASY_TEAM_OWNER, team.getOwner())
                                                    .add(JSON_FANTASY_TEAM_PURSE, team.getPurse())
                                                    .add(JSON_FANTASY_TEAM_ROSTER, roster)
                                                    .add(JSON_FANTASY_TEAM_TAXI_SQUAD, taxiSquad)
                                                    .build();
        return jso;
    }
    
    // Makes and returns a JsonArray for the provided fantasy team roster
    private JsonArray makeTaxiSquadJsonArray(ObservableList<Player> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Player player : data) {
            if (player instanceof Hitter) {
                jsb.add(makeHitterJsonObject((Hitter)player));
            } else if (player instanceof Pitcher) {
                jsb.add(makePitcherJsonObject((Pitcher)player));
            }
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    // Makes and returns a JsonArray for the provided fantasy team roster
    private JsonArray makeRosterJsonArray(ObservableList<Player> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Player player : data) {
            if (player instanceof Hitter) {
                jsb.add(makeHitterJsonObject((Hitter)player));
            } else if (player instanceof Pitcher) {
                jsb.add(makePitcherJsonObject((Pitcher)player));
            }
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    // Makes and returns a json object for the provided hitter
    private JsonObject makeHitterJsonObject(Hitter hitter) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_PLAYER_IS_HITTER, true)
                                                    .add(JSON_PLAYER_IS_PLACEHOLDER, hitter.isPlaceholder())
                                                    .add(JSON_PLAYER_POSITION, hitter.getPosition())
                                                    .add(JSON_PLAYER_PRO_TEAM, hitter.getProTeam())
                                                    .add(JSON_PLAYER_LAST_NAME, hitter.getLastName())
                                                    .add(JSON_PLAYER_FIRST_NAME, hitter.getFirstName())
                                                    .add(JSON_PLAYER_QUALIFIED_POS, hitter.getQualifiedPositions())
                                                    .add(JSON_HITTER_AB, hitter.getAB())
                                                    .add(JSON_HITTER_R, hitter.getR())
                                                    .add(JSON_PLAYER_H, hitter.getH())
                                                    .add(JSON_HITTER_HR, hitter.getHR())
                                                    .add(JSON_HITTER_RBI, hitter.getRBI())
                                                    .add(JSON_HITTER_SB, hitter.getSB())
                                                    .add(JSON_PLAYER_NOTES, hitter.getNotes())
                                                    .add(JSON_PLAYER_YEAR_OF_BIRTH, hitter.getYearOfBirth())
                                                    .add(JSON_PLAYER_NATION_OF_BIRTH, hitter.getNationOfBirth())
                                                    .add(JSON_PLAYER_ESTIMATED_VALUE, hitter.getEstimatedValue())
                                                    .add(JSON_PLAYER_CONTRACT, hitter.getContract())
                                                    .add(JSON_PLAYER_SALARY, hitter.getSalary())
                                                    .build();
        
        return jso;
    }
    
    // Makes and returns a json object for the provided pitcher
    private JsonObject makePitcherJsonObject(Pitcher pitcher) {
        
        JsonObject jso = Json.createObjectBuilder().add(JSON_PLAYER_IS_HITTER, false)
                                                    .add(JSON_PLAYER_IS_PLACEHOLDER, pitcher.isPlaceholder())
                                                    .add(JSON_PLAYER_POSITION, pitcher.getPosition())
                                                    .add(JSON_PLAYER_PRO_TEAM, pitcher.getProTeam())
                                                    .add(JSON_PLAYER_LAST_NAME, pitcher.getLastName())
                                                    .add(JSON_PLAYER_FIRST_NAME, pitcher.getFirstName())
                                                    .add(JSON_PLAYER_QUALIFIED_POS, pitcher.getQualifiedPositions())
                                                    .add(JSON_PITCHER_IP, pitcher.getIP())
                                                    .add(JSON_PITCHER_ER, pitcher.getER())
                                                    .add(JSON_PITCHER_W, pitcher.getW())
                                                    .add(JSON_PITCHER_SV, pitcher.getSV())
                                                    .add(JSON_PLAYER_H, pitcher.getH())
                                                    .add(JSON_PITCHER_BB, pitcher.getBB())
                                                    .add(JSON_PITCHER_K, pitcher.getK())
                                                    .add(JSON_PLAYER_NOTES, pitcher.getNotes())
                                                    .add(JSON_PLAYER_YEAR_OF_BIRTH, pitcher.getYearOfBirth())
                                                    .add(JSON_PLAYER_NATION_OF_BIRTH, pitcher.getNationOfBirth())
                                                    .add(JSON_PLAYER_ESTIMATED_VALUE, pitcher.getEstimatedValue())
                                                    .add(JSON_PLAYER_CONTRACT, pitcher.getContract())
                                                    .add(JSON_PLAYER_SALARY, pitcher.getSalary())
                                                    .build();
        
        return jso;
    }
    
    private void initFantasyTeamFromJsonFile(Team fantasyTeam, JsonObject teamObj) {
        fantasyTeam.setName(teamObj.getString(JSON_FANTASY_TEAM_NAME));
        fantasyTeam.setOwner(teamObj.getString(JSON_FANTASY_TEAM_NAME));
    }
        
    private void setDraftedPlayerStats(JsonObject playerObj, Player player, String position) {
        player.setContract(playerObj.getString(JSON_PLAYER_CONTRACT));
        player.setEstimatedValue(playerObj.getInt(JSON_PLAYER_ESTIMATED_VALUE));
        player.setSalary(playerObj.getInt(JSON_PLAYER_SALARY));
        player.setPosition(position);
    }
        
    private Hitter initHitterFromJsonFile(JsonObject hitterObj) {
        int H = hitterObj.getInt(JSON_PLAYER_H);
        int AB = hitterObj.getInt(JSON_HITTER_AB);
        String qualifiedPositions = hitterObj.getString(JSON_PLAYER_QUALIFIED_POS);
        
        Hitter hitter = new Hitter( hitterObj.getString(JSON_PLAYER_PRO_TEAM),
                                    hitterObj.getString(JSON_PLAYER_LAST_NAME),
                                    hitterObj.getString(JSON_PLAYER_FIRST_NAME),
                                    H,
                                    hitterObj.getString(JSON_PLAYER_NOTES),
                                    hitterObj.getInt(JSON_PLAYER_YEAR_OF_BIRTH),
                                    hitterObj.getString(JSON_PLAYER_NATION_OF_BIRTH),
                                    AB,
                                    hitterObj.getInt(JSON_HITTER_R),
                                    hitterObj.getInt(JSON_HITTER_HR),
                                    hitterObj.getInt(JSON_HITTER_RBI),
                                    hitterObj.getInt(JSON_HITTER_SB));
                                                
        hitter.setBA(H, AB);
        hitter.setQualifiedPositions(qualifiedPositions);
        hitter.setQualifiedPositionsList(Arrays.asList(qualifiedPositions.split("_")));
        
        return hitter;
    }
     
    private Pitcher initPitcherFromJsonFile(JsonObject pitcherObj) {
        int W = pitcherObj.getInt(JSON_PITCHER_W);
        int H = pitcherObj.getInt(JSON_PLAYER_H);
        int ER = pitcherObj.getInt(JSON_PITCHER_ER);
        double IP = pitcherObj.getJsonNumber(JSON_PITCHER_IP).doubleValue();
            
        Pitcher pitcher = new Pitcher(  pitcherObj.getString(JSON_PLAYER_PRO_TEAM),
                                        pitcherObj.getString(JSON_PLAYER_LAST_NAME),
                                        pitcherObj.getString(JSON_PLAYER_FIRST_NAME), 
                                        H,
                                        pitcherObj.getString(JSON_PLAYER_NOTES),
                                        pitcherObj.getInt(JSON_PLAYER_YEAR_OF_BIRTH),
                                        pitcherObj.getString(JSON_PLAYER_NATION_OF_BIRTH),
                                        "P",
                                        IP,
                                        ER,
                                        pitcherObj.getInt(JSON_PITCHER_W),
                                        pitcherObj.getInt(JSON_PITCHER_SV),
                                        pitcherObj.getInt(JSON_PITCHER_BB),
                                        pitcherObj.getInt(JSON_PITCHER_K));

        pitcher.setWHIP(W, H, IP);
        pitcher.setERA(IP, ER);
        pitcher.setQualifiedPositionsList(Arrays.asList("P"));
        
        return pitcher;
    }

    @Override
    public void loadPitchersToDefaultPlayerPool(String pitchersFilePath) throws IOException {
        loadPitchersToDefaultPlayerPool(pitchersFilePath, JSON_PITCHERS);
    }
    
    @Override
    public void loadHittersToDefaultPlayerPool(String hittersFilePath) throws IOException {
        loadHittersToDefaultPlayerPool(hittersFilePath, JSON_HITTERS);
    }
    
    
    /**
     * Loads a Json file as a single object and returns it.
     * 
     * @param jsonFilePath The location of the json file to load
     * 
     * @return The json file as a single object
     * @throws IOException 
     */
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        
        InputStream is = new FileInputStream(jsonFilePath);
        
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        
        jsonReader.close();
        is.close();
        
        return json;
    }    
    
    /**
     * Loads from the file containing all the pitchers, and loads each one to 
     * the collection of players who have not been drafted to a fantasy team
     * yet
     * 
     * @param pitchersJsonFilePath The location of the json file containing
     *                            all the pitchers data
     * 
     * @param pitchersArrayName    The name of the array that the data will be 
     *                            extracted from
     * @throws IOException 
     */
    private void loadPitchersToDefaultPlayerPool(String pitchersJsonFilePath, String pitchersArrayName) throws IOException {
        
        JsonObject jsonObj = loadJSONFile(pitchersJsonFilePath);
        JsonArray jsonArray = jsonObj.getJsonArray(pitchersArrayName);    
        for (int i = 0; i < jsonArray.size(); i++) {            
            JsonObject pitcherObj = jsonArray.getJsonObject(i);
            
            int W = Integer.parseInt(pitcherObj.getString(JSON_PITCHER_W));
            int H = Integer.parseInt(pitcherObj.getString(JSON_PLAYER_H));
            int ER = Integer.parseInt(pitcherObj.getString(JSON_PITCHER_ER));
            double IP = Double.parseDouble(pitcherObj.getString(JSON_PITCHER_IP));
            
            Pitcher pitcher = new Pitcher(  pitcherObj.getString(JSON_PLAYER_PRO_TEAM),
                                            pitcherObj.getString(JSON_PLAYER_LAST_NAME),
                                            pitcherObj.getString(JSON_PLAYER_FIRST_NAME), 
                                            H,
                                            pitcherObj.getString(JSON_PLAYER_NOTES),
                                            Integer.parseInt(pitcherObj.getString(JSON_PLAYER_YEAR_OF_BIRTH)),
                                            pitcherObj.getString(JSON_PLAYER_NATION_OF_BIRTH),
                                            "P",
                                            IP,
                                            ER,
                                            Integer.parseInt(pitcherObj.getString(JSON_PITCHER_W)),
                                            Integer.parseInt(pitcherObj.getString(JSON_PITCHER_SV)),
                                            Integer.parseInt(pitcherObj.getString(JSON_PITCHER_BB)),
                                            Integer.parseInt(pitcherObj.getString(JSON_PITCHER_K)));
            
            pitcher.setWHIP(W, H, IP);
            pitcher.setERA(IP, ER);
            pitcher.setQualifiedPositionsList(Arrays.asList("P"));
           
           dataManager.getDraftModel().addPlayerToDefaultPool(pitcher);
        }
    }
    
    /**
     * Loads from the file containing all the hitters, and loads each one to 
     * the collection of players who have not been drafted to a fantasy team
     * yet
     * 
     * @param hittersJsonFilePath The location of the json file containing
     *                            all the hitters data
     * 
     * @param hittersArrayName    The name of the array that the data will be 
     *                            extracted from
     * @throws IOException 
     */
    private void loadHittersToDefaultPlayerPool(String hittersJsonFilePath, String hittersArrayName) throws IOException {
        
        JsonObject jsonObj = loadJSONFile(hittersJsonFilePath);
        JsonArray jsonArray = jsonObj.getJsonArray(hittersArrayName);        
        
        for (int i = 0; i < jsonArray.size(); i++) {  
            JsonObject hitterObj = jsonArray.getJsonObject(i);
            
            int H = Integer.parseInt(hitterObj.getString(JSON_PLAYER_H));
            int AB = Integer.parseInt(hitterObj.getString(JSON_HITTER_AB));
            
            String qualifiedPositions = hitterObj.getString(JSON_PLAYER_QUALIFIED_POS);
            Hitter hitter = new Hitter(     hitterObj.getString(JSON_PLAYER_PRO_TEAM),
                                            hitterObj.getString(JSON_PLAYER_LAST_NAME),
                                            hitterObj.getString(JSON_PLAYER_FIRST_NAME), 
                                            H,
                                            hitterObj.getString(JSON_PLAYER_NOTES),
                                            Integer.parseInt(hitterObj.getString(JSON_PLAYER_YEAR_OF_BIRTH)),
                                            hitterObj.getString(JSON_PLAYER_NATION_OF_BIRTH),
                                            AB,
                                            Integer.parseInt(hitterObj.getString(JSON_HITTER_R)),
                                            Integer.parseInt(hitterObj.getString(JSON_HITTER_HR)),
                                            Integer.parseInt(hitterObj.getString(JSON_HITTER_RBI)),
                                            Integer.parseInt(hitterObj.getString(JSON_HITTER_SB)));
            
            hitter.setBA(H, AB);
            
            // A CI is a 1B or a 3B
            if (qualifiedPositions.contains("1B") || qualifiedPositions.contains("3B")) {
                qualifiedPositions += "_CI";
            } 

            // A MI is a 2B or a SS
            if (qualifiedPositions.contains("2B") || qualifiedPositions.contains("SS")) {
                qualifiedPositions += "_MI";
            } 

            // Any hitter has the U position
            qualifiedPositions += "_U";            
            
            hitter.setQualifiedPositions(qualifiedPositions);
            hitter.setQualifiedPositionsList(Arrays.asList(qualifiedPositions.split("_")));
           
            dataManager.getDraftModel().addPlayerToDefaultPool(hitter);
        }
    }
}

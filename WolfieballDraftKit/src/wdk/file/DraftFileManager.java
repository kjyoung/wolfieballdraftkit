package wdk.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import wdk.data.DraftModel;

/**
 * This interface provides an abstraction of what a file manager should do. Note
 * that file managers know how to read and write drafts and player data
 * but not how to export sites.
 * 
 * @author Kevin
 */
public interface DraftFileManager {
    public void                 saveDraft(DraftModel draftToSave) throws IOException;
    public void                 loadDraft(DraftModel draftToLoad, String draftPath) throws IOException;
    public void                 loadPitchersToDefaultPlayerPool(String pitchersFilePath) throws IOException;
    public void                 loadHittersToDefaultPlayerPool(String hittersFilePath) throws IOException;
    
}


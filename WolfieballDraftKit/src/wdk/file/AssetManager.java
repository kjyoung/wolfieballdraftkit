package wdk.file;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javafx.scene.image.Image;
import org.apache.commons.io.FilenameUtils;

/**
 * All assets such as images will be loaded and maintained here.
 * 
 * @author Kevin
 */
public class AssetManager {
    
    // The player profile images
    HashMap<String, Image> profileImagesMap;
    String profileImageFilePath;
    
    // The images of the flags of a Player's nation of birth
    HashMap<String, Image> flagImagesMap;
    String flagImageFilePath;
    
    public AssetManager(String profileImageFilePath, String flagImageFilePath) {
        this.profileImageFilePath = profileImageFilePath;
        this.flagImageFilePath = flagImageFilePath;
        
        // Set the default images here. So if a player is added without a picture,
        // these default images will show whenever we wish to view them
        String defaultProfileImage = "defaultProfile.jpg";
        String defaultFlagImage = "defaultFlag.png";
        
        // Override the map get method to set the default player profile image
        profileImagesMap = new HashMap<String, Image>() {
            
            @Override
            public Image get(Object key) {
                Image img = super.get(key);
                if (img == null && key instanceof String)
                    super.put((String)key, img = new Image("file:" + profileImageFilePath + defaultProfileImage));
                return img;
            }
        };
        
        // Override the map get method to set the default flag image
        flagImagesMap = new HashMap<String, Image>() {
            
            @Override
            public Image get(Object key) {
                Image img = super.get(key);
                if (img == null && key instanceof String)
                    super.put((String)key, img = new Image("file:" + flagImageFilePath + defaultFlagImage));
                return img;
            }
        };
     }
    
    /**
     * Loads the hash map of the player profile images. The key will be the
     * image name without the extension (so it is advisable to name the images
     * in the format lastname_firstname.*) and the value will be the appropriate
     * image
     * 
     * Also note that if we change the format of the image names, we must also 
     * change the key in the Player class to reflect the change
     * 
     * @throws IOException 
     */
    public void loadPlayerProfileImages() throws IOException  {
        
        File dir = new File(profileImageFilePath);
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                String fileName = file.getName();
                String fileNameWithOutExt = FilenameUtils.removeExtension(fileName);
                profileImagesMap.put(fileNameWithOutExt, new Image("file:" + profileImageFilePath + fileName));
            }            
        }
    }
    
    /**
     * Loads the hash map of the flag images. The key will be the
     * image name without the extension (so it is advisable to name the images
     * in the format nation.*) and the value will be the appropriate
     * image
     * 
     * Also note that if we change the format of the image names, we must also 
     * change the key in the Player class to reflect the change
     * 
     * @throws IOException 
     */
    public void loadFlagImages() throws IOException {
        
        File dir = new File(flagImageFilePath);
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                String fileName = file.getName();
                String fileNameWithOutExt = FilenameUtils.removeExtension(fileName);
                flagImagesMap.put(fileNameWithOutExt, new Image("file:" + flagImageFilePath + fileName));
            }            
        }
    }
    
    public HashMap<String, Image> getProfileImagesMap() {
        return profileImagesMap;
    }
    
    public HashMap<String, Image> getFlagImagesMap() {
        return flagImagesMap;
    }
    
}

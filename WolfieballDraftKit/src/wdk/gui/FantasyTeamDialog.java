package wdk.gui;

import javafx.beans.binding.Bindings;
import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_GUI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.data.DraftModel;
import wdk.data.Team;

/**
 *
 * @author Kevin
 */
public class FantasyTeamDialog  extends Stage {
    
    // This is the object data behind this ui
    Team team;
    
    // GUI controls for our dialog
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    TextField nameTextField;
    Label ownerLabel;
    TextField ownerTextField;
    Button completeButton;
    Button cancelButton;
    
    // This is for keeping track of which button the user pressed
    String selection;
    
    String name;
    String owner;
    
    // Constants for our ui
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String NAME_PROMPT = "Name: ";
    public static final String OWNER_PROMPT = "Owner: ";
    public static final String FANTASY_TEAM_HEADING = "Fantasy Team Details";
    public static final String ADD_FANTASY_TEAM_TITLE = "Add New Fantasy Team";
    public static final String EDIT_FANTASY_TEAM_TITLE = "Edit Fantasy Team";
    
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     * @param draft
     * @param messageDialog
     */
    public FantasyTeamDialog(Stage primaryStage, DraftModel draft,  MessageDialog messageDialog) {       
        // Make this dialog modal, meaning others will wait
        // for it when it is displayed
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // First our container
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // Put the heading in the grid, note that the text will depend
        // on whether we're adding or editing
        headingLabel = new Label(FANTASY_TEAM_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // Now the name
        nameLabel = new Label(NAME_PROMPT);
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        nameTextField = new TextField();
        nameTextField.textProperty().addListener((observable, oldName, newName) -> {
            name = newName;
        });
        
        // And the owner
        ownerLabel = new Label(OWNER_PROMPT);
        ownerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        ownerTextField = new TextField();
        ownerTextField.textProperty().addListener((observable, oldOwner, newOwner) -> {
            owner = newOwner;
        });
        
        // And finally the buttons
        initButtons();        

        // Now let's arrange them all at once
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(nameLabel, 0, 1, 1, 1);
        gridPane.add(nameTextField, 1, 1, 1, 1);
        gridPane.add(ownerLabel, 0, 2, 1, 1);
        gridPane.add(ownerTextField, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);

        // And put the grid pane in the window
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    private void initButtons() {
        
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // Register event handlers for our buttons
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            this.selection = sourceButton.getText();
            this.hide();
        };
        
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        // The user can only complete the dialog when every field has been filled
        completeButton.disableProperty().bind(
            Bindings.isEmpty(nameTextField.textProperty())
            .or(Bindings.isEmpty(ownerTextField.textProperty())));
    }
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Team getTeam() { 
        return team;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     */
    public void showAddFantasyTeamDialog() {
        // Set the dialog title
        setTitle(ADD_FANTASY_TEAM_TITLE);
        
        // Reset the UI fields
        resetGUIData();
        
        // And open it up
        this.showAndWait();
    }
    
    private void resetGUIData() {        
        nameTextField.setText("");
        ownerTextField.setText("");
    }
    
    public String getTeamName() {
        return name;
    }
    
    public String getTeamOwner() {
        return owner;
    }
    
    public void loadGUIData(String name, String owner) {
        // Load the GUI stuff
        nameTextField.setText(name);
        ownerTextField.setText(owner);       
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditFantasyTeamDialog(Team teamToEdit) {
        // Set the dialog title
        setTitle(EDIT_FANTASY_TEAM_TITLE);
        
        // And then into our gui
        loadGUIData(teamToEdit.getName(), teamToEdit.getOwner());
               
        // And open it up
        this.showAndWait();
    }
}
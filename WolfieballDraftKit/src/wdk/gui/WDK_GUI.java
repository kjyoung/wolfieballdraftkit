package wdk.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import static wdk.WDK_StartupConstants.CLOSE_BUTTON_LABEL;
import static wdk.WDK_StartupConstants.PATH_CSS;
import static wdk.WDK_StartupConstants.PATH_IMAGES;
import wdk.controller.DraftEditController;
import wdk.controller.FileController;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.data.DraftModelDataView;
import wdk.file.AssetManager;
import wdk.file.DraftFileManager;

/**
 * This class provides the Graphical User Interface for this application,
 * managing all the UI components for editing a Draft and exporting it to a
 * site.
 *
 * @author Kevin
 */
public class WDK_GUI implements DraftModelDataView {
   
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_DIALOG_COMBO_BOX = "dialog_combo_box";
    static final String CLASS_SMALLER_HEADING_LABEL = "smaller_heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_TABLE = "table"; 
    static final String EMPTY_TEXT = "";
    
    // The text that the table columns display
    static final String COL_POSITION = "Position";
    static final String COL_FIRST_NAME = "First";
    static final String COL_LAST_NAME = "Last";
    static final String COL_PRO_TEAM = "Pro Team";
    static final String COL_POSITIONS = "Positions";
    static final String COL_R_W = "R/W";
    static final String COL_HR_SV = "HR/SV";
    static final String COL_RBI_K = "RBI/K";
    static final String COL_SB_ERA = "SB/ERA";
    static final String COL_BA_WHIP = "BA/WHIP";
    static final String COL_EST_VALUE = "Estimated Value";
    static final String COL_CONTRACT = "Contract";
    static final String COL_SALARY = "Salary";
    
    private boolean workspaceActivated;
    
    // This handles interactions with course info controls
    DraftEditController draftEditController;    
    
    // Controls how the program responds to user interactions with the
    // file toolbar 
    static FileController fileController;
    
    // The buttons that are part of the file toolbar
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exportSiteButton;
    Button exitButton;
    
    // Holds the contents of the current screen that the user selects 
    ScrollPane workspaceScrollPane;
    
    DraftModelDataManager dataManager;
    
    // Holds all the options of screens to select from
    FlowPane screenToolbarPane;
    
    // The buttons that are part of the screen toolbar
    Button fantasyTeamsScreenButton;
    Button playersScreenButton;
    Button fantasyStandingsScreenButton;
    Button draftScreenButton;
    Button MLBTeamsScreenButton;  
    
    // Holds all the options of file management such as save draft, load draft
    FlowPane fileToolbarPane;
    
    // The top and bottom workspace will always be a toolbar, it is only the
    // center workspace that changes. That's why a BorderPane makes the most sense 
    BorderPane wdkPane;
    
    BorderPane workspacePane;
    
    Scene primaryScene;
    Stage primaryStage;
    
    // Keep a list of all the buttons to iterate through, so when we switch
    // screens we can seamlessly disable the button just pressed, and enable
    // the rest of the buttons 
    List<Button> screenButtonList;
    
    // The screens that are part of this application
    FantasyTeamsScreen fantasyTeamsScreen;
    PlayersScreen playersScreen;
    FantasyStandingsScreen fantasyStandingsScreen;
    DraftScreen draftScreen;
    MLBTeamsScreen MLBTeamsScreen;    
    
    // The screen contents to be displayed 
    Pane fantasyTeamsScreenPane;
    Pane playersScreenPane;
    Pane fantasyStandingsScreenPane;
    Pane draftScreenPane;
    Pane MLBTeamsScreenPane;      
    
    String currentScreen;
    
    VBox topWorkspacePane;
    
    // The dialogs
    YesNoCancelDialog yesNoCancelDialog;    
    MessageDialog messageDialog;
    
    AssetManager assetManager;
    DraftFileManager draftFileManager;
    
    public WDK_GUI(Stage initPrimaryStage) {
        
        primaryStage = initPrimaryStage;
    }
    
    public FileController getFileController() {
        return fileController;
    }
    
    public void initGUI(String windowTitle, AssetManager assetManager) {        
        
        this.assetManager = assetManager;
        
        initDialogs();         
        
        initScreenToolbar();  
        
        initFileToolbar();        
        
        initEventHandlers();
                
        initWorkspace();
        
        initScreens();  
        
        initWindow(windowTitle);        
    }
    
    private void initDialogs() {        
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }
    
    private void initWorkspace() {
        
        // This holds all our workspace components, so now we must
        // add the components we've just initialized
        workspacePane = new BorderPane();
        
        // And now put it in the workspace
        workspaceScrollPane = new ScrollPane();
        
        workspaceScrollPane.setContent(workspacePane);
        workspaceScrollPane.setFitToWidth(true);

        // Note that we have not put the workspace into the window,
        // that will be done when the user either creates a new
        // draft or loads an existing one for editing
        workspaceActivated = false;        
    }
    
    
    private void initWindow(String windowTitle) {
        
        wdkPane = new BorderPane();
        
        // Add the file toolbar to the top of the screen
        wdkPane.setTop(fileToolbarPane);
        
        primaryScene = new Scene(wdkPane);
        
        // Set the window title
        primaryStage.setTitle(windowTitle);
        
        // Get the size of the screen
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // And use it to size the window
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        
        // Finally show this beautiful screen
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();        
    }
    
    /**
     * When this method is called, the center workspace will be shown.
     * 
     * @param draftToReload This may be a brand new DraftModel, 
     *                      or a previously edited one.
     */
    @Override
    public void reloadDraftModel(DraftModel draftToReload) {        
        if (!workspaceActivated) {
            activateWorkspace();
        }
        
        // Clear the GUI of the fantasy teams screen, just in case
        // the user was viewing a team that is not part of the loaded
        // draft. This will ensure no deleted data is visually shown
        fantasyTeamsScreen.resetGUIData();
    }
    
    private void initScreens() {
        
        // Pass all required data to the screens
        fantasyTeamsScreen = new FantasyTeamsScreen(this, primaryStage, dataManager, messageDialog, yesNoCancelDialog, draftEditController, assetManager);
        playersScreen = new PlayersScreen(this, primaryStage, dataManager, messageDialog, yesNoCancelDialog, draftEditController, assetManager);
        fantasyStandingsScreen = new FantasyStandingsScreen(dataManager);
        draftScreen = new DraftScreen(dataManager);  
        MLBTeamsScreen = new MLBTeamsScreen(dataManager);
        
        // Initialize the screen GUIs
        fantasyTeamsScreen.initGUI();
        playersScreen.initGUI();
        fantasyStandingsScreen.initGUI();
        draftScreen.initGUI();
        MLBTeamsScreen.initGUI();        
        
        // Save the panes so we can switch to each pane when appropriate
        fantasyTeamsScreenPane = fantasyTeamsScreen.getPane();
        playersScreenPane = playersScreen.getPane();
        fantasyStandingsScreenPane = fantasyStandingsScreen.getPane();
        draftScreenPane = draftScreen.getPane();
        MLBTeamsScreenPane = MLBTeamsScreen.getPane(); 
    }
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        
        fileToolbarPane = new FlowPane();
        
        // Add the buttons to the file toolbar, initially only having new draft, load draft, and
        // exit enabled because we can't save or export a draft not yet being edited 
        newDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.NEW_DRAFT_ICON, WDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.LOAD_DRAFT_ICON, WDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.SAVE_DRAFT_ICON, WDK_PropertyType.SAVE_DRAFT_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXPORT_SITE_ICON, WDK_PropertyType.EXPORT_SITE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false);        
    }
    
    /**
     * This function initializes all the buttons in the toolbar at the bottom of
     * the application window. These are related to screen selection.
     */
    private void initScreenToolbar() {
        
        screenToolbarPane = new FlowPane();
        
        // Add the buttons to the screen toolbar.
        // Notice the screen buttons are only enabled when a new draft is selected
        // or a draft is loaded. So they are disabled initially 
        fantasyTeamsScreenButton = initChildButton(screenToolbarPane, WDK_PropertyType.FANTASY_TEAM_SCREEN_ICON, WDK_PropertyType.FANTASY_TEAM_SCREEN_TOOLTIP, false);
        playersScreenButton = initChildButton(screenToolbarPane, WDK_PropertyType.PLAYER_SCREEN_ICON, WDK_PropertyType.PLAYERS_SCREEN_TOOLTIP, false);
        fantasyStandingsScreenButton = initChildButton(screenToolbarPane, WDK_PropertyType.FANTASY_STANDINGS_SCREEN_ICON, WDK_PropertyType.FANTASY_STANDINGS_SCREEN_TOOLTIP, false);
        draftScreenButton = initChildButton(screenToolbarPane, WDK_PropertyType.DRAFT_SCREEN_ICON, WDK_PropertyType.DRAFT_SCREEN_TOOLTIP, false);
        MLBTeamsScreenButton = initChildButton(screenToolbarPane, WDK_PropertyType.MLB_TEAMS_SCREEN_ICON, WDK_PropertyType.MLB_TEAMS_SCREEN_TOOLTIP, false); 
        
        // Add each screen button to a list so we can iterate through them easily when disabling/enabling them
        screenButtonList = new ArrayList<Button>();
        screenButtonList.add(fantasyTeamsScreenButton);
        screenButtonList.add(playersScreenButton);
        screenButtonList.add(fantasyStandingsScreenButton);
        screenButtonList.add(draftScreenButton);
        screenButtonList.add(MLBTeamsScreenButton);
    }  
    
    /**
     * 
     * @param toolbar
     * @param icon
     * @param tooltip
     * @param disabled
     * @return 
     */
    private Button initChildButton(Pane toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        
        Button toolbarButton = new Button();
        toolbarButton.setDisable(disabled);
        
        // Add an image 
        Image buttonImage = new Image(imagePath);        
        toolbarButton.setGraphic(new ImageView(buttonImage));
        
        // Add a tooltip 
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        toolbarButton.setTooltip(buttonTooltip);
        
        // Add the button to the toolbar
        toolbar.getChildren().add(toolbarButton);
        
        return toolbarButton;
    }
    
    private void initEventHandlers() {
        
        // First the file controls
        fileController = new FileController(yesNoCancelDialog, messageDialog, draftFileManager);
        
        newDraftButton.setOnAction(e -> 
            fileController.handleNewDraftRequest(this)
        );
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });
        saveDraftButton.setOnAction(e -> 
            fileController.handleSaveDraftRequest(this, dataManager.getDraftModel())
        );
        loadDraftButton.setOnAction(e ->
            fileController.handleLoadDraftRequest(this)
        );
        
        // Then the Draft editing controls
        draftEditController = new DraftEditController(dataManager);
        
        // ############ Now the screen controls #################
        
        // When a screen toolbar button is pressed, it changes the center workspace to a
        // new screen 
        fantasyTeamsScreenButton.setOnAction(e -> 
                changeScreen(fantasyTeamsScreenPane, fantasyTeamsScreenButton)
        ); 
        playersScreenButton.setOnAction(e -> 
                changeScreen(playersScreenPane, playersScreenButton)
        );
        fantasyStandingsScreenButton.setOnAction(e -> 
                changeScreen(fantasyStandingsScreenPane, fantasyStandingsScreenButton)
        );
        draftScreenButton.setOnAction(e -> 
                changeScreen(draftScreenPane, draftScreenButton)
        );
        MLBTeamsScreenButton.setOnAction(e -> 
                changeScreen(MLBTeamsScreenPane, MLBTeamsScreenButton)
        );
    }
    
    /**
     * When called this function puts the workspace into the window,
     * revealing the controls for editing a Draft.
     */
    public void activateWorkspace() {
        
        if (!workspaceActivated) {            
            // Our "home page" is the fantasy teams screen, so show that first
            workspaceScrollPane.setContent(fantasyTeamsScreenPane);
            updateScreenToolbar(fantasyTeamsScreenButton);
            
            // The center will hold all of our screens, which are contained in
            // a scroll pane
            wdkPane.setCenter(workspaceScrollPane);
            
            // Show the screen toolbar 
            wdkPane.setBottom(screenToolbarPane);
            
            workspaceActivated = true;
        }
    }
    
    public boolean isWorkspaceActivated() {
        return workspaceActivated;
    }
    
    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * The new, load, and exit buttons are never disabled so we never
     * touch them.
     * 
     * @param saved Describes whether the loaded Draft has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // This toggles with whether the current draft
        // has been saved or not
        saveDraftButton.setDisable(saved);
        
        // All the other buttons are always enabled
        // once editing that first Draft begins
        loadDraftButton.setDisable(false);
        exportSiteButton.setDisable(false);
    }
    
    /**
     * Change to the screen specified, and disable the button that was pressed
     * to change to that screen
     * 
     * @param screen The screen that will appear in the center workspace
     * @param screenButton The button pressed that brought us to the new screen
     */
    private void changeScreen(Pane screen, Button screenButton) {        
        updateScreenToolbar(screenButton);               
        workspaceScrollPane.setContent(screen); 
    }
    
    /**
     * Enable all the buttons of the screen toolbar except the one specified
     * to avoid reopening the screen that is already opened
     * 
     * @param screenButton The only button that is not enabled 
     */
    private void updateScreenToolbar(Button screenButton) {
        
        screenButton.setDisable(true);
        for (Button button : screenButtonList) {
            if (button != screenButton) {
                button.setDisable(false);
            }
        }                
    }

    public void setDataManager(DraftModelDataManager dataManager) {
        this.dataManager = dataManager;        
    }
    
    public DraftModelDataManager getDataManager() {
        return dataManager;
    }
    
    /**
     * Mutator method for the Draft file manager.
     *
     * @param initDraftFileManager The DraftFileManager to be used by this UI
     */
    public void setDraftFileManager(DraftFileManager initDraftFileManager) {
        draftFileManager = initDraftFileManager;
    }
    
    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }
}

package wdk.gui;

import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;

/**
 * This class serves to present custom text messages to the user when
 * events occur. Note that it always provides the same controls, a label
 * with a message, and a single OK button. The scheduled release of 
 * Java 8 version 40 in March 2015 will make this class irrelevant 
 * because the Alert class will do what this does and much more.
 * 
 * @author Kevin 
 */
public class MessageDialog extends Stage {
    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Button closeButton;
    
    /**
     * Initializes this dialog so that it can be used repeatedly
     * for all kinds of messages.
     * 
     * @param owner The owner stage of this modal dialog.
     * 
     * @param closeButtonText Text to appear on the close button.
     */
    public MessageDialog(Stage owner, String closeButtonText) {
        // Make it modal
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        // Label to display the custom message
        messageLabel = new Label();        

        // Close button on press and when the enter key is pressed
        closeButton = new Button(closeButtonText);
        closeButton.setOnAction(e -> { 
            closeDialog(); 
        });
        
        closeButton.setOnKeyReleased((KeyEvent key) -> {            
                if (key.getCode().equals(KeyCode.ENTER)) {
                    closeDialog();
                }            
        });

        // We'll put everything here
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(closeButton);
        
        // Make it look nice
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // And put it in the window
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }
 
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(String message, boolean automatedDraft) {
        if (automatedDraft) {
            return;
        }
        
        messageLabel.setText(message);
        this.showAndWait();
    }
    
    private void closeDialog() {
        MessageDialog.this.close();
    }
}
package wdk.gui;

import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.beans.binding.Bindings;
import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_GUI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.data.DraftModelDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.data.ProfessionalTeam;
import wdk.data.Team;

/**
 *
 * @author Kevin
 */
public class AddPlayerDialog  extends Stage {
    
    // These are the object data behind this ui
    Team team;
    Player player;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label firstNameLabel;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;
    Label birthYearLabel;
    TextField birthYearTextField;
    Label proTeamLabel;
    ComboBox<ProfessionalTeam> proTeamComboBox;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    String firstName;
    String lastName;
    ProfessionalTeam proTeam;
    List<String> qualifiedPositionsList;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FIRST_NAME_PROMPT = "First Name: ";
    public static final String LAST_NAME_PROMPT = "Last name: ";
    public static final String PRO_TEAM_PROMPT = "Pro Team: ";
    public static final String PLAYER_HEADING = "Player Details";
    public static final String ADD_SCHEDULE_ITEM_TITLE = "Add New Fantasy Team";
    public static final String EDIT_SCHEDULE_ITEM_TITLE = "Edit Fantasy Team";
    
    CheckBox cbC, cb1B, cb3B, cb2B, cbSS, cbOF, cbP;
    
    // The textual representation of the position radiobuttons
    String CB_C = "C";
    String CB_1B = "1B";
    String CB_3B = "3B";
    String CB_2B = "2B";
    String CB_SS = "SS";
    String CB_OF = "OF";
    String CB_P = "P";
    
    List<CheckBox> positionCheckBoxes;
    String[] availablePositions;
    HBox paneForCheckBoxes;
    
    int qualifiedPositionCount; 
    
    MessageDialog messageDialog;
    
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     * @param messageDialog
     * @param dataManager
     */
    public AddPlayerDialog(Stage primaryStage, MessageDialog messageDialog,
                           DraftModelDataManager dataManager) {  
        
        this.messageDialog = messageDialog;
        
        // This way if the user exits without clicking
        // the complete or cancel button, we'll pretend they hit cancel
        this.selection = CANCEL; 
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // Put the heading in the grid
        headingLabel = new Label(PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // Now the first name
        firstNameLabel = new Label(FIRST_NAME_PROMPT);
        firstNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        firstNameTextField = new TextField();
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            firstName = newValue;
        });
        
        // And the last name
        lastNameLabel = new Label(LAST_NAME_PROMPT);
        lastNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lastNameTextField = new TextField();
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            lastName = newValue;
        });
        
        // The Pro Team this player will be drafted to
        proTeamLabel = new Label(PRO_TEAM_PROMPT);
        proTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        proTeamComboBox = new ComboBox<ProfessionalTeam>();
        proTeamComboBox.setItems(dataManager.getDraftModel().getProTeams());
        proTeamComboBox.valueProperty().addListener((ov, oldProTeam, newProTeam) -> {
            proTeam = newProTeam;
        });
        
        initPositionCheckBoxes();
        
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // The user can only complete the dialog when every field has been filled
        // Exclude the position check box, that will be handled with a message
        // so the user can get better feedback on their invalid choice
        completeButton.disableProperty().bind(
            Bindings.isEmpty(firstNameTextField.textProperty())
            .or(Bindings.isEmpty(lastNameTextField.textProperty())
            .or(Bindings.isNull(proTeamComboBox.valueProperty()))));
        
        // Register event handlers for complete button
        EventHandler completeHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> { 
            player = initNewPlayer();
            Button sourceButton = (Button)ae.getSource();
            this.selection = sourceButton.getText();
            this.hide();
        };
        
        // Register event handlers for cancel button
        EventHandler cancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            this.selection = sourceButton.getText();
            this.hide();
        };
        
        completeButton.setOnAction(completeHandler);
        cancelButton.setOnAction(cancelHandler);

        // Now let's arrange them all at once
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(firstNameLabel, 0, 1, 1, 1);
        gridPane.add(firstNameTextField, 1, 1, 1, 1);
        gridPane.add(lastNameLabel, 0, 2, 1, 1);
        gridPane.add(lastNameTextField, 1, 2, 1, 1);
        gridPane.add(proTeamLabel, 0, 3, 1, 1);
        gridPane.add(proTeamComboBox, 1, 3, 1, 1);
        gridPane.add(paneForCheckBoxes, 0, 4, 1, 1);
        gridPane.add(completeButton, 0, 5, 1, 1);
        gridPane.add(cancelButton, 1, 5, 1, 1);
        
        GridPane.setMargin(completeButton, new Insets(50, 0, 0, 0));
        GridPane.setMargin(cancelButton, new Insets(50, 0, 0, 0));

        // And put the grid pane in the window
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * We will handle all potential errors here (i.e. user did not fill in 
     * the first name field)
     * 
     * @return Either a Hitter or Pitcher, or null if we encountered an error     
     */
    private Player initNewPlayer() {
        
        fillQualifiedPositionsList();
        
        if (qualifiedPositionCount == 0) {
            messageDialog.show("Must choose atleast one position", false);
            return null;
        }
        
        if (cbP.isSelected()) {
            if (qualifiedPositionCount > 1) {
                messageDialog.show("Invalid position choices. Player cannot be a pitcher and a hitter", false);
                return null;           
            }
            Pitcher pitcher = new Pitcher(proTeam.toString(), lastName, firstName, qualifiedPositionsList);
            pitcher.setQualifiedPositions(StringUtils.join(qualifiedPositionsList, "_"));
            return pitcher;
        } else {
            Hitter hitter = new Hitter(proTeam.toString(), lastName, firstName, qualifiedPositionsList);
            //hitter.setBA(H, AB);
            
            // A CI is a 1B or a 3B
            if (qualifiedPositionsList.contains("1B") || qualifiedPositionsList.contains("3B")) {
                qualifiedPositionsList.add("CI");
            } 

            // A MI is a 2B or a SS
            if (qualifiedPositionsList.contains("2B") || qualifiedPositionsList.contains("SS")) {
                qualifiedPositionsList.add("MI");
            } 

            // Any hitter has the U position
            qualifiedPositionsList.add("U");
            hitter.setQualifiedPositions(StringUtils.join(qualifiedPositionsList, "_"));
            return hitter;
        }
    }
    
    /**
     * Fill the list of qualified positions of the new player based on
     * which check boxes have been selected by the user
     */
    private void fillQualifiedPositionsList() {
        
        qualifiedPositionsList = new ArrayList<String>();
        
        qualifiedPositionCount = 0;
        for (CheckBox cb: positionCheckBoxes) {
            if (cb.isSelected()) {
                qualifiedPositionCount++;
                qualifiedPositionsList.add(cb.getText());
            }
        }
    }
        
    private void initPositionCheckBoxes() {
        
        paneForCheckBoxes = new HBox();
        positionCheckBoxes = new ArrayList<CheckBox>();
        
        // Initialize all the radio buttons
        cbC = initChildCheckBox(paneForCheckBoxes, CB_C);
        cb1B = initChildCheckBox(paneForCheckBoxes, CB_1B);
        cb3B = initChildCheckBox(paneForCheckBoxes, CB_3B);
        cb2B = initChildCheckBox(paneForCheckBoxes, CB_2B);
        cbSS  = initChildCheckBox(paneForCheckBoxes, CB_SS);
        cbOF = initChildCheckBox(paneForCheckBoxes, CB_OF);
        cbP = initChildCheckBox(paneForCheckBoxes, CB_P); 
        
        HBox.setMargin(cbC, new Insets(0, 10, 0, 0));
        HBox.setMargin(cb1B, new Insets(0, 10, 0, 0));
        HBox.setMargin(cb3B, new Insets(0, 10, 0, 0));
        HBox.setMargin(cb2B, new Insets(0, 10, 0, 0));
        HBox.setMargin(cbSS, new Insets(0, 10, 0, 0));
        HBox.setMargin(cbOF, new Insets(0, 10, 0, 0));
        HBox.setMargin(cbP, new Insets(0, 10, 0, 0));
        
        positionCheckBoxes.addAll(Arrays.asList(cbC, cb1B, cb3B, cb2B, cbSS, cbOF, cbP));  
    }
    
    private CheckBox initChildCheckBox(Pane pane, String text) {
        CheckBox cb = new CheckBox(text);
        pane.getChildren().add(cb);
        return cb;
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Team getTeam() { 
        return team;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     */
    public void showAddPlayerDialog() {
        // Set the dialog title
        setTitle(ADD_SCHEDULE_ITEM_TITLE);
        resetGUIData();
        
        // And open it up
        this.showAndWait();
    }
    
    private void resetGUIData() {
        firstNameTextField.setText("");
        lastNameTextField.setText("");
        proTeamComboBox.valueProperty().set(null);
        
        // Reset the check boxes to unselected
        for (CheckBox cb : positionCheckBoxes) {
            if (cb.isSelected()) {
                cb.setSelected(false);
            }
        }
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
}
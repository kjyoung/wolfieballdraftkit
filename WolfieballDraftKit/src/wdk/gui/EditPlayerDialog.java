package wdk.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import static wdk.gui.WDK_GUI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_GUI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_GUI.PRIMARY_STYLE_SHEET;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.data.Player;
import wdk.data.Team;
import wdk.file.AssetManager;
import static wdk.gui.WDK_GUI.CLASS_SMALLER_HEADING_LABEL;

/**
 *
 * @author Kevin
 */
public class EditPlayerDialog  extends Stage {
    
    // These are the object data behind this ui
    Player playerToEdit;
    
    // Gui controls for our dialog
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    Label positionsLabel;
    Label fantasyTeamLabel;
    ComboBox<Team> fantasyTeamComboBox;
    Label posLabel;
    ComboBox<String> posComboBox;
    Label contractLabel;
    ComboBox<String> contractComboBox;
    Label salaryLabel;
    TextField salaryTextField;
    Button addToPoolButton;
    Button completeButton;
    Button cancelButton;
    
    // This is for keeping track of which button the user pressed
    String selection;
    
    Player player;
    String firstName;
    String lastName;
    String proTeam;
    List<String> qualifiedPositionsList;
    
    // Constants for our ui
    public static final String FREE_AGENT = "Free agent";
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FANTASY_TEAM_LABEL = "Fantasy Team: ";
    public static final String POSITION_LABEL = "Position: ";
    public static final String CONTRACT_LABEL = "Contract: ";
    public static final String SALARY_PROMPT = "Salary: ";
    public static final String PLAYER_HEADING = "Player Details";
    public static final String EDIT_PLAYER_TITLE = "Edit Player";
    
    Team previousFantasyTeam;
    Team newFantasyTeam;
    String position;
    String contract;
    String salary;
    
    MessageDialog messageDialog;
    
    // This will keep all the positions that the player is qualified for, which
    // the selected team (via the combo box) still has room for. This is so
    // a user is forced to select the position of the player to be a position
    // the player is qualified for, and that the team still needs
    FilteredList<String> possiblePlayerPositions;
    
    AssetManager assetManager;
    ImageView profileImageView;
    ImageView flagImageView;
    
    // This list will contain the list of positions to show in the position
    // combo box
    List<String> positionsToDisplay;
    
    DraftModel draft;
    
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     * @param messageDialog
     * @param dataManager
     * @param assetManager
     */
    public EditPlayerDialog(Stage primaryStage, MessageDialog messageDialog,
                           DraftModelDataManager dataManager, AssetManager assetManager) {  
        
        // Initially add every fantasy team position to the list of this player's possible
        // positions
        possiblePlayerPositions = new FilteredList<String>(FXCollections.observableArrayList(DraftModel.getFantasyTeamPositions()), 
                                        pos -> true);
        
        positionsToDisplay = new ArrayList<String>();
        
        this.assetManager = assetManager;
        this.messageDialog = messageDialog;
        this.draft = dataManager.getDraftModel();
        
        // This way if the user exits without clicking
        // the complete or cancel button, we'll pretend they hit cancel
        this.selection = CANCEL; 
        
        // Make this dialog modal, so others will wait
        // for it when it is displayed
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // First our container
        int prefWidth = 200;
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setMinWidth(prefWidth);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // The heading
        headingLabel = new Label(PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        // These will hold our images
        profileImageView = new ImageView();
        flagImageView = new ImageView();
        
        // The label that displays the player's full name
        nameLabel = new Label("");
        nameLabel.getStyleClass().add(CLASS_SMALLER_HEADING_LABEL);
        
        // The label that displays the player's qualified positions
        positionsLabel = new Label("");
        positionsLabel.setStyle("-fx-font-size: 12pt; -fx-font-weight: bold;");   
        
        VBox playerBox = new VBox();
        playerBox.getChildren().addAll(flagImageView, nameLabel, positionsLabel);
        
        VBox.setMargin(nameLabel, new Insets(5, 0, 25, 0));
        
        // The fantasy team selection
        fantasyTeamLabel = new Label(FANTASY_TEAM_LABEL);
        fantasyTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fantasyTeamComboBox = initComboBox(false, prefWidth);
        
        // Fill the team combo box will all the fantasy teams in the draft,
        // and only enable it when there is atleast one fantasy team created
        fantasyTeamComboBox.setItems(dataManager.getDraftModel().getFantasyTeams());
        fantasyTeamComboBox.disableProperty().bind(
            Bindings.isEmpty(dataManager.getDraftModel().getFantasyTeams()));
        
        // The player's position of the respective fantasy team
        posLabel = new Label(POSITION_LABEL);
        posLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        posComboBox = initComboBox(true, prefWidth);
        posComboBox.setItems(possiblePlayerPositions); 
              
        // Edit the player's contract on the fantasy team.
        contractLabel = new Label(CONTRACT_LABEL);
        contractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        contractComboBox = initComboBox(true, prefWidth);
        
        // Edit the player's salary
        salaryLabel = new Label(SALARY_PROMPT);
        salaryLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        salaryTextField = new TextField();
        
        // Initialize the event handlers
        initEventHandlers();
        
        // Now initialize the buttons
        initButtons();        
        
        // Now let's arrange them all at once
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(profileImageView, 0, 1, 1, 1);
        gridPane.add(playerBox, 1, 1, 1, 1);
        gridPane.add(fantasyTeamLabel, 0, 2, 1, 1);
        gridPane.add(fantasyTeamComboBox, 1, 2, 1, 1);
        gridPane.add(posLabel, 0, 3, 1, 1);
        gridPane.add(posComboBox, 1, 3, 1, 1);
        gridPane.add(contractLabel, 0, 4, 1, 1);
        gridPane.add(contractComboBox, 1, 4, 1, 1);
        gridPane.add(salaryLabel, 0, 5, 1, 1);
        gridPane.add(salaryTextField, 1, 5, 1, 1);
        gridPane.add(addToPoolButton, 0, 6, 1, 1);
        gridPane.add(completeButton, 1, 6, 1, 1);
        gridPane.add(cancelButton, 2, 6, 1, 1);                       
        
        this.setResizable(false);
        
        // And put the grid pane in the window
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    private void initEventHandlers() {        
        
        // When a fantasy team is selected, we must fill the other fields with
        // appropriate values as to not even give the user an option to select
        // an invalid choice for any other fields (i.e. we don't want the user
        // to try to add a pitcher to a team when that team needs no more pitchers
        fantasyTeamComboBox.valueProperty().addListener((ov, oldTeam, newTeam) -> {
            
            // When the UI fields are reset, the current team is null. So check
            // for this possibility first
            if (newTeam != null) {
                
                newFantasyTeam = newTeam;
                
                if (!draft.allPositionsFilled()) {
                    possiblePlayerPositions.setPredicate(pos -> {
                               return playerToEdit.getQualifiedPositionsList().contains(pos)
                                       && newTeam.getUnfilledPositions().contains(pos);
                   });             
                } else {
                    possiblePlayerPositions.setPredicate(pos -> {
                               return playerToEdit.getQualifiedPositionsList().contains(pos)
                                       && DraftModel.getFantasyTeamPositions().contains(pos);
                   }); 
                }
                
                // The possible player positions list will be empty if this player
                // has no position to fill on the team, so disable it if this is
                // the case
                if (possiblePlayerPositions.isEmpty()) {
                    posComboBox.setDisable(true);
                } else {
                    posComboBox.setDisable(false);
                }
                
                // Now enable the selection of a contract because there's a team selected
                contractComboBox.setDisable(false);
                contractComboBox.setItems(newFantasyTeam.getAvailableContracts());
            }
        });
        
        posComboBox.valueProperty().addListener((ov, oldPos, newPos) -> {
            position = newPos;
        });
        
        contractComboBox.valueProperty().addListener((ov, oldContract, newContract) -> {
            contract = newContract;
        });
        
        salaryTextField.textProperty().addListener((observable, oldSalary, newSalary) -> {
            salary = newSalary;
        });
    }    
    
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * @param playerToEdit
     * @param fantasyTeam
     * @param isFreeAgent
     */
    public void showEditPlayerDialog(Player playerToEdit, Team fantasyTeam, boolean isFreeAgent) {
        this.playerToEdit = playerToEdit;
        
        // We don't want to give the user the option to add a player
        // to the player pool if he is already a free agent
        addToPoolButton.setDisable(isFreeAgent);
        
        // Change the player profile image and the flag image to reflect the
        // selected player
        profileImageView.setImage(assetManager.getProfileImagesMap().get(playerToEdit.getProfileImageKey()));
        flagImageView.setImage(assetManager.getFlagImagesMap().get(playerToEdit.getFlagImageKey()));
        
        nameLabel.setText(playerToEdit.getFullName());
        positionsLabel.setText(playerToEdit.getQualifiedPositions());
        
        // If the player is not a free agent, then the player is drafted
        // to a team, so we must load his stats into the UI
        if (!isFreeAgent) {
            previousFantasyTeam = fantasyTeam;
            loadGUIData(playerToEdit, previousFantasyTeam);
        } else {
            resetGUIData();
        }
        
        // Set the dialog title
        setTitle(EDIT_PLAYER_TITLE);
        
        // And open it up
        this.showAndWait();
    }
    
    /**
     * Clears all the fields of the GUI. This is good for when we are
     * editing a player that has not been edited before (ex. is in the
     * player pool)
     */
    private void resetGUIData() {
        posComboBox.setDisable(true);
        contractComboBox.setDisable(true);
        fantasyTeamComboBox.valueProperty().set(null);
        posComboBox.valueProperty().set(null);
        contractComboBox.valueProperty().set(null);
        salaryTextField.setText("");
    }
    
    /**
     * Loads the fields of the GUI with data from the given team. This is
     * good when we are editing data of a player who has already been edited
     * before (ex. is currently drafted to a fantasy team)
     * 
     * @param fantasyTeam 
     */
    private void loadGUIData(Player playerToEdit, Team fantasyTeam) {    
        fantasyTeamComboBox.setValue(fantasyTeam);
        posComboBox.setValue(playerToEdit.getPosition());
        contractComboBox.setValue(playerToEdit.getContract());
        salaryTextField.setText(Integer.toString(playerToEdit.getSalary())); 
       
        if (!draft.allPositionsFilled()) {
            possiblePlayerPositions.setPredicate(pos -> {
                       return playerToEdit.getQualifiedPositionsList().contains(pos)
                               && fantasyTeam.getUnfilledPositions().contains(pos);
           });             
        } else {
            possiblePlayerPositions.setPredicate(pos -> {
                       return playerToEdit.getQualifiedPositionsList().contains(pos)
                               && DraftModel.getFantasyTeamPositions().contains(pos);
           }); 
        }
    }
    
    public Team getFantasyTeam() {
        return newFantasyTeam;
    }

    public void setPreviousFantasyTeam(Team previousFantasyTeam) {
        this.previousFantasyTeam = previousFantasyTeam;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }
    
    public void setSalary(String salary) {
        this.salary = salary;
    }
    
    /**
     * Returns the salary selected, unless the salary isn't valid, in which case
     * return -1 which will tell the program that we encountered an error
     * 
     * @return The salary entered by the user
     */
    public int getSalary() {
        try {
            return Integer.parseInt(salary);
        } catch (IllegalArgumentException ex) {
            return -1;
        }
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public boolean wasFreeAgentSelected() {
        return selection.equals(FREE_AGENT);
    }
    
    public boolean playerChangedPosition() {
        return (!wasPlayerTraded() && !position.equals(playerToEdit.getPosition()));
    }
    
    public boolean wasPlayerTraded() {
        return previousFantasyTeam != newFantasyTeam;
    }
    
    /**
     * Initialize all the buttons of this UI
     */
    private void initButtons() {
        addToPoolButton = new Button(FREE_AGENT);        
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // The user can only complete the dialog when every field has been filled
        completeButton.disableProperty().bind(
            Bindings.isEmpty(salaryTextField.textProperty())
            .or(Bindings.isNull(fantasyTeamComboBox.valueProperty())
            .or(Bindings.isNull(posComboBox.valueProperty()))
            .or(Bindings.isNull(contractComboBox.valueProperty()))));
        
        // Register event handlers for complete and cancel button
        EventHandler cancelCompleteHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> { 
            Button sourceButton = (Button)ae.getSource();
            this.selection = sourceButton.getText();
            this.hide();
        };
        
        // Clicking the add to pool button tells the program to move this
        // player to the player pool
        addToPoolButton.setOnAction(e -> {
            this.selection = FREE_AGENT;
            this.hide();
        });                
        
        completeButton.setOnAction(cancelCompleteHandler);        
        cancelButton.setOnAction(cancelCompleteHandler);
    }
    
    private ComboBox initComboBox(boolean disabled, int prefWidth) {
        ComboBox cb = new ComboBox();
        cb.setDisable(disabled);
        cb.setMinWidth(prefWidth);
        cb.setPrefWidth(prefWidth);
        
        return cb;
    }
}
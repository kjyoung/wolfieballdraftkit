package wdk.gui;

import java.text.DecimalFormat;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import static wdk.WDK_StartupConstants.PATH_IMAGES;
import wdk.controller.DraftEditController;
import wdk.controller.FantasyTeamController;
import wdk.controller.PlayerController;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.data.Team;
import wdk.data.TeamComparator;
import wdk.file.AssetManager;
import static wdk.gui.WDK_GUI.CLASS_TABLE;
import static wdk.gui.WDK_GUI.COL_BA_WHIP;
import static wdk.gui.WDK_GUI.COL_CONTRACT;
import static wdk.gui.WDK_GUI.COL_EST_VALUE;
import static wdk.gui.WDK_GUI.COL_FIRST_NAME;
import static wdk.gui.WDK_GUI.COL_HR_SV;
import static wdk.gui.WDK_GUI.COL_LAST_NAME;
import static wdk.gui.WDK_GUI.COL_POSITION;
import static wdk.gui.WDK_GUI.COL_POSITIONS;
import static wdk.gui.WDK_GUI.COL_PRO_TEAM;
import static wdk.gui.WDK_GUI.COL_RBI_K;
import static wdk.gui.WDK_GUI.COL_R_W;
import static wdk.gui.WDK_GUI.COL_SALARY;
import static wdk.gui.WDK_GUI.COL_SB_ERA;

/**
 * This screen lets one view the players on all the fantasy team rosters
 * where the user would select the fantasy team to view via a combo box
 * 
 * @author Kevin
 */
public class FantasyTeamsScreen {
    
    HBox topToolbar;
    
    // Will prompt the user to confirm or back out of a selection
    YesNoCancelDialog yesNoCancelDialog;
    
    MessageDialog messageDialog;
    
    // The columns for the player pool table
    TableColumn positionCol;
    TableColumn firstNameCol;
    TableColumn lastNameCol;
    TableColumn proTeamCol;
    TableColumn positionsCol;
    TableColumn R_WCol;
    TableColumn HR_SVCol;
    TableColumn RBI_KCol;
    TableColumn SB_ERACol;
    TableColumn BA_WHIPCol;
    TableColumn estValueCol;
    TableColumn contractCol;
    TableColumn salaryCol;
    
    // The columns for the taxi squad
    TableColumn TSPositionCol;
    TableColumn TSFirstNameCol;
    TableColumn TSLastNameCol;
    TableColumn TSProTeamCol;
    TableColumn TSPositionsCol;
    TableColumn TSR_WCol;
    TableColumn TSHR_SVCol;
    TableColumn TSRBI_KCol;
    TableColumn TSSB_ERACol;
    TableColumn TSBA_WHIPCol;
    TableColumn TSEstValueCol;
    TableColumn TSContractCol;
    TableColumn TSSalaryCol;
    
    // By typing in the search text field, the table should only display players
    // whose first or last names start with that text (ignore case)
    TextField draftNameField;    
    static String draftNameFieldText = "";   
    
    TableView<Player> startingLineupTable;
    TableView<Player> taxiSquadTable;
    
    // These will format the numbers of certain player stats so they look 
    // nice in the table
    DecimalFormat BA_FORMAT; 
    DecimalFormat WHIP_FORMAT; 
    DecimalFormat ERA_FORMAT; 
    
    BorderPane fantasyTeamsScreenPane;
    
    Label fantasyTeamsScreenHeadingLabel;
    
    // Will hold the starting lineup table
    VBox startingLineupBox;
    VBox taxiSquadBox;
 
    Button addTeamButton;
    Button removeTeamButton;
    Button editTeamButton;
    
    VBox topWorkspacePane;
    
    Label startingLineupTableLabel;
    Label taxiSquadTableLabel;
    
    Pane primaryPane;
    
    // Holds the draft name label and text field
    HBox draftNamePane;
    
    // Holds the buttons to control the teams, as well as the team selection control
    HBox teamPane;
    
    // This is how we will select which fantasy team to view
    public static ComboBox<Team> teamSelectionComboBox;
    
    // This controls adding, removing, and editing a fantasy team
    FantasyTeamController fantasyTeamController;
    
    DraftModelDataManager dataManager;
    
    Stage primaryStage;
    
    DraftModel draft;            
    
    AssetManager assetManager;
    
    Team currentlySelectedFantasyTeam;
    
    DraftEditController draftEditController;
    
    WDK_GUI gui;
    
    public static ChangeListener<Team> teamSelectionListener;
    
    public FantasyTeamsScreen(WDK_GUI gui, Stage initPrimaryStage, DraftModelDataManager dataManager,
                              MessageDialog messageDialog, YesNoCancelDialog yesNoCancelDialog,
                              DraftEditController draftEditController, 
                              AssetManager assetManager) {
       
        this.gui = gui;
        this.draftEditController = draftEditController;
        this.primaryStage = initPrimaryStage;
        this.dataManager = dataManager;
        this.draft = dataManager.getDraftModel();
        this.messageDialog = messageDialog;
        this.yesNoCancelDialog = yesNoCancelDialog;
        this.assetManager = assetManager;

        fantasyTeamsScreenPane = new BorderPane();

        BA_FORMAT = new DecimalFormat(".000");
        WHIP_FORMAT = new DecimalFormat("0.00");
        ERA_FORMAT = new DecimalFormat("0.00");
    }
    
    public void initGUI() {
       
       initButtons();
       
       initTables();
       
       initDraftNameField();
       
       initWorkspace();
       
       initEventHandlers(); 
    }
    
    /**
     * Visually removes any of the GUI data currently being shown. This
     * is useful for when we load a Draft in the middle of viewing a fantasy
     * team, because if the GUI is not reset and we are viewing a fantasy team
     * that the loaded draft does not contain, then the table may still show
     * that fantasy team.
     */
    public void resetGUIData() {       
        teamSelectionComboBox.valueProperty().set(null); 
        draftNameField.setText("");
        startingLineupTable.setItems(null);
        startingLineupTable.setVisible(false);
        startingLineupTable.setVisible(true);
    }
    
    public Pane getPane() {
        return fantasyTeamsScreenPane;
    }
    
    private void initTeamSelectionComboBox() {
        
        teamSelectionComboBox = initHBoxComboBox(teamPane);   
        teamSelectionComboBox.setPrefWidth(200);   
        
        ObservableList<Team> fantasyTeams = draft.getFantasyTeams();
        
        // Now trap the FilteredList of Players in a SortedList.
        SortedList<Team> sortedFantasyTeams = new SortedList<>(fantasyTeams);
        
        TeamComparator teamComparator = new TeamComparator();
        sortedFantasyTeams.setComparator(teamComparator);
        
        // The combo box will hold all the fantasy teams that are part of the draft
        teamSelectionComboBox.setItems(sortedFantasyTeams);
        
        // Bind the combobox and the edit and add team buttons to be disabled
        // when there are no teams to display or a team is not selected by the user
        teamSelectionComboBox.disableProperty().bind(
            Bindings.isEmpty(fantasyTeams));        
        removeTeamButton.disableProperty().bind(
            Bindings.isNull(teamSelectionComboBox.valueProperty()));        
        editTeamButton.disableProperty().bind(
            Bindings.isNull(teamSelectionComboBox.valueProperty()));
    }
    
     /**
     * Initialize the field that will take in the Draft name
     */
    private void initDraftNameField() {      
        draftNameField = new TextField("");   
    }
    
    private void initEventHandlers() {   
        fantasyTeamController = new FantasyTeamController(primaryStage, draft, messageDialog, yesNoCancelDialog);
        
        addTeamButton.setOnAction(e -> {
            boolean wasTeamAdded = fantasyTeamController.handleAddFantasyTeamRequest();
            if (wasTeamAdded) {
                // Mark the draft file as dirty as we have just changed it
                draftEditController.handleDraftChangeRequest(gui);                
            }  
        });
        editTeamButton.setOnAction(e -> {
            boolean wasTeamEdited = fantasyTeamController.handleEditFantasyTeamRequest(teamSelectionComboBox.getSelectionModel().getSelectedItem());
            if (wasTeamEdited) {
                // Mark the draft file as dirty as we have just changed it
                draftEditController.handleDraftChangeRequest(gui);                
            }    
        });
        removeTeamButton.setOnAction(e -> {
            boolean wasTeamRemoved = fantasyTeamController.handleRemoveFantasyTeamRequest(teamSelectionComboBox.getSelectionModel().getSelectedItem());
            if (wasTeamRemoved) {
                teamSelectionComboBox.setValue(null);
                
                // Mark the draft file as dirty as we have just changed it
                draftEditController.handleDraftChangeRequest(gui);                
            }            
        });
        
        PlayerController playerController = new PlayerController(primaryStage, messageDialog,
                                                yesNoCancelDialog, dataManager, assetManager);
        
        startingLineupTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Player player = startingLineupTable.getSelectionModel().getSelectedItem();
                
                // If the first name is empty, then this player has not been intiialized
                // past being a placeholder in the list, so we don't want to edit the player
                if (player != null) {
                    if (!player.isPlaceholder()) {
                        boolean wasDraftedPlayerEdited = playerController.handleEditDraftedPlayerRequest(player, (Team)teamSelectionComboBox.getValue());
                        
                        if (wasDraftedPlayerEdited) {
                            // Mark the draft file as dirty as we have just changed it
                            draftEditController.handleDraftChangeRequest(gui);                
                        }  
                        
                        // As of this version of Java 8, table views do not always
                        // update as we would hope, so as a work-around, this will
                        // simply "refresh" the column so that any changed player data
                        // will be shown
                        TableColumn playerColumn = (TableColumn) startingLineupTable.getColumns().get(0);
                        playerColumn.setVisible(false);
                        playerColumn.setVisible(true);
                    }
                }
            }
        });
        
        teamSelectionListener = new ChangeListener<Team>() {
            @Override 
            public void changed(ObservableValue ov, Team oldTeam, Team newSelectedTeam) {
                if (newSelectedTeam != null) {
                    startingLineupTable.setItems(newSelectedTeam.getRoster());
                    taxiSquadTable.setItems(newSelectedTeam.getTaxiSquad());
                } else {
                    startingLineupTable.setItems(null);
                    taxiSquadTable.setItems(null);
                }
            }
        };
            
        teamSelectionComboBox.valueProperty().addListener(teamSelectionListener);
        
        
        draftNameField.textProperty().addListener(new ChangeListener<String>() {
            @Override 
            public void changed(ObservableValue ov, String oldName, String newName) {
                // Mark the draft file as dirty as we have just changed the name
                //draftEditController.handleDraftChangeRequest(gui);     
                // The draft is now dirty, meaning it's been 
                // changed since it was last saved, so make sure
                // the save button is enabled
                gui.getFileController().markFileAsDirty(gui);
                draft.setDraftName(newName);
            }    
        });
    }
    
    public TableView getStartingLineupTable() {
        return startingLineupTable;
    }
    
    private void initWorkspace() {
        
        initTopWorkspace();        
        
        fantasyTeamsScreenPane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);        
        fantasyTeamsScreenPane.setTop(topWorkspacePane);        
        fantasyTeamsScreenPane.setCenter(startingLineupBox);
        fantasyTeamsScreenPane.setBottom(taxiSquadBox);
    }
    
    private void initTopWorkspace() {
        
        topWorkspacePane = new VBox();
        topWorkspacePane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        
        GridPane topToolbarPane = new GridPane();
        topToolbarPane.setMinWidth(100);
        
        // The screen label
        fantasyTeamsScreenHeadingLabel = initChildLabel(topWorkspacePane, WDK_PropertyType.FANTASY_TEAM_HEADING_LABEL, WDK_GUI.CLASS_HEADING_LABEL);
        
        // Create the area for the draft name editing
        draftNamePane = new HBox();
        Label draftNameLabel = initLabel(WDK_PropertyType.ENTER_DRAFT_NAME_LABEL, WDK_GUI.CLASS_SUBHEADING_LABEL);
        HBox.setMargin(draftNameLabel, new Insets(0, 25, 0, 0));
        draftNamePane.getChildren().addAll(draftNameLabel, draftNameField);
        
        // Create the area for the fantasy team control
        teamPane = new HBox();
        Label selectTeamLabel = initLabel(WDK_PropertyType.SELECT_FANTASY_TEAM_LABEL, WDK_GUI.CLASS_SUBHEADING_LABEL); 
        HBox.setMargin(removeTeamButton, new Insets(0, 0, 0, 15));
        HBox.setMargin(editTeamButton, new Insets(0, 20, 0, 15));    
        HBox.setMargin(selectTeamLabel, new Insets(0, 25, 0, 0));
        teamPane.getChildren().addAll(addTeamButton, removeTeamButton, editTeamButton, selectTeamLabel);
        
        // Create the team selection combo box
        initTeamSelectionComboBox();
        
        topWorkspacePane.getChildren().addAll(draftNamePane, teamPane);
    }
    
    private void initTables() {
        
        // Initialize the starting lineup table and make it pretty
        startingLineupTable = new TableView();  
        startingLineupTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        startingLineupTable.setPrefHeight(600);
        startingLineupTable.getStyleClass().add(CLASS_TABLE);
        
        // Initialize the taxi squad lineup table and make it pretty
        taxiSquadTable = new TableView();
        taxiSquadTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        taxiSquadTable.setPrefHeight(600);
        taxiSquadTable.getStyleClass().add(CLASS_TABLE);        
        
        // Initialize the pane that will hold the table and make it pretty
        startingLineupBox = new VBox();
        startingLineupBox.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        VBox.setVgrow(startingLineupBox, Priority.ALWAYS);    
        
        taxiSquadBox = new VBox();
        taxiSquadBox.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        VBox.setVgrow(taxiSquadBox, Priority.ALWAYS);        
        
        // Label the starting lineup pane
        Label startingLineupLabel = initLabel(WDK_PropertyType.STARTING_LINEUP_LABEL, WDK_GUI.CLASS_SMALLER_HEADING_LABEL); 
        
        // Label the taxi squad pane
        Label taxiSquadLabel = initLabel(WDK_PropertyType.TAXI_SQUAD_LABEL, WDK_GUI.CLASS_SMALLER_HEADING_LABEL); 
        
        // Add the table to the pane
        startingLineupBox.getChildren().addAll(startingLineupLabel, startingLineupTable);        
        taxiSquadBox.getChildren().addAll(taxiSquadLabel, taxiSquadTable);
        
        // Initialize all of the Table columns
        positionCol = new TableColumn(COL_POSITION);
        firstNameCol = new TableColumn(COL_FIRST_NAME);
        lastNameCol = new TableColumn(COL_LAST_NAME);
        proTeamCol = new TableColumn(COL_PRO_TEAM);
        positionsCol = new TableColumn(COL_POSITIONS);
        R_WCol = new TableColumn(COL_R_W);
        HR_SVCol = new TableColumn(COL_HR_SV);
        RBI_KCol = new TableColumn(COL_RBI_K);
        SB_ERACol = new TableColumn(COL_SB_ERA);
        BA_WHIPCol = new TableColumn(COL_BA_WHIP);
        estValueCol = new TableColumn(COL_EST_VALUE);
        contractCol = new TableColumn(COL_CONTRACT);
        salaryCol = new TableColumn(COL_SALARY);
        
        // Now link the columns to the data
        positionCol.setCellValueFactory(new PropertyValueFactory<String, String>("position"));        
        firstNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        proTeamCol.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        positionsCol.setCellValueFactory(new PropertyValueFactory<String, String>("qualifiedPositions"));     
        estValueCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("estimatedValue")); 
        contractCol.setCellValueFactory(new PropertyValueFactory<String, String>("contract")); 
        
        estValueCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                Player thisPlayer = player.getValue();
                if (thisPlayer != null) {
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int estimatedValue = thisPlayer.getEstimatedValue();
                    return new SimpleIntegerProperty(estimatedValue);
                } else {
                    return null;
                }
            }
        });
        
        salaryCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                Player thisPlayer = player.getValue();
                if (thisPlayer != null) {
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int salary = thisPlayer.getSalary();
                    return new SimpleIntegerProperty(salary);
                } else {
                    return null;
                }
            }
        });
        
        /* The following columns hold one of two data fields of a Player, depending on
         if the Player is a Hitter or a Pitcher. So for each column field, we must
         link the column to the data depending on what instance of Player
         the TableView row contains. */
        R_WCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int W = thisPlayer.getW();
                    return new SimpleIntegerProperty(W);
                } else if (player.getValue() instanceof Hitter) {  
                    Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int R = thisPlayer.getR();
                    return new SimpleIntegerProperty(R);
                } else {
                    return null;
                }
            }
        });
        
        HR_SVCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int SV = thisPlayer.getSV();
                    return new SimpleIntegerProperty(SV);
                } else if (player.getValue() instanceof Hitter) { 
                    Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int HR = thisPlayer.getHR();
                    return new SimpleIntegerProperty(HR);
                } else {
                    return null;
                }
            }
        });
        
        RBI_KCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int K = thisPlayer.getK();
                    return new SimpleIntegerProperty(K);
                } else if (player.getValue() instanceof Hitter) { 
                    Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int RBI = thisPlayer.getRBI();
                    return new SimpleIntegerProperty(RBI);
                } else {
                    return null;
                }
            }
        });
        
        SB_ERACol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Double> player) {
                if (player.getValue() instanceof Pitcher) {
                    Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    double ERA = thisPlayer.getERA();
                    return new SimpleStringProperty(ERA_FORMAT.format(ERA));
                } else if (player.getValue() instanceof Hitter) { 
                    Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int SB = thisPlayer.getSB();
                    return new SimpleIntegerProperty(SB);
                } else {
                    return null;
                }
            }
        });
        
        BA_WHIPCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {

           @Override
           public ObservableValue call(TableColumn.CellDataFeatures<Player, Double> player) {
               if (player.getValue() instanceof Pitcher) {
                   Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                   double WHIP = thisPlayer.getWHIP();
                   return new SimpleStringProperty(WHIP_FORMAT.format(WHIP));
               } else if (player.getValue() instanceof Hitter) {
                   Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                   double BA = thisPlayer.getBA();
                   return new SimpleStringProperty(BA_FORMAT.format(BA).substring(BA_FORMAT.format(BA).indexOf('.')));
               } else {
                   return null;
               }
           }
       });                
        
        
        // We want the names to be compared ignoring case so that lowercase names aren't by default "greater than" uppercase names
        firstNameCol.setComparator((name1, name2) -> ((String)name1).compareToIgnoreCase((String)name2));
        lastNameCol.setComparator((name1, name2) -> ((String)name1).compareToIgnoreCase((String)name2));
        
        // Finally add all the columns to the Player pool Table        
        startingLineupTable.getColumns().addAll(positionCol, firstNameCol, lastNameCol, proTeamCol, 
                                            positionsCol, R_WCol, HR_SVCol,
                                            RBI_KCol, SB_ERACol, BA_WHIPCol, estValueCol,
                                            contractCol, salaryCol); 
        
        // Now for taxi squad
        
        TSPositionCol = new TableColumn(COL_POSITION);
        TSFirstNameCol = new TableColumn(COL_FIRST_NAME);
        TSLastNameCol = new TableColumn(COL_LAST_NAME);
        TSProTeamCol = new TableColumn(COL_PRO_TEAM);
        TSPositionsCol = new TableColumn(COL_POSITIONS);
        TSR_WCol = new TableColumn(COL_R_W);
        TSHR_SVCol = new TableColumn(COL_HR_SV);
        TSRBI_KCol = new TableColumn(COL_RBI_K);
        TSSB_ERACol = new TableColumn(COL_SB_ERA);
        TSBA_WHIPCol = new TableColumn(COL_BA_WHIP);
        TSEstValueCol = new TableColumn(COL_EST_VALUE);
        TSContractCol = new TableColumn(COL_CONTRACT);
        TSSalaryCol = new TableColumn(COL_SALARY);
        
        // Now link the columns to the data
        TSPositionCol.setCellValueFactory(new PropertyValueFactory<String, String>("position"));        
        TSFirstNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        TSLastNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        TSProTeamCol.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        TSPositionsCol.setCellValueFactory(new PropertyValueFactory<String, String>("qualifiedPositions"));      
        TSContractCol.setCellValueFactory(new PropertyValueFactory<String, String>("contract")); 
        
        TSEstValueCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                Player thisPlayer = player.getValue();
                if (thisPlayer != null) {
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int estimatedValue = thisPlayer.getEstimatedValue();
                    return new SimpleIntegerProperty(estimatedValue);
                } else {
                    return null;
                }
            }
        });
        
        TSSalaryCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                Player thisPlayer = player.getValue();
                if (thisPlayer != null) {
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int salary = thisPlayer.getSalary();
                    return new SimpleIntegerProperty(salary);
                } else {
                    return null;
                }
            }
        });
        
        /* The following columns hold one of two data fields of a Player, depending on
         if the Player is a Hitter or a Pitcher. So for each column field, we must
         link the column to the data depending on what instance of Player
         the TableView row contains. */
        TSR_WCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int W = thisPlayer.getW();
                    return new SimpleIntegerProperty(W);
                } else if (player.getValue() instanceof Hitter) {  
                    Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int R = thisPlayer.getR();
                    return new SimpleIntegerProperty(R);
                } else {
                    return null;
                }
            }
        });
        
        TSHR_SVCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int SV = thisPlayer.getSV();
                    return new SimpleIntegerProperty(SV);
                } else if (player.getValue() instanceof Hitter) { 
                    Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int HR = thisPlayer.getHR();
                    return new SimpleIntegerProperty(HR);
                } else {
                    return null;
                }
            }
        });
        
        TSRBI_KCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int K = thisPlayer.getK();
                    return (K <= 0 ? new SimpleStringProperty("") : new SimpleIntegerProperty(K));
                } else if (player.getValue() instanceof Hitter) { 
                    Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int RBI = thisPlayer.getRBI();
                    return new SimpleIntegerProperty(RBI);
                } else {
                    return null;
                }
            }
        });
        
        TSSB_ERACol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Double> player) {
                if (player.getValue() instanceof Pitcher) {
                    Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    double ERA = thisPlayer.getERA();
                    return new SimpleStringProperty(ERA_FORMAT.format(ERA));
                } else if (player.getValue() instanceof Hitter) { 
                    Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                    int SB = thisPlayer.getSB();
                    return new SimpleIntegerProperty(SB);
                } else {
                    return null;
                }
            }
        });
        
        TSBA_WHIPCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {

           @Override
           public ObservableValue call(TableColumn.CellDataFeatures<Player, Double> player) {
               if (player.getValue() instanceof Pitcher) {
                   Pitcher thisPlayer = (Pitcher)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                   double WHIP = thisPlayer.getWHIP();
                   return new SimpleStringProperty(WHIP_FORMAT.format(WHIP));
               } else if (player.getValue() instanceof Hitter) {
                   Hitter thisPlayer = (Hitter)player.getValue();
                    if (thisPlayer.isPlaceholder()) {
                        return new SimpleStringProperty("");
                    }
                   double BA = thisPlayer.getBA();
                   return new SimpleStringProperty(BA_FORMAT.format(BA).substring(BA_FORMAT.format(BA).indexOf('.')));
               } else {
                   return null;
               }
           }
       });                
        
        taxiSquadTable.getColumns().addAll(TSPositionCol, TSFirstNameCol, TSLastNameCol, TSProTeamCol, 
                                            TSPositionsCol, TSR_WCol, TSHR_SVCol,
                                            TSRBI_KCol, TSSB_ERACol, TSBA_WHIPCol, TSEstValueCol,
                                            TSContractCol,TSSalaryCol);
    }
    
   
    // Init a combo box and put it in a HBox
    private ComboBox initHBoxComboBox(HBox container) {
        ComboBox comboBox = new ComboBox();
        container.getChildren().add(comboBox);
        return comboBox;
    }
    
    // Init a label and put it in a toolbar
    private Label initChildLabel(Pane container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    // Init a label and set it's stylesheet class
    private Label initLabel(String labelText, String styleClass) {
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    
    // Init a label and set it's stylesheet class
    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
     private void initButtons() {        
        addTeamButton = initButton(WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_TEAM_TOOLTIP);
        removeTeamButton = initButton(WDK_PropertyType.DELETE_ICON, WDK_PropertyType.REMOVE_TEAM_TOOLTIP);        
        editTeamButton = initButton(WDK_PropertyType.EDIT_ICON, WDK_PropertyType.EDIT_TEAM_TOOLTIP);
    }
     
    private Button initButton(WDK_PropertyType icon, WDK_PropertyType tooltip) {
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        
        Button toolbarButton = new Button();
        toolbarButton.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        toolbarButton.setTooltip(buttonTooltip);
        
        return toolbarButton;
    }
    
}

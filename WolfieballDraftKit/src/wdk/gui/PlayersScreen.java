package wdk.gui;

import java.text.DecimalFormat;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import static wdk.WDK_StartupConstants.PATH_IMAGES;
import wdk.controller.DraftEditController;
import wdk.controller.PlayerController;
import wdk.data.DraftModelDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.file.AssetManager;
import static wdk.gui.WDK_GUI.CLASS_TABLE;
import static wdk.gui.WDK_GUI.COL_BA_WHIP;
import static wdk.gui.WDK_GUI.COL_EST_VALUE;
import static wdk.gui.WDK_GUI.COL_FIRST_NAME;
import static wdk.gui.WDK_GUI.COL_HR_SV;
import static wdk.gui.WDK_GUI.COL_LAST_NAME;
import static wdk.gui.WDK_GUI.COL_POSITIONS;
import static wdk.gui.WDK_GUI.COL_PRO_TEAM;
import static wdk.gui.WDK_GUI.COL_RBI_K;
import static wdk.gui.WDK_GUI.COL_R_W;
import static wdk.gui.WDK_GUI.COL_SB_ERA;


/**
 * This screen lets one view all the players available, a.k.a the player pool
 * (i.e. not on any fantasy team), and lets the user limit those presented to 
 * specific positions with their statistics and their estimated value.
 * It also lets one search for any player by first or last name
 * 
 * @author Kevin
 */
public class PlayersScreen {
    
    // The primary pane of this screen
    BorderPane playerScreenPane;
    
    HBox topToolbar;
    
    MessageDialog messageDialog;
    
    // Manages and holds the data of the Draft we are currently editing
    DraftModelDataManager dataManager;
    
    // Will prompt the user to confirm or back out of a selection
    YesNoCancelDialog yesNoCancelDialog;
    
    // The table of all the players in the player pool and its columns
    public static TableView<Player> playerPoolTable;
    
    // The columns for the player pool table
    TableColumn firstNameCol;
    TableColumn lastNameCol;
    TableColumn proTeamCol;
    TableColumn posCol;
    TableColumn birthYearCol;
    TableColumn R_WCol;
    TableColumn HR_SVCol;
    TableColumn RBI_KCol;
    TableColumn SB_ERACol;
    TableColumn BA_WHIPCol;
    TableColumn estValueCol;
    TableColumn notesCol;
    
    // The text that the table columns display
    String COL_BIRTH_YEAR = "Year of Birth";
    String COL_NOTES = "Notes";
    
    // The RadioButtons that when clicked, will determine what type of Players
    // to show in the table based on the Player's position    
    HBox paneForRadioButtons;
    RadioButton rbAll;
    RadioButton rbC;
    RadioButton rb1B;
    RadioButton rbCI;
    RadioButton rb3B;
    RadioButton rb2B;
    RadioButton rbMI;
    RadioButton rbSS;
    RadioButton rbOF;
    RadioButton rbU; // All hitters
    RadioButton rbP; // All pitchers 
    
    // The textual representation of the position radiobuttons
    String RB_ALL = "All";
    String RB_C = "C";
    String RB_1B = "1B";
    String RB_CI = "CI";
    String RB_3B = "3B";
    String RB_2B = "2B";
    String RB_MI = "MI";
    String RB_SS = "SS";
    String RB_OF = "OF";
    String RB_U = "U";
    String RB_P = "P";
    
    // Will control all of the position radiobuttons
    ToggleGroup positionSelectionGroup;
    
    // By typing in the search text field, the table should only display players
    // whose first or last names start with that text (ignore case)
    TextField playerSearchBar;    
    static String playerSearchBarText = "";   
    
    // This will make it easier to filter out the players we don't want to show
    // based on the search bar and position buttons
    FilteredList<Player> filteredPlayerList;
    
    // These will format the numbers of certain player stats so they look 
    // nice in the table
    DecimalFormat BA_FORMAT; 
    DecimalFormat WHIP_FORMAT; 
    DecimalFormat ERA_FORMAT; 
    
    // Will hold the player pool table
    VBox playerPoolBox;
 
    Button addPlayerButton;
    Button removePlayerButton;
    
    VBox topWorkspacePane;
    Label playersScreenHeadingLabel;
    
    Label playerPoolLabel;
    
    Stage primaryStage;
    
    AssetManager assetManager;
    
    WDK_GUI gui;
    
    DraftEditController draftEditController;
    
    public PlayersScreen(WDK_GUI gui, Stage initPrimaryStage, DraftModelDataManager dataManager,
                              MessageDialog messageDialog, YesNoCancelDialog yesNoCancelDialog,
                              DraftEditController draftEditController, 
                              AssetManager assetManager) {
        
        this.draftEditController = draftEditController;
        this.gui = gui;
        this.assetManager = assetManager;
        this.primaryStage = initPrimaryStage;
        this.messageDialog = messageDialog;
        
        playerScreenPane = new BorderPane();

        this.dataManager = dataManager;

        this.yesNoCancelDialog = yesNoCancelDialog;

        BA_FORMAT = new DecimalFormat("#.000");
        WHIP_FORMAT = new DecimalFormat("0.00");
        ERA_FORMAT = new DecimalFormat("0.00"); 

        // Wrap the ObservableList in a FilteredList (initially display all data).
        filteredPlayerList = new FilteredList<>(dataManager.getDraftModel().
                                                     getPlayerPool(), p -> true);
    }
    
    public void initGUI() {
        
       initButtons();
       
       initRadioButtons();
       
       initPlayerPoolTable();
       
       initPlayerSearchBar();
       
       initEventHandlers();       
       
       initWorkspace();
    }
    
    private void initWorkspace() {
        
        initTopWorkspace();
        
        playerScreenPane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        
        playerScreenPane.setTop(topWorkspacePane);
        playerScreenPane.setCenter(paneForRadioButtons);
        playerScreenPane.setBottom(playerPoolBox);
    }
    
    private void initRadioButtons() {
        
        paneForRadioButtons = new HBox();
        paneForRadioButtons.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        paneForRadioButtons.setPadding(new Insets(5,5,5,5));
        
        // Initialize all the radio buttons
        rbAll = new RadioButton(RB_ALL);
        rbC = new RadioButton(RB_C);
        rb1B = new RadioButton(RB_1B);
        rbCI = new RadioButton(RB_CI);
        rb3B = new RadioButton(RB_3B);
        rb2B = new RadioButton(RB_2B);
        rbMI = new RadioButton(RB_MI);
        rbSS  = new RadioButton(RB_SS);
        rbOF = new RadioButton(RB_OF);
        rbU = new RadioButton(RB_U);
        rbP = new RadioButton(RB_P); 
        
        /* Set the user data of each button to the position it corresponds to
         this is so when we do the event handle, we'll have an easier way
         of knowing what position player we want when a button is toggled */
        rbAll.setUserData(RB_ALL);
        rbC.setUserData(RB_C);
        rb1B.setUserData(RB_1B);
        rbCI.setUserData(RB_CI);
        rb3B.setUserData(RB_3B);
        rb2B.setUserData(RB_2B);
        rbMI.setUserData(RB_MI);
        rbSS.setUserData(RB_SS);
        rbOF.setUserData(RB_OF);
        rbU.setUserData(RB_U);
        rbP.setUserData(RB_P);        
        
        // Now add all of the radio buttons to the toggle group
        positionSelectionGroup = new ToggleGroup();
        rbAll.setToggleGroup(positionSelectionGroup);
        rbC.setToggleGroup(positionSelectionGroup);
        rb1B.setToggleGroup(positionSelectionGroup);
        rbCI.setToggleGroup(positionSelectionGroup);
        rb3B.setToggleGroup(positionSelectionGroup);
        rb2B.setToggleGroup(positionSelectionGroup);
        rbMI.setToggleGroup(positionSelectionGroup);
        rbSS.setToggleGroup(positionSelectionGroup);
        rbOF.setToggleGroup(positionSelectionGroup);
        rbU.setToggleGroup(positionSelectionGroup);
        rbP.setToggleGroup(positionSelectionGroup);
        
        // Finally add the radio buttons to the pane
        paneForRadioButtons.getChildren().addAll(rbAll, rbC, rb1B, rbCI, rb3B, 
                rb2B, rbMI, rbSS, rbOF, rbU, rbP);
    }
    
    private void initTopWorkspace() {
        
        topWorkspacePane = new VBox();
        topWorkspacePane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        
        // The screen label
        playersScreenHeadingLabel = initChildLabel(topWorkspacePane, WDK_PropertyType.PLAYER_SCREEN_HEADING_LABEL, WDK_GUI.CLASS_HEADING_LABEL);
        
        Label searchPlayerLabel = initLabel(WDK_PropertyType.SEARCH_PLAYER_LABEL, WDK_GUI.CLASS_SUBHEADING_LABEL);
        
        HBox topToolbarBox = new HBox();
        topToolbarBox.setMinWidth(100);
        topToolbarBox.getChildren().addAll(addPlayerButton, removePlayerButton, searchPlayerLabel,  playerSearchBar);
        
        HBox.setMargin(addPlayerButton, new Insets(0, 20, 0,0));
        HBox.setMargin(removePlayerButton, new Insets(0, 30, 0, 0));
        HBox.setMargin(searchPlayerLabel, new Insets(0, 25, 0, 0));
        HBox.setHgrow(playerSearchBar, Priority.ALWAYS);
        
        topWorkspacePane.getChildren().add(topToolbarBox);
    }
    
    @SuppressWarnings("Convert2Lambda")
    private void initEventHandlers() {
        
        PlayerController playerController = new PlayerController(primaryStage, messageDialog,
                                                yesNoCancelDialog, dataManager, assetManager);
        
        addPlayerButton.setOnAction(e -> playerController.handleAddPlayerRequest());
        removePlayerButton.setOnAction(e -> {
            Player playerToRemove = playerPoolTable.getSelectionModel().getSelectedItem();
            if (playerToRemove != null) {
                boolean wasPlayerRemoved = playerController.handleRemovePlayerRequest(playerToRemove);
                if (wasPlayerRemoved) {
                    // Mark the draft file as dirty as we have just changed it
                    draftEditController.handleDraftChangeRequest(gui);                
                }  
            }
         });
        
        playerPoolTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Player player = playerPoolTable.getSelectionModel().getSelectedItem();
                boolean wasFreeAgentEdited = playerController.handleEditFreeAgentRequest(player, null, false);
                if (wasFreeAgentEdited) {
                    // Mark the draft file as dirty as we have just changed it
                    draftEditController.handleDraftChangeRequest(gui);                
                }  
            }
        });        
        
        /* When the notes column is clicked, the user can edit the field
         which will change that Player's notes value */
        notesCol.setOnEditCommit(new EventHandler<CellEditEvent<Player, String>>() {                
            @Override
            public void handle(CellEditEvent<Player, String> player) {
                ((Player) player.getTableView().getItems().get(
                        player.getTablePosition().getRow())).
                        setNotes(player.getNewValue());
                // Mark the draft file as dirty as we have just changed it
                draftEditController.handleDraftChangeRequest(gui);   
            }                  
        });                
                
        /* This search bar acts in real time, and when the user inputs text,
         the table will display all the Players with a first or last name that 
         matches that text */
        playerSearchBar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredPlayerList.setPredicate(player -> {
                
                // Compare the first and last name of every player in the table 
                // with the text entered                
                String nameToMatch = newValue.toLowerCase();
                
                boolean nameMatchesSearch = (player.getFirstName().toLowerCase().startsWith(nameToMatch)
                                || player.getLastName().toLowerCase().startsWith(nameToMatch));
                
                playerSearchBarText = nameToMatch;
                
                // The currently selected position button
                Toggle currentPositionToggle = positionSelectionGroup.getSelectedToggle();
                
                if (currentPositionToggle == null) {                    
                    // If filter text is empty, display all of the players
                    if (nameToMatch == null || nameToMatch.isEmpty()) {
                        return true;
                    }
                    
                    return nameMatchesSearch;
                    
                } else {                    
                    String selectedPosition = currentPositionToggle.getUserData().toString();
                    
                    switch (selectedPosition) {
                        case "All":
                            return nameMatchesSearch;
                        case "U":
                            return nameMatchesSearch && (player instanceof Hitter);
                        default:
                            return nameMatchesSearch && player.searchForPosition(selectedPosition);
                    }                    
                }
            });
        });        
        
        // Now trap the FilteredList of Players in a SortedList.
        SortedList<Player> sortedPlayerData = new SortedList<>(filteredPlayerList);

        // And bind the SortedList comparator to the table comparator.
        sortedPlayerData.comparatorProperty().bind(playerPoolTable.comparatorProperty());

        // Finally add the sorted (and filtered) data to the table
        playerPoolTable.setItems(sortedPlayerData);        
        
        // When one of the buttons is toggled, the player pool table contents
        // change to show only the players whose qualified positions corresponds
        // to the toggled button.      
        // Keep in mind we also have a search bar, so we have to handle that too */
        positionSelectionGroup.selectedToggleProperty().addListener((
                ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
                    
            /* If one of the Toggles are clicked, then we have to change what type
             of Players are shown in the table, based on their qualified positions */
            if (positionSelectionGroup.getSelectedToggle() != null) {

                String selectedPosition = positionSelectionGroup.getSelectedToggle().
                                                getUserData().toString();               

                filteredPlayerList.setPredicate(player -> {
                    boolean nameMatchesSearch = (player.getFirstName().toLowerCase().startsWith(playerSearchBarText)
                            || player.getLastName().toLowerCase().startsWith(playerSearchBarText));                    
                    
                    
                     // If there is nothing in the search bar, then we only 
                     // have to worry about the qualified positions of the player.
                     // Also note that if the user wants to show only hitters,
                     // then the column text should reflect only hitter stats.
                     // The same idea applies for pitchers. If the user wants to show
                     // all players, then we set the column text to their defaults 
                    if (playerSearchBarText.equals("")) {
                        switch (selectedPosition) {
                            case "All":
                                setColTextToDefaults();
                                return true;
                            case "P":
                                setColTextToPitchersStats();
                                return player.searchForPosition(selectedPosition)
                                       && nameMatchesSearch;
                            default:
                                setColTextToHittersStats();
                                return player.searchForPosition(selectedPosition)
                                       && nameMatchesSearch;
                        } 
                    } // In this case, we have to worry about the positions AND the player name
                    else {  
                        switch (selectedPosition) {
                            case "All":  
                               setColTextToDefaults();
                               return nameMatchesSearch;
                            case "P":
                                setColTextToPitchersStats();
                                return player.searchForPosition(selectedPosition)
                                       && nameMatchesSearch;
                            default:
                                setColTextToHittersStats(); 
                                return player.searchForPosition(selectedPosition)
                                       && nameMatchesSearch;
                        }  
                    }
                }); // Closes the filteredlist predicate
            } 
                            
        });        
    }
    
    /**
     * Initialize the search bar that will search 
     */
    private void initPlayerSearchBar() {        
        // Initialize the search bar
        playerSearchBar = new TextField("");
        playerSearchBar.setPromptText("Enter beginning of player's first or last name");      
    }
    
    @SuppressWarnings("Convert2Lambda")
    private void initPlayerPoolTable() {
        
        // Initialize the table and make it pretty
        playerPoolTable = new TableView();  
        playerPoolTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        playerPoolTable.setPrefHeight(600);
        playerPoolTable.getStyleClass().add(CLASS_TABLE);
        
        // Initialize the pane that will hold the table and make it pretty
        playerPoolBox = new VBox();
        playerPoolBox.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        VBox.setVgrow(playerPoolBox, Priority.ALWAYS);         
        
        // Add the table to the pane
        playerPoolBox.getChildren().add(playerPoolTable);
        
        // We want the notes cell to be editable so set the table to editable
        playerPoolTable.setEditable(true);
        
        // Initialize all of the Table columns
        firstNameCol = new TableColumn(COL_FIRST_NAME);
        lastNameCol = new TableColumn(COL_LAST_NAME);
        proTeamCol = new TableColumn(COL_PRO_TEAM);
        posCol = new TableColumn(COL_POSITIONS);
        birthYearCol = new TableColumn(COL_BIRTH_YEAR);
        R_WCol = new TableColumn(COL_R_W);
        HR_SVCol = new TableColumn(COL_HR_SV);
        RBI_KCol = new TableColumn(COL_RBI_K);
        SB_ERACol = new TableColumn(COL_SB_ERA);
        BA_WHIPCol = new TableColumn(COL_BA_WHIP);
        estValueCol = new TableColumn(COL_EST_VALUE);
        notesCol = new TableColumn(COL_NOTES);
        
        // Now link the columns to the data        
        firstNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        proTeamCol.setCellValueFactory(new PropertyValueFactory<String, String>("proTeam"));
        posCol.setCellValueFactory(new PropertyValueFactory<String, String>("qualifiedPositions"));        
        estValueCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("estimatedValue"));
        notesCol.setCellValueFactory(new PropertyValueFactory<String, String>("notes")); 
        
        // We want only the notes column to be editable
        notesCol.setCellFactory(TextFieldTableCell.forTableColumn());  
        
        birthYearCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                int yearOfBirth = player.getValue().getYearOfBirth();
                if (yearOfBirth <= 0) {
                    return new SimpleStringProperty("-");
                } else {
                    return new SimpleIntegerProperty(yearOfBirth);
                }
            }
        });       
        
        /* The following columns hold one of two data fields of a Player, depending on
         if the Player is a Hitter or a Pitcher. So for each column field, we must
         link the column to the data depending on what instance of Player
         the TableView row contains. */
        R_WCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    int W = ((Pitcher)player.getValue()).getW();
                    return new SimpleIntegerProperty(W);
                } else {  
                    int R = ((Hitter)player.getValue()).getR();
                    return new SimpleIntegerProperty(R);
                }
            }
        });
        
        HR_SVCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    int SV = ((Pitcher)player.getValue()).getSV();
                    return new SimpleIntegerProperty(SV);
                } else {
                    int HR = ((Hitter)player.getValue()).getHR();
                    return new SimpleIntegerProperty(HR);
                }
            }
        });
        
        RBI_KCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Integer>, ObservableValue<Integer>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Integer> player) {
                if (player.getValue() instanceof Pitcher) {
                    int K = ((Pitcher)player.getValue()).getK();
                    return new SimpleIntegerProperty(K);
                } else {
                    int RBI = ((Hitter)player.getValue()).getRBI();
                    return new SimpleIntegerProperty(RBI);
                }
            }
        });
        
        SB_ERACol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {
             
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<Player, Double> player) {
                if (player.getValue() instanceof Pitcher) {
                    double ERA = ((Pitcher)player.getValue()).getERA();
                    return new SimpleStringProperty(ERA_FORMAT.format(ERA));
                } else {
                    int SB = ((Hitter)player.getValue()).getSB();
                    return new SimpleIntegerProperty(SB);
                }
            }
        });
        
        BA_WHIPCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, Double>, ObservableValue<Double>>() {

           @Override
           public ObservableValue call(TableColumn.CellDataFeatures<Player, Double> player) {
               if (player.getValue() instanceof Pitcher) {
                   double WHIP = ((Pitcher)player.getValue()).getWHIP();
                   return new SimpleStringProperty(WHIP_FORMAT.format(WHIP));
               } else {                    
                   double BA = ((Hitter)player.getValue()).getBA();
                   return new SimpleStringProperty(BA_FORMAT.format(BA).substring(BA_FORMAT.format(BA).indexOf('.')));
               }
           }
       });        
        
        // We want the names to be compared ignoring case so that lowercase names aren't by default "greater than" uppercase names
        firstNameCol.setComparator((name1, name2) -> ((String)name1).compareToIgnoreCase((String)name2));
        lastNameCol.setComparator((name1, name2) -> ((String)name1).compareToIgnoreCase((String)name2));
        
        // Finally add all the columns to the Player pool Table        
        playerPoolTable.getColumns().addAll(firstNameCol, lastNameCol, proTeamCol, 
                                            posCol, birthYearCol, R_WCol, HR_SVCol,
                                            RBI_KCol, SB_ERACol, BA_WHIPCol, estValueCol,
                                            notesCol); 
        
        // The table contents is binded to the list of all the players in the
        // Player pool 
        playerPoolTable.setItems(dataManager.getDraftModel().getPlayerPool()); 
    }

    public Pane getPane() {
        return playerScreenPane;
    }
    
    private void setColTextToDefaults() {
        R_WCol.setText(COL_R_W);
        HR_SVCol.setText(COL_HR_SV);
        RBI_KCol.setText(COL_RBI_K);
        SB_ERACol.setText(COL_SB_ERA);
        BA_WHIPCol.setText(COL_BA_WHIP);
    }
    
    private void setColTextToHittersStats() {
        R_WCol.setText("R");
        HR_SVCol.setText("HR");
        RBI_KCol.setText("RBI");
        SB_ERACol.setText("SB");
        BA_WHIPCol.setText("BA");
    }
    
    private void setColTextToPitchersStats() {
        R_WCol.setText("W");
        HR_SVCol.setText("SV");
        RBI_KCol.setText("K");
        SB_ERACol.setText("ERA");
        BA_WHIPCol.setText("WHIP");
    }    
    
    // init a label and set it's stylesheet class
    private Label initLabel(String labelText, String styleClass) {
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    // Init a label and put it in a toolbar
    private Label initChildLabel(Pane container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    // Init a label and set it's stylesheet class
    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    private void initButtons() {        
        addPlayerButton = initButton(WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removePlayerButton = initButton(WDK_PropertyType.DELETE_ICON, WDK_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
    }
    
    private Button initButton(WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        
        Button toolbarButton = new Button();
        toolbarButton.setDisable(disabled);
        toolbarButton.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        toolbarButton.setTooltip(buttonTooltip);
        
        return toolbarButton;
    }
}

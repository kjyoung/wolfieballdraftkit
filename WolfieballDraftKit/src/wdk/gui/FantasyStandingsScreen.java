package wdk.gui;

import java.text.DecimalFormat;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.data.Team;
import static wdk.gui.WDK_GUI.CLASS_TABLE;

/**
 * This screen lets one compare teams and sorts them all together in one table
 * according to different criteria
 * 
 * @author Kevin
 */
public class FantasyStandingsScreen {
    
    // These will format the numbers of certain player stats so they look 
    // nice in the table
    DecimalFormat BA_FORMAT; 
    DecimalFormat WHIP_FORMAT; 
    DecimalFormat ERA_FORMAT; 
    
    BorderPane fantasyStandingsScreenPane;
    
    private VBox topWorkspacePane;
    
    public static TableView<Team> teamsTable;
    
    VBox teamsBox;
    
    TableColumn teamNameCol, playersNeededCol, purseCol, moneyPerPlayerCol, RCol,
            HRCol, RBICol, SBCol, BACol, WCol, SVCol, KCol, ERACol, WHIPCol, 
            totalPointsCol;
            
    DraftModel draft;
    
    Label draftScreenHeadingLabel;
    
    final String COL_TEAM_NAME = "Team Name";
    final String COL_PLAYERS_NEEDED = "Players Needed";
    final String COL_PURSE = "$ Left";
    final String COL_MONEY_PER_PLAYER = "$ PP";
    final String COL_R = "R";
    final String COL_HR = "HR";
    final String COL_RBI = "RBI";
    final String COL_SB = "SB";
    final String COL_BA = "BA";
    final String COL_W = "W";
    final String COL_SV = "SV";
    final String COL_K = "K";
    final String COL_ERA = "ERA";
    final String COL_WHIP = "WHIP";
    final String COL_TOTAL_POINTS = "Total Points";
    
    public FantasyStandingsScreen(DraftModelDataManager dataManager) {
       fantasyStandingsScreenPane = new BorderPane();
       draft = dataManager.getDraftModel();
       BA_FORMAT = new DecimalFormat(".000");
       WHIP_FORMAT = new DecimalFormat("0.00");
       ERA_FORMAT = new DecimalFormat("0.00");
    }
    
    public void initGUI() { 
        
       initTeamsTable();
       initWorkspace();
    }
    
    private void initTeamsTable() {
        // Initialize the fantasy teams table and make it pretty
        teamsTable = new TableView();  
        teamsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        teamsTable.setPrefHeight(600);
        teamsTable.getStyleClass().add(CLASS_TABLE);
        
        
        // Initialize the pane that will hold the table and make it pretty
        teamsBox = new VBox();
        teamsBox.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        VBox.setVgrow(teamsBox, Priority.ALWAYS);    
        
        // Label the starting lineup pane
        Label fantasyTeamsLabel = initLabel(WDK_PropertyType.FANTASY_STANDINGS_LABEL, WDK_GUI.CLASS_SMALLER_HEADING_LABEL); 
        
        // Add the table to the pane
        teamsBox.getChildren().addAll(fantasyTeamsLabel, teamsTable);        
        
        // Initialize all of the Table columns            
        teamNameCol = new TableColumn(COL_TEAM_NAME);
        playersNeededCol = new TableColumn(COL_PLAYERS_NEEDED);
        purseCol = new TableColumn(COL_PURSE);
        moneyPerPlayerCol = new TableColumn(COL_MONEY_PER_PLAYER);
        RCol = new TableColumn(COL_R);
        HRCol = new TableColumn(COL_HR);
        RBICol = new TableColumn(COL_RBI);
        SBCol = new TableColumn(COL_SB);
        BACol = new TableColumn(COL_BA);
        WCol = new TableColumn(COL_W);
        SVCol = new TableColumn(COL_SV);
        KCol = new TableColumn(COL_K);
        ERACol = new TableColumn(COL_ERA);
        WHIPCol = new TableColumn(COL_WHIP);
        totalPointsCol = new TableColumn(COL_TOTAL_POINTS);        
        
        // Now link the columns to the data
        teamNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        playersNeededCol.setCellValueFactory(new PropertyValueFactory<String, String>("playersNeededInRoster"));
        purseCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("purse"));
        
        WHIPCol.setCellValueFactory(new Callback<CellDataFeatures<Team, Double>, ObservableValue<Double>>() {

           @Override
           public ObservableValue call(CellDataFeatures<Team, Double> team) {
                   double avgWHIP = team.getValue().getAvgWHIP();
                   return new SimpleStringProperty(WHIP_FORMAT.format(avgWHIP));
           }
        });      
        
        BACol.setCellValueFactory(new Callback<CellDataFeatures<Team, Double>, ObservableValue<Double>>() {

           @Override
           public ObservableValue call(CellDataFeatures<Team, Double> team) {
                   double avgBA = team.getValue().getAvgBA();
                   return (new SimpleStringProperty(BA_FORMAT.format(avgBA).substring(BA_FORMAT.format(avgBA).indexOf('.'))));
           }
        }); 
        
        ERACol.setCellValueFactory(new Callback<CellDataFeatures<Team, Double>, ObservableValue<Double>>() {

           @Override
           public ObservableValue call(CellDataFeatures<Team, Double> team) {
                   double avgERA = team.getValue().getAvgERA();
                   return new SimpleStringProperty(ERA_FORMAT.format(avgERA));
           }
        });
        
        moneyPerPlayerCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("pp"));
        RCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalR"));
        HRCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalHR")); 
        RBICol.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalRBI"));
        SBCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalSB"));
        WCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalW"));
        SVCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalSV"));
        KCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalK")); 
        totalPointsCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalPoints")); 
        
        // We want the names to be compared ignoring case so that lowercase names aren't by default "greater than" uppercase names
        teamNameCol.setComparator((name1, name2) -> ((String)name1).compareToIgnoreCase((String)name2));
        
        // Finally add all the columns to the Player pool Table        
        teamsTable.getColumns().addAll(teamNameCol, playersNeededCol, purseCol, moneyPerPlayerCol, RCol,
            HRCol, RBICol, SBCol, BACol, WCol, SVCol, KCol, ERACol, WHIPCol, 
            totalPointsCol);
        
        // The combo box will hold all the fantasy teams that are part of the draft        
        teamsTable.setItems(draft.getFantasyTeams());
    }
    
    private void initWorkspace() {
        initTopWorkspace();
        
        fantasyStandingsScreenPane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        
        fantasyStandingsScreenPane.setTop(topWorkspacePane);
        fantasyStandingsScreenPane.setCenter(teamsBox);
    }
    
    private void initTopWorkspace() {
        
        topWorkspacePane = new VBox();
        topWorkspacePane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        
        GridPane topToolbarPane = new GridPane();
        topToolbarPane.setMinWidth(100);
        
        // The screen label
        draftScreenHeadingLabel = initChildLabel(topWorkspacePane, WDK_PropertyType.FANTASY_STANDINGS_HEADING_LABEL, WDK_GUI.CLASS_HEADING_LABEL);
    }
    
    public Pane getPane() {
        return fantasyStandingsScreenPane;
    }
    
    // Init a label and put it in a toolbar
    private Label initChildLabel(Pane container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
     // Init a label and set it's stylesheet class
    private Label initLabel(String labelText, String styleClass) {
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    // Init a label and set it's stylesheet class
    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
}

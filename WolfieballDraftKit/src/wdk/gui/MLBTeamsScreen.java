package wdk.gui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.data.Player;
import wdk.data.ProfessionalTeam;
import static wdk.gui.WDK_GUI.CLASS_TABLE;
import static wdk.gui.WDK_GUI.COL_FIRST_NAME;
import static wdk.gui.WDK_GUI.COL_LAST_NAME;
import static wdk.gui.WDK_GUI.COL_POSITIONS;

/**
 * This screen lets one view all the players in the draft on each MLB team
 * where the user would select the team to view via a combo box
 * 
 * @author Kevin
 */
public class MLBTeamsScreen {
    
    BorderPane MLBTeamsScreenPane;
    
    private VBox topWorkspacePane;
    
    ComboBox<ProfessionalTeam> MLBTeamSelectionComboBox;
    
    TableView<Player> MLBPlayersTable;
    
    VBox MLBPlayersBox;
    
    TableColumn firstNameCol, lastNameCol, positionsCol;
    
    HBox MLBTeamPane;
    
    DraftModel draft;
    
    // This will make it easier to filter out the players we don't want to show
    // based on the professional team roster selected
    FilteredList<Player> filteredPlayerList;
    
    public MLBTeamsScreen(DraftModelDataManager dataManager) {
       draft = dataManager.getDraftModel();
       MLBTeamsScreenPane = new BorderPane();
       
       // Wrap the ObservableList in a FilteredList (initially display all data).
       filteredPlayerList = new FilteredList<>(dataManager.getDraftModel().
                                                     getPlayerPool(), p -> true);
    }
    
    public void initGUI() {        
        
       initMLBTeamTable();
       
       initWorkspace();
       
       initEventHandlers();
    }
    
    private void initEventHandlers() {
        MLBTeamSelectionComboBox.valueProperty().addListener(new ChangeListener<ProfessionalTeam>() {

            @Override
            public void changed(ObservableValue<? extends ProfessionalTeam> observable, ProfessionalTeam oldTeam, ProfessionalTeam newTeam) {
                // Finally add the sorted (and filtered) data to the table 
                filteredPlayerList.setPredicate(player -> {
                    return player.getProTeam().equals(newTeam.toString());
                });
                MLBPlayersTable.setItems(filteredPlayerList);                
            }
        });
    }
    
    private void initMLBTeamTable() {
        // Initialize the starting lineup table and make it pretty
        MLBPlayersTable = new TableView();  
        MLBPlayersTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        MLBPlayersTable.setPrefHeight(600);
        MLBPlayersTable.getStyleClass().add(CLASS_TABLE);
        
        // Initialize the pane that will hold the table and make it pretty
        MLBPlayersBox = new VBox();
        MLBPlayersBox.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        VBox.setVgrow(MLBPlayersBox, Priority.ALWAYS);    
        
        // Label the starting lineup pane
        Label MLBPlayersLabel = initLabel(WDK_PropertyType.MLB_PLAYERS_LABEL, WDK_GUI.CLASS_SMALLER_HEADING_LABEL); 
        
        // Add the table to the pane
        MLBPlayersBox.getChildren().addAll(MLBPlayersLabel, MLBPlayersTable);        
        
        // Initialize all of the Table columns
        firstNameCol = new TableColumn(COL_FIRST_NAME);
        lastNameCol = new TableColumn(COL_LAST_NAME);
        positionsCol = new TableColumn(COL_POSITIONS);
        
        // Now link the columns to the data
        firstNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        positionsCol.setCellValueFactory(new PropertyValueFactory<String, String>("qualifiedPositions")); 
        
        // We want the names to be compared ignoring case so that lowercase names aren't by default "greater than" uppercase names
        firstNameCol.setComparator((name1, name2) -> ((String)name1).compareToIgnoreCase((String)name2));
        lastNameCol.setComparator((name1, name2) -> ((String)name1).compareToIgnoreCase((String)name2));
        
        // Finally add all the columns to the Player pool Table        
        MLBPlayersTable.getColumns().addAll(firstNameCol, lastNameCol, positionsCol);
    }
    
    private void initWorkspace() {
        initTopWorkspace();        
        
        MLBTeamsScreenPane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);        
        MLBTeamsScreenPane.setTop(topWorkspacePane);
        MLBTeamsScreenPane.setCenter(MLBPlayersBox);
    }
    
    private void initMLBTeamSelectionComboBox() {
        
        MLBTeamSelectionComboBox = initHBoxComboBox(MLBTeamPane);   
        MLBTeamSelectionComboBox.setPrefWidth(200);   
        
        // The combo box will hold all the fantasy teams that are part of the draft
        MLBTeamSelectionComboBox.setItems(FXCollections.observableArrayList(ProfessionalTeam.values()));
    }
    
    private void initTopWorkspace() {
        
        topWorkspacePane = new VBox();
        topWorkspacePane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        
        GridPane topToolbarPane = new GridPane();
        topToolbarPane.setMinWidth(100);
        
        // The screen label
        Label MLBScreenHeadingLabel = initChildLabel(topWorkspacePane, WDK_PropertyType.MLB_TEAMS_HEADING_LABEL, WDK_GUI.CLASS_HEADING_LABEL);
        
        // Create the area for the fantasy team control
        MLBTeamPane = new HBox();
        Label selectTeamLabel = initLabel(WDK_PropertyType.SELECT_MLB_TEAM_LABEL, WDK_GUI.CLASS_SUBHEADING_LABEL);    
        HBox.setMargin(selectTeamLabel, new Insets(0, 25, 0, 0));
        MLBTeamPane.getChildren().addAll(selectTeamLabel);
        
        // Create the team selection combo box
        initMLBTeamSelectionComboBox();
        
        topWorkspacePane.getChildren().addAll(MLBTeamPane);
    }
    
    // Init a combo box and put it in a HBox
    private ComboBox initHBoxComboBox(HBox container) {
        ComboBox comboBox = new ComboBox();
        container.getChildren().add(comboBox);
        return comboBox;
    }
    
    public Pane getPane() {
        return MLBTeamsScreenPane;
    }
    
    // Init a label and put it in a toolbar
    private Label initChildLabel(Pane container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
     // Init a label and set it's stylesheet class
    private Label initLabel(String labelText, String styleClass) {
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    // Init a label and set it's stylesheet class
    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
}

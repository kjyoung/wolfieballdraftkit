package wdk.gui;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import wdk.WDK_PropertyType;
import static wdk.WDK_StartupConstants.PATH_IMAGES;
import wdk.controller.PlayerController;
import wdk.data.DraftEvent;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import static wdk.gui.WDK_GUI.CLASS_TABLE;

/**
 * This screen lets one view all the players drafted so far, and lets one automate
 * player selection
 * 
 * @author Kevin
 */
public class DraftScreen {
    
    
    public static TableView<DraftEvent> draftOrderTable;
    
    VBox draftOrderBox;
    
    TableColumn firstNameCol, lastNameCol, positionsCol;
    
    HBox buttonPane;
    
    DraftModel draft;
    
    BorderPane draftScreenPane;
    
    private VBox topWorkspacePane;
    
    Label draftScreenHeadingLabel;
    
    TableColumn pickCol, firstCol, lastCol, teamCol, contractCol, salaryCol;
    
    public static Button pickPlayerButton;
    public static Button startAutoDraftButton;
    public static Button pauseAutoDraftButton;
    
    final String COL_PICK = "Pick #";
    final String COL_FIRST = "First";
    final String COL_LAST = "Last";
    final String COL_TEAM = "Team";
    final String COL_CONTRACT = "Contract";
    final String COL_SALARY = "Salary ($)";
   
    ObservableList<DraftEvent> draftOrderList;
    
    DraftModelDataManager dataManager;
    
    
    public DraftScreen(DraftModelDataManager dataManager) {
        this.draft = dataManager.getDraftModel();
        this.dataManager = dataManager;
        draftScreenPane = new BorderPane();
        
        
    }
    
    public void initGUI() { 
        
       initButtons();
       
       initWorkspace();
       
       initDraftOrderTable();
       
       initWorkspace();
       
       initEventHandlers();
    }
    
    private void initEventHandlers() {
        
        PlayerController playerController = new PlayerController(dataManager);
        
        pickPlayerButton.setOnAction(e -> playerController.addRandomPlayerToDraft());
        startAutoDraftButton.setOnAction(e -> playerController.startAutomatedDraft());
        pauseAutoDraftButton.setOnAction(e -> playerController.pauseAutomatedDraft());
        
    }
    
    private void initButtons() {        
        pickPlayerButton = initButton(WDK_PropertyType.PICK_ICON, WDK_PropertyType.PICK_PLAYER_TOOLTIP, false);
        startAutoDraftButton = initButton(WDK_PropertyType.START_ICON, WDK_PropertyType.START_AUTO_DRAFT_TOOLTIP, false);
        pauseAutoDraftButton = initButton(WDK_PropertyType.PAUSE_ICON, WDK_PropertyType.PAUSE_AUTO_DRAFT_TOOLTIP, true);
    }
    
    private Button initButton(WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disable) {
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        
        Button toolbarButton = new Button();
        toolbarButton.setGraphic(new ImageView(buttonImage));
        toolbarButton.setDisable(disable);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        toolbarButton.setTooltip(buttonTooltip);
        
        return toolbarButton;
    }
    
    private void initWorkspace() {
        initTopWorkspace();
        
        draftScreenPane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        
        draftScreenPane.setTop(topWorkspacePane);
        draftScreenPane.setCenter(draftOrderBox);
    }
    
    private void initTopWorkspace() {
        
        topWorkspacePane = new VBox();
        topWorkspacePane.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        
        // The screen label 
        draftScreenHeadingLabel = initChildLabel(topWorkspacePane, WDK_PropertyType.DRAFT_SCREEN_HEADING_LABEL, WDK_GUI.CLASS_HEADING_LABEL);
        
        // Holds the buttons
        HBox topToolbarBox = new HBox();
        topToolbarBox.setMinWidth(100);
        topToolbarBox.getChildren().addAll(pickPlayerButton, startAutoDraftButton, pauseAutoDraftButton);
        
        HBox.setMargin(pickPlayerButton, new Insets(0, 15, 0,0));
        HBox.setMargin(startAutoDraftButton, new Insets(0, 15, 0, 0));
        
        topWorkspacePane.getChildren().add(topToolbarBox);
    }
    
    public Pane getPane() {
        return draftScreenPane;
    }
    
    // Init a label and put it in a toolbar
    private Label initChildLabel(Pane container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    // Init a label and set it's stylesheet class
    private Label initLabel(String labelText, String styleClass) {
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    // Init a label and set it's stylesheet class
    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }   
    
    private void initDraftOrderTable() {
        // Initialize the starting lineup table and make it pretty
        draftOrderTable = new TableView();  
        draftOrderTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        draftOrderTable.setPrefHeight(600);
        draftOrderTable.getStyleClass().add(CLASS_TABLE);
        
        // Initialize the pane that will hold the table and make it pretty
        draftOrderBox = new VBox();
        draftOrderBox.getStyleClass().add(WDK_GUI.CLASS_BORDERED_PANE);
        VBox.setVgrow(draftOrderBox, Priority.ALWAYS);    
        
        // Label the starting lineup pane
        Label MLBPlayersLabel = initLabel(WDK_PropertyType.DRAFT_ORDER_LABEL, WDK_GUI.CLASS_SMALLER_HEADING_LABEL); 
        
        // Add the table to the pane
        draftOrderBox.getChildren().addAll(MLBPlayersLabel, draftOrderTable);        
        
        // Initialize all of the Table columns    
        pickCol = new TableColumn(COL_PICK);
        firstCol = new TableColumn(COL_FIRST);
        lastCol = new TableColumn(COL_LAST);
        teamCol = new TableColumn(COL_TEAM);
        contractCol = new TableColumn(COL_CONTRACT);
        salaryCol = new TableColumn(COL_SALARY);
        
        // Now link the columns to the data
        pickCol.setCellValueFactory(new PropertyValueFactory<String, Integer>("pickNumber")); 
        firstCol.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastCol.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        teamCol.setCellValueFactory(new PropertyValueFactory<String, String>("teamName"));
        contractCol.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        salaryCol.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));        
        
        // Finally add all the columns to the Player pool Table        
        draftOrderTable.getColumns().addAll(pickCol, firstCol, lastCol, teamCol, 
                contractCol, salaryCol);
        
        draftOrderTable.setItems(draft.getDraftOrder());
    }
    
}
package wdk;

/**
 * This class stores all the constants used by the Wolfieball Draft Kit application 
 * at startup, which means before the user interface is even loaded. This mostly 
 * means how to find files for initializing the application, like properties.xml.
 * 
 * @author Kevin
 */
public class WDK_StartupConstants {
    
    // The name of the file that we will get our properties from
    public static final String PROPERTIES_FILE_NAME = "properties.xml";
    
    // We verify the format of our properties file with this scheme
    public static final String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";   
    
    // Paths to files we need for running, such as data and css files
    public static final String PATH_IMAGES = "./images/";
    public static final String PATH_DATA = "./data/";
    public static final String PATH_DRAFTS = PATH_DATA + "drafts/";
    public static final String PATH_PLAYER_PROFILE_IMAGES = PATH_IMAGES + "players/";
    public static final String PATH_FLAG_IMAGES = PATH_IMAGES + "flags/";
    public static final String PATH_PLAYERS = PATH_DATA + "players/";   
    public static final String PATH_HITTERS = PATH_DATA + "hitters/"; 
    public static final String PATH_CSS = "wdk/css/";   
        
    // The name of the data files we will load at startup
    public static final String JSON_FILE_PATH_PITCHERS = PATH_PLAYERS + "Pitchers.json";
    public static final String JSON_FILE_PATH_HITTERS = PATH_PLAYERS + "Hitters.json";
    
    // Error message associated with properties file loading errors
    public static String PROPERTIES_FILE_ERROR_MESSAGE = "Error Loading properties.xml";
    
    // Error dialog control
    public static String CLOSE_BUTTON_LABEL = "Close";    
}

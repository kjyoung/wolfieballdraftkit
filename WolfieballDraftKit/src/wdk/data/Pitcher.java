package wdk.data;

import java.util.Comparator;
import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * A Pitcher is a Player that has statistics which only a baseball pitcher has,
 * such as Innings Pitched.
 * 
 * @author Kevin
 */
public class Pitcher extends Player {
    
    SimpleDoubleProperty IP, // "Innings Pitched", used to measure how much this pitcher played in a season
            WHIP, // calculated as (Walks + Hits) / Innings Pitched 
            ERA; // "Earned Run Average", calculated as (Earned Runs * 9) / Innings Pitched
    
    SimpleIntegerProperty ER, // "Earned Runs"
            W, // "Wins", earned by pitching the player's team to victory
            SV, // "Saves"
            BB, // "Bases on Balls (Walks)", earned by pitching four balls to the same hitter on the same play
            K; // "Strikeouts", earned by getting the hitter out without the hitter putting the ball in play
    
    /**
     *
     * @param proTeam
     * @param lastName
     * @param firstName
     * @param H
     * @param notes
     * @param IP
     * @param ER
     * @param W
     * @param qualifiedPositions
     * @param SV
     * @param yearOfBirth
     * @param nationOfBirth
     * @param BB
     * @param K
     */
    public Pitcher(String  proTeam, String lastName, String firstName, int H, 
            String notes, int yearOfBirth, String nationOfBirth, String qualifiedPositions,
            double IP, int ER, int W, int SV, int BB, int K) {        
        super(proTeam, lastName, firstName, H, notes, yearOfBirth, nationOfBirth);
        
        this.qualifiedPositions = new SimpleStringProperty(qualifiedPositions);
        
        this.IP = new SimpleDoubleProperty(IP);
        this.ER = new SimpleIntegerProperty(ER);
        this.W = new SimpleIntegerProperty(W);
        this.SV = new SimpleIntegerProperty(SV);
        this.BB = new SimpleIntegerProperty(BB);
        this.K = new SimpleIntegerProperty(K);
        
        this.WHIP = new SimpleDoubleProperty(0);
        this.ERA = new SimpleDoubleProperty(0);
    }    
    
    public Pitcher(String  proTeam, String lastName, String firstName,
                   List<String> qualifiedPositionsList) {        
        super(proTeam, lastName, firstName);
        
        this.qualifiedPositionsList = qualifiedPositionsList;        
        initDefaultPitcher();
    }
    
    // This is good for constructing a pitcher placeholder
    public Pitcher(String position) {
        super("","","");
        this.position = new SimpleStringProperty(position);
        initDefaultPitcher();
    }
    
    private void initDefaultPitcher() {
        this.qualifiedPositions = new SimpleStringProperty("");        
        this.IP = new SimpleDoubleProperty(0);
        this.ER = new SimpleIntegerProperty(0);
        this.W = new SimpleIntegerProperty(0);
        this.SV = new SimpleIntegerProperty(0);
        this.BB = new SimpleIntegerProperty(0);
        this.K = new SimpleIntegerProperty(0); 
        this.WHIP = new SimpleDoubleProperty(0);
        this.ERA = new SimpleDoubleProperty(0);
    }
    
    public void setWHIP(double WHIP) {
        this.WHIP.set(WHIP);
    }
    
    /**
     * WHIP is calculated by (Walks + Hits)/Innings Pitched
     * 
     * @param W
     * @param H
     * @param IP 
     */
    public void setWHIP(int W, int H, double IP) {
        // Make sure we are not dividing by zero
        if (IP == 0) {                
            this.WHIP.set(0);
        } else {
            this.WHIP.set((double)(W + H)/IP); 
        }
    }
    
    public void setERA(double ERA) {
        this.ERA.set(ERA);
    }
    
    /**
     * ERA is calculated by 9 × Earned Runs Allowed / Innings Pitched
     * 
     * @param IP
     * @param ER 
     */
    public void setERA(double IP, int ER) {
        // Make sure we are not dividing by zero
        if (IP == 0) {                
            this.ERA = new SimpleDoubleProperty(0);
        } else { 
            this.ERA = new SimpleDoubleProperty(9.0 * (ER/IP)); 
        }
        
    }
    
    public SimpleIntegerProperty getWProperty() {
        return W;
    }
    
    public double getERA() {
        return ERA.get();
    }
    
    public double getIP() {
        return IP.get();
    }
    
    public int getER() {
        return ER.get();
    }
    
    public int getW() {
        return W.get();
    }
    
    public int getSV() {
        return SV.get();
    }
    
    public int getBB() {
        return BB.get();
    }
    
    public int getK() {
        return K.get();
    }
    
    public double getWHIP() {
        return WHIP.get();
    }
    
    // Lower is better
    public static Comparator<Pitcher> COMPARE_BY_W = (Pitcher p1, Pitcher p2) ->
            p1.getW() - p2.getW();
    
    // Lower the better
    public static Comparator<Pitcher> COMPARE_BY_ERA = (Pitcher p1, Pitcher p2) ->
            Double.compare(p1.getERA(), p2.getERA());
    
    // Lower the better
    public static Comparator<Pitcher> COMPARE_BY_WHIP = (Pitcher p1, Pitcher p2) ->
            Double.compare(p1.getWHIP(), p2.getWHIP());
    
    public static Comparator<Pitcher> COMPARE_BY_SV = (Pitcher p1, Pitcher p2) ->
            p2.getSV() - p1.getSV();
    
    public static Comparator<Pitcher> COMPARE_BY_K= (Pitcher p1, Pitcher p2) ->
            p2.getK() - p1.getK();

    @Override
    public String toString() {
        
        String pitcherString = "PITCHER\n";
        pitcherString += super.toString();
        pitcherString += "IP: " + IP
                + "\nER: " + ER
                + "\nWHIP: " + WHIP
                + "\nSV: " + SV
                + "\nBB: " + BB
                + "\nK: " + K;
        
       return pitcherString;                       
    }
    
}

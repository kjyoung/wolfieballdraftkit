package wdk.data;

/**
 * This class manages a DraftModel, which means it knows how to
 * reset one with default values. Whenever any other class tries
 * to access the data of the DraftModel being worked on,
 * it should access the DraftModel from this class.
 * 
 * @author Kevin
 */
public class DraftModelDataManager {
    
    // This is the UI, which must be updated
    // whenever our model's data changes
    DraftModelDataView view;
    
    // The DraftModel which this class will manage
    DraftModel draftModel;
    
    public DraftModelDataManager(DraftModelDataView initView) {
        view = initView;
        draftModel = new DraftModel();
    }

    /**
    * At the time of writing this program, Java 8 doesn't update lists in GUIs,
    * but a workaround is done by adding and removing to the list which then 
    * updates the GUI.
    */
    public void refreshDraft() {
        draftModel.addFantasyTeam(null);
        draftModel.removeTeam(null);
    }
    
    public void reset() {
        
        // Clear all of the values of the draft
        draftModel.clearAllValues();
        draftModel.calculateEstimatedValues();
        draftModel.calculateTotalPoints();
        
        // The player pool is now the same as the pool loaded from the files
        // at the beginning of the program
        draftModel.resetPlayerPool();        
        
        // Force the UI to reload the updated DraftModel
        view.reloadDraftModel(draftModel);  
    }
    
    public DraftModel getDraftModel() {
        return draftModel;
    }
    
}

package wdk.data;

/**
 *
 * @author Kevin
 */
public class DraftEvent {
    
    int pickNumber;
    String contract;
    int salary;
    String teamName;
    String firstName;
    String lastName;
    String eventString = "";
    
    public DraftEvent() {
        
    }
    
    public String getFullName() {
        return firstName + " " + lastName;
    }
    
    public void setPickNumber(int pickNumber) {
        this.pickNumber = pickNumber;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    // Player was added to team one
    public DraftEvent(int pickNumber, String firstName, String lastName, String teamName, String contract, int salary) {
        this.pickNumber = pickNumber;
        this.contract = contract;
        this.salary = salary;
        this.firstName = firstName;
        this.lastName = lastName;
        this.teamName = teamName;
//   
    }
//    
//    public DraftEvent(int pickNumber, Player player) {
//        this.contract = player.getContract();
//        this.salary = player.getSalary();
//        this.firstName = player.getFirstName();
//        this.lastName = player.getLastName();
//        this.teamName = player.getT;
//    }

    public int getPickNumber() {
        return pickNumber;
    }

    public String getContract() {
        return contract;
    }

    public int getSalary() {
        return this.salary;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEventString() {
        return eventString;
    }
    
    public DraftEvent(String eventString) {
        pickNumber++;
        this.eventString = eventString;
    }
    
    @Override
    public String toString() {        
        return eventString;        
    }
}

package wdk.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class holds all the data for a fantasy baseball team. This class
 * will hold methods to add to the roster and the taxi squad of a fantasy 
 * baseball team.
 * 
 * @author Kevin
 */
public class Team {
    
    private String name;    
    private String owner;
    
    int rosterSize;
    
    int purse;
    int pp; // the money per player for starting players still required
    int playersNeededInRoster;
    int totalR;
    int totalHR;
    int totalRBI;
    int totalSB;
    
    double totalBA;
    double avgBA;
    
    int totalW;
    int totalSV;
    int totalK;
    
    double totalERA;
    double avgERA;
    
    double totalWHIP;
    double avgWHIP;
    
    int totalPoints;
    
    int hittersNeededInRoster = 14;
    int pitchersNeededInRoster = 9;
    
    // Arraylist because we want to keep track of the order that players are
    // drafted
    ObservableList<Player> roster;
    
    // Arraylist because we want to keep track of the order that players are
    // drafted
    ObservableList<Player> taxiSquad;
    
    // A list of positions this team needs to complete
    ObservableList<String> observableUnfilledPositions;
    
    // A list of the contracts that a player can have on this team
    ObservableList<String> availableContracts;
    
    List<String> unfilledPositions;
    List<Player> playerPlaceholders;
    List<String> fixedPositions;
    String[] allPossibleContracts;
    
    // Keep a count of all positions that need more than one position filled 
    // so we can keep track of which of the duplicate positions we are adding
    // a player to
    int PCount = 0;
    int OFCount = 0;
    int CCount = 0;
    
    int hitterCount = 0;
    int pitcherCount = 0;
    
    int taxiSquadCount = 0;
    
    int sizeOfRoster;
    
    public Team(String name) {
        this.name = name;        
        initTeam();        
    }
    
    public Team() {        
        initTeam();
    } 
    
    private void initTeam() {
    
        totalPoints = 0;
        purse = 260;
        pp = 0;
        totalR = 0;
        totalHR = 0;
        totalRBI = 0;
        totalSB = 0;
        avgBA = 0;
        totalW = 0;
        totalSV = 0;
        totalK = 0;
        avgERA = 0;
        avgWHIP = 0;        
        
        roster = FXCollections.observableArrayList();
        
        // Only players with contract X. Their stats don't count for a team
        // performance analysis
        taxiSquad = FXCollections.observableArrayList();
        availableContracts = FXCollections.observableArrayList();
        
        // S2 = Player has 2 more years after this year.
        // S1 = Player has 1 more year. 
        // X  = Player has 0 more years. 
        allPossibleContracts = new String[]{"S1", "S2", "X"};   
        availableContracts.addAll(Arrays.asList(allPossibleContracts));
        
        playerPlaceholders = DraftModel.getFantasyTeamPlaceholders();        
        roster.addAll(playerPlaceholders);      
        
        fixedPositions = DraftModel.getFixedFantasyTeamPositions();       
        sizeOfRoster = fixedPositions.size();
        
        unfilledPositions = new ArrayList<String>(fixedPositions); 
        observableUnfilledPositions = FXCollections.observableArrayList(unfilledPositions);
        
        playersNeededInRoster = unfilledPositions.size();
    }
    
    public void decrementPurse(int amount) {
        purse -= amount; 
    }
    
    public void setPurse(int amount) {
        purse = amount;
    }
    
    public void incrementPurse(int amount) {
        purse += amount;
    }
    
    // purse - salary / players needed in the roster - 1
    public boolean canAffordPlayer(int salary) {
        if (playersNeededInRoster == 1 || playersNeededInRoster == 0) {
            return purse - salary >= 0;
        } 
        
        
        
        return ((purse - salary) / (playersNeededInRoster - 1)) >= 1;
    }
    
    // purse - salary / players needed in the roster - 1
    public boolean canAffordTaxiPlayer() {
        if (taxiSquad.size() == 8) {
            return purse - 1 >= 0;
        } 
        
        return ((purse - 1) / ((9 - taxiSquad.size()) - 1)) >= 1;
    }
    
    public boolean addToTaxiSquad(Player player) {
        if (!player.getContract().equals("X") || hasFilledTaxiSquad()) {
            return false;
        }
        
        taxiSquadCount++;
        taxiSquad.add(player);
        return true;
    }
    
    public void removeFromTaxiSquad(Player player) {
        
        taxiSquadCount--;
        taxiSquad.remove(player);
    }
    
    public boolean hasFilledTaxiSquad() {
        return taxiSquadCount == 9;
    }
    
    public int getPlayersNeededInRoster() {
        return this.playersNeededInRoster;
    }

    public int getPurse() {
        return purse;
    }

    public int getPp() {
        return pp;
    }

    public int getTotalR() {
        return totalR;
    }

    public int getTotalHR() {
        return totalHR;
    }

    public int getTotalRBI() {
        return totalRBI;
    }

    public int getTotalSB() {
        return totalSB;
    }

    public double getAvgBA() {
        return avgBA;
    }

    public int getTotalW() {
        return totalW;
    }

    public int getTotalSV() {
        return totalSV;
    }

    public int getTotalK() {
        return totalK;
    }

    public double getAvgERA() {
        return avgERA;
    }

    public double getAvgWHIP() {
        return avgWHIP;
    }

    public int getTotalPoints() {
        return totalPoints;        
    }

    public List<String> getFixedPositions() {
        return fixedPositions;
    }

    public String[] getAllPossibleContracts() {
        return allPossibleContracts;
    }
    
    public ObservableList<String> getObservableUnfilledPositions() {
        return observableUnfilledPositions;
    }
    
    public List<String> getUnfilledPositions() {
        return unfilledPositions;
    }
    
    public ObservableList<String> getAvailableContracts() {
        return availableContracts;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setOwner(String owner) {
        this.owner = owner;
    }
    
    public String getOwner() {
        return this.owner;
    }
    
    public String getName() {
        return this.name;
    }
    
    public ObservableList<Player> getRoster() {
        return roster;
    }    
    
    public ObservableList<Player> getTaxiSquad() {
        return taxiSquad;
    }
    
    private double calculateAvgBA() {
        if (hitterCount == 0) {
            return 0;
        }
        
        return totalBA / hitterCount;
    }
    
    private double calculateAvgERA() {
        if (pitcherCount == 0) {
            return 0;
        }
        
        return totalERA / pitcherCount;
    }
    
    private double calculateAvgWHIP() {
        return totalWHIP / pitcherCount;
    }
    
    private int calculatePP() {
        return (hasFilledRoster()) ? -1 : (purse / playersNeededInRoster) + 1;
    }
    
    public boolean hasfilledPitchersForRoster() {
        return pitchersNeededInRoster == 0;
    }
    
    public boolean hasfilledHittersForRoster() {
        return hittersNeededInRoster == 0;
    }
    
    public boolean hasFilledRoster() {
        return playersNeededInRoster == 0;
    }
    
    private void increaseTeamStats(Player newPlayer) {
        playersNeededInRoster -= 1;         
        pp = calculatePP();
        if (newPlayer instanceof Hitter) {
            hitterCount += 1; 
            hittersNeededInRoster--;
            totalR += ((Hitter)newPlayer).getR();
            totalHR += ((Hitter)newPlayer).getHR();
            totalRBI += ((Hitter)newPlayer).getRBI();
            totalSB += ((Hitter)newPlayer).getSB();
            totalBA += ((Hitter)newPlayer).getBA();
            avgBA = calculateAvgBA();                
        } else if (newPlayer instanceof Pitcher) {
            pitcherCount += 1;
            pitchersNeededInRoster--;
            totalWHIP += ((Pitcher)newPlayer).getWHIP();
            avgWHIP = calculateAvgWHIP();
            totalERA += ((Pitcher)newPlayer).getERA();
            avgERA = calculateAvgERA();
            totalW += ((Pitcher)newPlayer).getW();
            totalSV += ((Pitcher)newPlayer).getSV();
            totalK += ((Pitcher)newPlayer).getK(); 
        }
    }
    
    private void decreaseTeamStats(Player newPlayer) {
       playersNeededInRoster += 1; 
       pp = calculatePP();
        if (newPlayer instanceof Hitter) {
            hitterCount -= 1; 
            hittersNeededInRoster++; 
            totalR -= ((Hitter)newPlayer).getR();
            totalHR -= ((Hitter)newPlayer).getHR();
            totalRBI -= ((Hitter)newPlayer).getRBI();
            totalSB -= ((Hitter)newPlayer).getSB();
            totalBA -= ((Hitter)newPlayer).getBA();
            avgBA = calculateAvgBA();                
        } else if (newPlayer instanceof Pitcher) {
            pitcherCount -= 1;
            pitchersNeededInRoster++;
            totalWHIP -= ((Pitcher)newPlayer).getWHIP();
            avgWHIP = calculateAvgWHIP();
            totalERA -= ((Pitcher)newPlayer).getERA();
            avgERA = calculateAvgERA();
            totalW -= ((Pitcher)newPlayer).getW();
            totalSV -= ((Pitcher)newPlayer).getSV();
            totalK -= ((Pitcher)newPlayer).getK(); 
        }
    }
    
    public void addToRoster(Player newPlayer) {
        
        String position = newPlayer.getPosition();        
        unfilledPositions.remove(position);
        increaseTeamStats(newPlayer);
        
        switch (position) {
            case "P":
                addToFirstAvailablePosition(position, newPlayer);
                PCount++;
                break;
            case "OF":
                addToFirstAvailablePosition(position, newPlayer);
                OFCount++;
                break;
            case "C":
                addToFirstAvailablePosition(position, newPlayer);
                CCount++;
                break;
            default:
                roster.set(fixedPositions.indexOf(position), newPlayer);
                break;
        }        
    }
    
    public void addToRoster(Player newPlayer, String position) {       
        newPlayer.setPosition(position);  
        unfilledPositions.remove(position);
        increaseTeamStats(newPlayer);
        
        switch (position) {
            case "P":
                addToFirstAvailablePosition(position, newPlayer);
                PCount++;
                break;
            case "OF":
                addToFirstAvailablePosition(position, newPlayer);
                OFCount++;
                break;
            case "C":
                addToFirstAvailablePosition(position, newPlayer);
                CCount++;
                break;
            default:
                roster.set(fixedPositions.indexOf(position), newPlayer);
                break;
        }
    }
    
    public void removeFromRoster(Player playerToRemove) {
        
        String position = playerToRemove.getPosition(); 
        
        switch (position) {
            case "P":
                PCount--;
                break;
            case "OF":
                OFCount--;
                break;
            case "C":
                CCount--;
                break;
        }        
        
        addPlayerPlaceholder(position, roster.indexOf(playerToRemove));      
        unfilledPositions.add(position);        
        decreaseTeamStats(playerToRemove);
    }
    
    public void swapPlayerPosition(Player player, String oldPosition, String newPosition) {
        removeFromRoster(player);
        addToRoster(player, newPosition);
    }
    
    public void addPlayerPlaceholder(String position, int index) {
        roster.set(index, playerPlaceholders.get(fixedPositions.indexOf(position)));
    }
    
    public void rosterToString() {
        for (Player player : roster) {
            System.out.println(player);
        }
    }
    
    public void resetTotalPoints() {
        totalPoints = 0;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    public void incrementTotalPoints(int amount) {
        totalPoints += amount;
    }
    
    private void addToFirstAvailablePosition(String position, Player playerToAdd) {
        for (int i = fixedPositions.indexOf(position); i < sizeOfRoster; i++) {
            Player iteratedPlayer = roster.get(i);
            if (iteratedPlayer.isPlaceholder() && iteratedPlayer.getPosition().equals(position)) {
                roster.set(i, playerToAdd);
                return;
            }
        }
    }
    
    // Sort the Teams based on their total HR
    public static Comparator<Team> COMPARE_BY_TOTAL_HR = (Team t1, Team t2) -> 
            t2.getTotalHR() - t1.getTotalHR();
    
    // Sort the Teams based on their total HR
    public static Comparator<Team> COMPARE_BY_TOTAL_R = (Team t1, Team t2) -> 
            t2.getTotalR() - t1.getTotalR();
    
    // Sort the Teams based on their total HR
    public static Comparator<Team> COMPARE_BY_TOTAL_RBI = (Team t1, Team t2) -> 
            t2.getTotalRBI() - t1.getTotalRBI();
    
    // Sort the Teams based on their total HR
    public static Comparator<Team> COMPARE_BY_TOTAL_SB = (Team t1, Team t2) -> 
            t2.getTotalSB() - t1.getTotalSB();
    
    // Sort the Teams based on their total HR
    public static Comparator<Team> COMPARE_BY_AVG_BA = (Team t1, Team t2) -> 
            Double.compare(t2.getAvgBA(), t1.getAvgBA());
    
    // Less is better
    public static Comparator<Team> COMPARE_BY_TOTAL_W = (Team t1, Team t2) -> 
            t1.getTotalW() - t2.getTotalW();
    
    // Sort the Teams based on their total HR
    public static Comparator<Team> COMPARE_BY_TOTAL_SV = (Team t1, Team t2) -> 
            t2.getTotalSV() - t1.getTotalSV();
    
    // Sort the Teams based on their total HR
    public static Comparator<Team> COMPARE_BY_TOTAL_K = (Team t1, Team t2) -> 
            t2.getTotalK() - t1.getTotalK();
    
    // THE LOWER THE BETTER
    public static Comparator<Team> COMPARE_BY_AVG_ERA = (Team t1, Team t2) -> 
            Double.compare(t1.getAvgERA(), t2.getAvgERA());
    
    // THE LOWER THE BETTER
    public static Comparator<Team> COMPARE_BY_AVG_WHIP = (Team t1, Team t2) -> 
            Double.compare(t1.getAvgWHIP(), t2.getAvgWHIP());
    
    public static Comparator<Team> COMPARE_BY_TOTAL_POINTS = (Team t1, Team t2) ->
            t2.getTotalPoints() - t1.getTotalPoints();
     
}

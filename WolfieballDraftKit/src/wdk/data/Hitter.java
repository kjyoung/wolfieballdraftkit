package wdk.data;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * A Hitter is a Player with certain statistics which only a hitter has.
 * 
 * @author Kevin
 */
public class Hitter extends Player {
    
    SimpleIntegerProperty AB, // "At Bats", which is used to calculate Batting Average
            R, // "Runs", earned by reaching home plate
            HR, // "Home Runs", earned by hitting a fair ball over the outfield fence
            RBI, // "Runs Batted In", earned by hitting the ball and allowing another player to reach home plate
            SB; // "Stolen Bases", earned by advancing a base on the player's own without the benefit of a hit
    
    private SimpleDoubleProperty BA; // "Batting Average", calculated as Hits/At Bats
    
    public Hitter(String proTeam, String lastName, String firstName, int H, 
            String notes, int yearOfBirth, String nationOfBirth,
            int AB, int R, int HR, int RBI, int SB) {
        super(proTeam, lastName, firstName, H, notes, yearOfBirth, 
                nationOfBirth); 
        
        
        this.AB = new SimpleIntegerProperty(AB);
        this.R = new SimpleIntegerProperty(R);
        this.HR = new SimpleIntegerProperty(HR);
        this.RBI = new SimpleIntegerProperty(RBI);
        this.SB = new SimpleIntegerProperty(SB);
        this.BA = new SimpleDoubleProperty(0);        
    }
    
    public Hitter(String proTeam, String lastName, String firstName) {
        super(proTeam, lastName, firstName); 
        initDefaultHitter();
    }
    
    // Good for creating a hitter placeholder
    public Hitter(String position) {
        super("","","");
        this.position = new SimpleStringProperty(position);
        initDefaultHitter();
    }
    
    public Hitter(String proTeam , String lastName, String firstName, List<String> qualifiedPositionsList) {
        super(proTeam, lastName, firstName);
        this.qualifiedPositionsList = qualifiedPositionsList;
        initDefaultHitter();
    }
    
    private void initDefaultHitter() {
        this.qualifiedPositions = new SimpleStringProperty("");  
        this.AB = new SimpleIntegerProperty(0);
        this.R = new SimpleIntegerProperty(0);
        this.HR = new SimpleIntegerProperty(0);
        this.RBI = new SimpleIntegerProperty(0);
        this.SB = new SimpleIntegerProperty(0); 
        this.BA = new SimpleDoubleProperty(0);        
    }
    
    public void setAB(int AB) {
        this.AB.set(AB);
    }
    
    public void setR(int R) {
        this.R.set(R);
    }
    
    public void setHR(int HR) {
        this.HR.set(HR);
    }
    
    public void setRBI(int RBI) {
        this.RBI.set(RBI);
    }
    
    public void setSB(int SB) {
        this.SB.set(SB);
    }
    
    public void setBA(int H, double AB) {
         // Make sure we are not dividing by zero
        if (AB == 0) {
            this.BA .set(0);
        } else {            
            this.BA = new SimpleDoubleProperty((double)H / AB); 
        }
    }
    
    public int getAB() {
        return AB.get();
    }
    
    public int getR() {
        return R.get();
    }
    
    public int getHR() {
        return HR.get();
    }
    
    public int getRBI() {
        return RBI.get();
    }
    
    public int getSB() {
        return SB.get();
    }
    
    public double getBA() {
        return BA.get();
    }
    
    // Good
    public static Comparator<Hitter> COMPARE_BY_BA = (Hitter h1, Hitter h2) ->
            Double.compare(h2.getBA(), h1.getBA());
    
    // Good
    public static Comparator<Hitter> COMPARE_BY_R = (Hitter h1, Hitter h2) ->
            h2.getR() - h1.getR();
    
    // Good
    public static Comparator<Hitter> COMPARE_BY_HR = (Hitter h1, Hitter h2) ->
            h2.getHR() - h1.getHR();
    
    // Good
    public static Comparator<Hitter> COMPARE_BY_RBI = (Hitter h1, Hitter h2) ->
            h2.getRBI() - h1.getRBI();
    
    // Good
    public static Comparator<Hitter> COMPARE_BY_SB = (Hitter h1, Hitter h2) ->
            h2.getSB() - h1.getSB();
    
    
    @Override
    public String toString() {
        String hitterString = "HITTER\n";
        hitterString += super.toString();
        hitterString += "BA: " + BA
                + "\nR: " + R
                + "\nHR: " + HR
                + "\nRBI: " + RBI
                + "\nSB: " + SB;
        
       return hitterString;                       
    }
 }
    


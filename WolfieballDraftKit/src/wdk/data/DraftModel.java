package wdk.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import wdk.gui.FantasyStandingsScreen;

/**
 * This class holds the fantasy teams, as well as all the players in the drafting
 * pool. 
 * 
 * @author Kevin
 */
public class DraftModel {    
    
    // The name of this draft
    String draftName;    
    
    // All the professional teams that a player could play for
    ObservableList<ProfessionalTeam> listOfProTeams;
            
    // All the fantasy teams that a player can be drafted to
    ObservableList<Team> fantasyTeams;
    public int fantasyTeamsCount;
    
    List<Player> defaultPlayerPool;
    ObservableList<Player> observableDefaultPlayerPool;
    ObservableList<Player> playerPool;
    
    // Arraylist because we want to keep track of the order that players are
    // drafted
    ObservableList<Player> draftedPlayers;
    
    List<Player> playerPoolRanking;    
    
    static List<String> fixedFantasyTeamPositions;
    static List<Player> fantasyTeamPlaceholders;
    static List<String> fantasyTeamPositions;
    
    final String pos_C = "C";
    final String pos_1B = "1B";
    final String pos_CI = "CI";
    final String pos_3B = "3B";
    final String pos_2B = "2B";
    final String pos_MI = "MI";
    final String pos_SS = "SS";
    final String pos_U = "U";
    final String pos_OF = "OF";
    final String pos_P = "P";
    
    Hitter placeholder_C1, placeholder_C2, placeholder_1B, placeholder_CI,
            placeholder_3B, placeholder_2B, placeholder_MI, placeholder_SS,
            placeholder_OF1, placeholder_OF2, placeholder_OF3, placeholder_OF4
            ,placeholder_OF5, placeholder_U;
    
    Pitcher placeholder_P1, placeholder_P2, placeholder_P3, placeholder_P4,
            placeholder_P5, placeholder_P6, placeholder_P7, placeholder_P8
            ,placeholder_P9;
    
    ObservableList<DraftEvent> draftOrder;
    
    List<Hitter> topHitters;
    List<Pitcher> topPitchers;
    
    List<Hitter> hitterRanking;
    List<Pitcher> pitcherRanking;
    
    int totalMoney;
    int totalHittersNeeded;
    int totalPitchersNeeded;
    int hitterSize;
    int pitcherSize;
    int medianHitterSalary;
    int medianPitcherSalary;
    
    int pickCount = 1;
        
    public DraftModel() {
        
        draftOrder = FXCollections.observableArrayList();
        draftName = "WolfieballDraft";
        fantasyTeams = FXCollections.observableArrayList();
        
        // Used to calculate estimated value for the players
        hitterRanking = new ArrayList<Hitter>();
        pitcherRanking = new ArrayList<Pitcher>();    
        
        defaultPlayerPool = new ArrayList<Player>();
        playerPool = FXCollections.observableArrayList();
        draftedPlayers = FXCollections.observableArrayList();
        initListOfProTeams();
        
        fantasyTeamPositions = new ArrayList<String>(Arrays.asList(pos_C, pos_1B, pos_CI, pos_3B, pos_2B, pos_MI, 
                                         pos_SS, pos_OF, pos_U, pos_P));
        
        /* A baseball team needs two catchers, one 1B, CI, 3B, 2B, MI, SS, 
        five outfielders, and nine pitchers */
        fixedFantasyTeamPositions = new ArrayList<String>(Arrays.asList(pos_C, pos_C,
                                         pos_1B, pos_CI, pos_3B, pos_2B, pos_MI, 
                                         pos_SS, pos_OF, pos_OF, pos_OF, pos_OF, 
                                         pos_OF, pos_U, pos_P, pos_P, pos_P, pos_P, pos_P, 
                                         pos_P, pos_P, pos_P, pos_P));
        
        initFantasyTeamPlaceholders();        
    }
    
    public void setPickCount(int count) {
        pickCount = count;
    }
    
    public void clearAllValues() {  
        pickCount = 1;
        clearFantasyTeams();
        clearDraftOrder();
    }
    
    public ObservableList<Player> getObservableDefaultPlayerPool() {
        return observableDefaultPlayerPool;
    }
    
    public void addToDraftOrder(DraftEvent event) {
        draftOrder.add(event);
    }
    
    // Player contract changed from S2 to S1
    public synchronized void removeFromDraftOrder(Player player) {
        pickCount--;
        for (int i = 0; i < draftOrder.size(); i++) {
            DraftEvent event = draftOrder.get(i);
            if ((event.getFullName()).equals(player.getFullName())) {
                draftOrder.remove(event);
                for (int j = i; j < draftOrder.size(); j++) {
                    event = draftOrder.get(j);
                    int pickNumber = event.getPickNumber();
                    event.setPickNumber(pickNumber - 1);
                }
                return;
            }
        }        
    }
    
    public void clearDraftOrder() {
        draftOrder.clear();
    }
    
    public ObservableList<DraftEvent> getDraftOrder() {
        return draftOrder;
    }
    
    public boolean allPositionsFilled() {
        for (Team team : fantasyTeams) {
            if (!team.hasFilledRoster()) {
                return false;
            }
        }
        
        return true;
    }
    
    public void addPickToDraftOrder(Player player, Team team) {
        draftOrder.add(new DraftEvent(pickCount, player.getFirstName(), player.getLastName(), 
                    team.getName(), player.getContract(), player.getSalary()));
        pickCount++;
    }
    
    public void addTradeToDraftOrder(Player player, Team team) {
        for (DraftEvent event: draftOrder) {
            if (event.getFullName().equals(player.getFullName())) {
                event.setTeamName(team.getName());
                return;
            }
        }
    }
    
    public void sortDefaultPlayerPool() {        
        Collections.sort(defaultPlayerPool, Player.COMPARE_BY_LAST_NAME);  
    }
    
    public void sortPlayerPool() {        
        Collections.sort(playerPool, Player.COMPARE_BY_LAST_NAME);  
    }
    
    public void resetPlayerPool() {
        playerPool.setAll(defaultPlayerPool);
    }
    
    public void clearFantasyTeams() {
        fantasyTeams.clear();
    }
    
    public void clearPlayerPool() {
        playerPool.clear();
    }
    
    public static List<String> getFantasyTeamPositions() {
        return fantasyTeamPositions;
    }
    
    public void addFantasyTeam(Team teamToAdd) {
        calculateTotalPoints();
        calculateEstimatedValues();
        fantasyTeamsCount++;
        fantasyTeams.add(teamToAdd);
    }
    
    public void setDraftName(String draftName) {
        this.draftName = draftName;
    }
    
    public String getDraftName() {
        return draftName;
    }
    
    private void initFantasyTeamPlaceholders() {
        placeholder_C1 = new Hitter(pos_C); 
        placeholder_C2 = new Hitter(pos_C);
        placeholder_1B = new Hitter(pos_1B);
        placeholder_CI = new Hitter(pos_CI);
        placeholder_3B = new Hitter(pos_3B);
        placeholder_2B = new Hitter(pos_2B);
        placeholder_MI = new Hitter(pos_MI);
        placeholder_SS = new Hitter(pos_SS);
        placeholder_OF1 = new Hitter(pos_OF);
        placeholder_OF2 = new Hitter(pos_OF);
        placeholder_OF3 = new Hitter(pos_OF);
        placeholder_OF4 = new Hitter(pos_OF);
        placeholder_OF5 = new Hitter(pos_OF);
        placeholder_U = new Hitter(pos_U);
        placeholder_P1 = new Pitcher(pos_P);
        placeholder_P2 = new Pitcher(pos_P);
        placeholder_P3 = new Pitcher(pos_P); 
        placeholder_P4 = new Pitcher(pos_P); 
        placeholder_P5 = new Pitcher(pos_P); 
        placeholder_P6 = new Pitcher(pos_P); 
        placeholder_P7 = new Pitcher(pos_P); 
        placeholder_P8 = new Pitcher(pos_P); 
        placeholder_P9 = new Pitcher(pos_P); 
        
        fantasyTeamPlaceholders = new ArrayList<Player>(Arrays.asList(placeholder_C1,
                placeholder_C2, placeholder_1B, placeholder_CI, placeholder_3B,
                placeholder_2B, placeholder_MI, placeholder_SS, placeholder_OF1,
                placeholder_OF2, placeholder_OF3, placeholder_OF4, placeholder_OF5, 
                placeholder_U, placeholder_P1, placeholder_P2, placeholder_P3, placeholder_P4,
                placeholder_P5, placeholder_P6, placeholder_P7, placeholder_P8,
                placeholder_P9));
        
        for (Player placeholder : fantasyTeamPlaceholders) {
            placeholder.setIsPlaceholder(true);
        }
    }
    
    public ObservableList<Team> getFantasyTeams() {
        return fantasyTeams;
    }
    
    static public List<Player> getFantasyTeamPlaceholders() {
        return fantasyTeamPlaceholders;
    }
    
    static public List<String> getFixedFantasyTeamPositions() {
        return fixedFantasyTeamPositions;    
    }
    
    /**
     * Add all of the professional teams that a player can be drafted to
     */
    private void initListOfProTeams() { 
        listOfProTeams = FXCollections.observableArrayList();
        listOfProTeams.addAll(ProfessionalTeam.values());
    }
    
    public ObservableList<ProfessionalTeam> getProTeams() {
        return listOfProTeams;
    }    
    
    public ObservableList<Player> getPlayerPool() {
        return playerPool;
    }
    
    public Team getFantasyTeamByName(String teamName) {
        for (Team team : fantasyTeams) {
            if(team.getName().equals(teamName)) {
                return team;
            }
        }
        
        return null;
    }
    
    public boolean fantasyTeamExists(String teamName) {
        
        if (fantasyTeams.isEmpty()) {
            return false;
        }
        
        return getFantasyTeamByName(teamName) != null;
    }
    
    public void removeTeam(Team teamToRemove) {
        fantasyTeamsCount--;
        fantasyTeams.remove(teamToRemove);
        calculateTotalPoints();
        calculateEstimatedValues();
    }
    
    public void removeTeamAndReturnToPool(Team teamToRemove) {        
                
        // Return all non-placeholder players to the player pool
        for (Player player : teamToRemove.getRoster()) {
           if (!player.isPlaceholder()) {
             addPlayerToPool(player);
           }
        }
        
        for (Player player : teamToRemove.getTaxiSquad()) {
            addPlayerToPool(player);
        }
        
        // And now remove the team from the draft
        removeTeam(teamToRemove);
    }
    
    public void removeFromPlayerPool(Player player) {
        playerPool.remove(player);
    }  
    
    public void addPlayerToDefaultPool(Player player) {
        defaultPlayerPool.add(player);
    }
    
    public void addPlayerToPool(Player player) {
        playerPool.add(player);
    }
    
    public String fantasyTeamsToString() {
        return fantasyTeams.toString();
    }
    
    public String playerPoolToString() {
        
        String playersString = "";
        for (Player player : playerPool) {
            playersString += player.toString();
        }
        return playersString;
    }    
    
    private void resetTeamTotalPoints() {
        for (Team team : fantasyTeams) {
            team.resetTotalPoints();
        }
    }
    
    public synchronized void calculateEstimatedValues() {
        
        for (Player player : draftedPlayers) {
            player.resetRank();
        }
        
        totalMoney = 0;
        totalHittersNeeded = 0;
        totalPitchersNeeded = 0;
        for (Team team : fantasyTeams) {
            if (!team.getUnfilledPositions().isEmpty() || team.getTaxiSquad().size() != 9) {
                totalMoney += team.getPurse();
            }
            for (String position : team.getUnfilledPositions()) {
                if (position.equals("P")) {
                    totalPitchersNeeded++;
                } else {
                    totalHittersNeeded++;
                }
            }
        }
        
        hitterRanking.clear();
        pitcherRanking.clear();
        
        for (Player player : playerPool) {
            if (player instanceof Hitter) {
                hitterRanking.add((Hitter)player);
            } else if (player instanceof Pitcher) {
                pitcherRanking.add((Pitcher)player);
            }
        }        
        
        hitterSize = hitterRanking.size();
        pitcherSize = pitcherRanking.size();
        
        if (totalHittersNeeded != 0) {
             // Rank the hitters        
            rankHitters();
            medianHitterSalary = totalMoney / (2 * totalHittersNeeded);
            
            for (Hitter hitter : hitterRanking) {
                hitter.setRank(hitter.getRank() / 5);
            }           
            
            Collections.sort(hitterRanking, Player.COMPARE_BY_RANK);
            
            for (Hitter hitter : hitterRanking) {
                //System.out.println("Hitter rank = " + hitter.getRank());
                hitter.setEstimatedValue((int)(Math.round(medianHitterSalary * (totalHittersNeeded * (2.0 / hitter.getRank())))));
                //System.out.println("Hitter estvalue = " + hitter.getEstimatedValue());
            }
        } else {
            // Do nothing
        }
        
        if (totalPitchersNeeded != 0) {
           // Now rank the pitchers
            rankPitchers();
            medianPitcherSalary = totalMoney / (2 * totalPitchersNeeded);
            
            for (Pitcher pitcher : pitcherRanking) {
                pitcher.setRank(pitcher.getRank() / 5);
            } 
            
            Collections.sort(pitcherRanking, Player.COMPARE_BY_RANK);
            
            for (Pitcher pitcher : pitcherRanking) {     
                //System.out.println("Pitcher rank = " + pitcher.getRank());
                pitcher.setEstimatedValue((int)(Math.round(medianPitcherSalary * (totalPitchersNeeded * (2.0 / pitcher.getRank())))));
                //System.out.println("Pitcher estvalue = " + pitcher.getEstimatedValue());
            }
        } else {
            // Do nothing
        }        
    }
    
    public void rankHitters() {
        Collections.sort(hitterRanking, Hitter.COMPARE_BY_BA);
        for (Hitter hitter : hitterRanking) {
            System.out.println("Hitter BA = " + hitter.getBA());
        }
        
        updateHitterRanking(hitterSize, hitterRanking);
        
        
        Collections.sort(hitterRanking, Hitter.COMPARE_BY_R);
        System.out.println("-----------------------------");
        for (Hitter hitter : hitterRanking) {
            System.out.println("Hitter R = " + hitter.getR());
        }
        updateHitterRanking(hitterSize, hitterRanking);
        
        Collections.sort(hitterRanking, Hitter.COMPARE_BY_HR);
        System.out.println("-----------------------------");
        for (Hitter hitter : hitterRanking) {
            System.out.println("Hitter HR = " + hitter.getHR());
        }
        updateHitterRanking(hitterSize, hitterRanking);
        
        Collections.sort(hitterRanking, Hitter.COMPARE_BY_RBI);
        System.out.println("-----------------------------");
        for (Hitter hitter : hitterRanking) {
            System.out.println("Hitter RBI = " + hitter.getRBI());
        }
        updateHitterRanking(hitterSize, hitterRanking);
        
        Collections.sort(hitterRanking, Hitter.COMPARE_BY_SB);
        System.out.println("-----------------------------");
        for (Hitter hitter : hitterRanking) {
            System.out.println("Hitter SB = " + hitter.getSB());
        }
        updateHitterRanking(hitterSize, hitterRanking);  
    }
    
    public void rankPitchers() {
        Collections.sort(pitcherRanking, Pitcher.COMPARE_BY_ERA);
        System.out.println("-----------------------------");
        for (Pitcher pitcher : pitcherRanking) {
            System.out.println("Hitter ERA = " + pitcher.getERA());
        }
        updatePitcherRanking(pitcherSize, pitcherRanking);  
        
        Collections.sort(pitcherRanking, Pitcher.COMPARE_BY_K);
        System.out.println("-----------------------------");
        for (Pitcher pitcher : pitcherRanking) {
            System.out.println("Hitter K = " + pitcher.getK());
        }
        updatePitcherRanking(pitcherSize, pitcherRanking);     
        
        Collections.sort(pitcherRanking, Pitcher.COMPARE_BY_SV);
        System.out.println("-----------------------------");
        for (Pitcher pitcher : pitcherRanking) {
            System.out.println("Hitter SV = " + pitcher.getSV());
        }
        updatePitcherRanking(pitcherSize, pitcherRanking);    
        
        Collections.sort(pitcherRanking, Pitcher.COMPARE_BY_W);
        System.out.println("-----------------------------");
        for (Pitcher pitcher : pitcherRanking) {
            System.out.println("Hitter W = " + pitcher.getW());
        }
        updatePitcherRanking(pitcherSize, pitcherRanking);    
        
        Collections.sort(pitcherRanking, Pitcher.COMPARE_BY_WHIP);
        System.out.println("-----------------------------");
        for (Pitcher pitcher : pitcherRanking) {
            System.out.println("Hitter WHIP = " + pitcher.getWHIP());
        }
        updatePitcherRanking(pitcherSize, pitcherRanking); 
    }
    
    public void updateHitterRanking(int hitterSize, List<Hitter> hitterRanking) {
        for (int i = 0; i < hitterSize; i++) {
            Hitter hitter = hitterRanking.get(i);
            hitter.incrementRank(i+1);
        }
    }
    
    public void updatePitcherRanking(int pitcherSize, List<Pitcher> pitcherRanking) {
        for (int i = 0; i < pitcherSize; i++) {
            Pitcher pitcher = pitcherRanking.get(i);
            pitcher.incrementRank(i+1);
        }
    }
    
    public synchronized void calculateTotalPoints() {
        
        resetTeamTotalPoints();        
        int lastPoint;
        
        // Get the ranking for the R category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_TOTAL_R); 
        //Collections.reverse(fantasyTeams);
        
        for (Team team : fantasyTeams) {
            System.out.println("R = " + team.getTotalR());
        }
        
        fantasyTeamsCount = fantasyTeams.size();
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                int totalR = team.getTotalR();
                lastPoint = getTotalRanking(team, totalR, lastPoint, fantasyTeams.get(i-1).getTotalR(), i);
            }            
        }  
        
        // Get the ranking for the HR category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_TOTAL_HR); 
        //Collections.reverse(fantasyTeams);
        
        for (Team team : fantasyTeams) {
            System.out.println("HR = " + team.getTotalHR());
        }
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                int totalHR = team.getTotalHR();
                lastPoint = getTotalRanking(team, totalHR, lastPoint, fantasyTeams.get(i-1).getTotalHR(), i);
            }            
        }  
        
        // Get the ranking for the HR category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_TOTAL_RBI);
        //Collections.reverse(fantasyTeams);
        System.out.println("------------------------------------");
        for (Team team : fantasyTeams) {
            System.out.println("HR = " + team.getTotalRBI());
        }
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                int totalRBI = team.getTotalRBI();
                lastPoint = getTotalRanking(team, totalRBI, lastPoint, fantasyTeams.get(i-1).getTotalRBI(), i);
            }            
        }  
        
        // Get the ranking for the HR category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_TOTAL_SB); 
        //Collections.reverse(fantasyTeams);
        System.out.println("------------------------------------");
        for (Team team : fantasyTeams) {
            System.out.println("SB = " + team.getTotalSB());
        }
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                int totalSB = team.getTotalSB();
                lastPoint = getTotalRanking(team, totalSB, lastPoint, fantasyTeams.get(i-1).getTotalSB(), i);
            }            
        }  
        
        // Get the ranking for the HR category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_AVG_BA); 
        //Collections.reverse(fantasyTeams);
        
        System.out.println("------------------------------------");
        for (Team team : fantasyTeams) {
            System.out.println("BA = " + team.getAvgBA());
        }
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                double avgBA = team.getAvgBA();
                lastPoint = getAvgRanking(team, avgBA, lastPoint, fantasyTeams.get(i-1).getAvgBA(), i);
            }            
        }  
        
        // Get the ranking for the HR category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_TOTAL_W); 
        //Collections.reverse(fantasyTeams);
        
        System.out.println("------------------------------------");
        for (Team team : fantasyTeams) {
            System.out.println("W = " + team.getTotalW());
        }
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                int totalW = team.getTotalSV();
                lastPoint = getTotalRanking(team, totalW, lastPoint, fantasyTeams.get(i-1).getTotalW(), i);
            }            
        }   
        
        // Get the ranking for the HR category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_TOTAL_SV); 
        //Collections.reverse(fantasyTeams);
        
        System.out.println("------------------------------------");
        for (Team team : fantasyTeams) {
            System.out.println("SV = " + team.getTotalSV());
        }
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                int totalSV = team.getTotalSV();
                lastPoint = getTotalRanking(team, totalSV, lastPoint, fantasyTeams.get(i-1).getTotalSV(), i);
            }            
        }   
        
        // Get the ranking for the HR category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_TOTAL_K); 
        //Collections.reverse(fantasyTeams);
        
        System.out.println("------------------------------------");
        for (Team team : fantasyTeams) {
            System.out.println("K = " + team.getTotalK());
        }
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                int totalK = team.getTotalK();
                lastPoint = getTotalRanking(team, totalK, lastPoint, fantasyTeams.get(i-1).getTotalK(), i);
            }            
        }   
        
        // Get the ranking for the HR category
        Collections.sort(fantasyTeams, Team.COMPARE_BY_AVG_ERA); 
        //Collections.reverse(fantasyTeams);
        
        System.out.println("------------------------------------");
        for (Team team : fantasyTeams) {
            System.out.println("ERA = " + team.getAvgERA());
        }
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints(fantasyTeamsCount);
            } else {                
                double avgERA = team.getAvgERA();
                lastPoint = getAvgRanking(team, avgERA, lastPoint, fantasyTeams.get(i-1).getAvgERA(), i);
            }            
        }                    
        
        Collections.sort(fantasyTeams, Team.COMPARE_BY_AVG_WHIP); 
        //Collections.reverse(fantasyTeams);
        
        System.out.println("------------------------------------");
        for (Team team : fantasyTeams) {
            System.out.println("WHIP = " + team.getAvgWHIP());
        }
        System.out.println("====================================");
        
        lastPoint = fantasyTeamsCount;
        for (int i = 0; i < fantasyTeamsCount; i++) {              
            Team team = fantasyTeams.get(i);            
            if (i == 0) {
                team.incrementTotalPoints((fantasyTeamsCount));
            } else {                
                double avgWHIP = team.getAvgWHIP();
                lastPoint = getAvgRanking(team, avgWHIP, lastPoint, fantasyTeams.get(i-1).getAvgWHIP(), i);
            }            
        }  
        
        TableColumn playerColumn = (TableColumn) FantasyStandingsScreen.teamsTable.getColumns().get(0);
        playerColumn.setVisible(false);
        playerColumn.setVisible(true);
        
        Collections.sort(fantasyTeams, Team.COMPARE_BY_TOTAL_POINTS);
    }
    
    private int getAvgRanking(Team team, double avg, int lastPoint, double avgToCompare, int index) {
        
        if (index == fantasyTeamsCount - 1) {
            if (avg == avgToCompare) {
                team.incrementTotalPoints(lastPoint);             
            } else {               
                team.incrementTotalPoints(1);
                lastPoint = fantasyTeamsCount - index;
            }       
            return lastPoint;
        }
        if (index > 0) {
            if (avg == avgToCompare) {
                team.incrementTotalPoints(lastPoint);             
            } else {               
                team.incrementTotalPoints(fantasyTeamsCount - index);
                lastPoint = fantasyTeamsCount - index;
            }       
        } else {
            team.incrementTotalPoints(fantasyTeamsCount - index);
        }
        
        return lastPoint;        
        
    }
    
    // CHANGE WHATS IN PLATFORM.RUNLATER FOR THREADING
    
    private int getTotalRanking(Team team, int total, int lastPoint, int totalToCompare, int index) {
        if (index == fantasyTeamsCount - 1) {
            if (total == totalToCompare) {
                team.incrementTotalPoints(lastPoint);             
            } else {               
                team.incrementTotalPoints(1);
                lastPoint = fantasyTeamsCount - index;
            }       
            return lastPoint;
        }
        if (index > 0) {
            if (total == totalToCompare) {
                team.incrementTotalPoints(lastPoint);             
            } else {               
                team.incrementTotalPoints(fantasyTeamsCount - index);
                lastPoint = fantasyTeamsCount - index;
            }       
        } else {
            team.incrementTotalPoints(fantasyTeamsCount - index);
        }
        
        return lastPoint; 
    }

    
    
}

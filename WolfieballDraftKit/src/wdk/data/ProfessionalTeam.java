/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

/**
 *
 * @author Kevin
 */
public enum ProfessionalTeam {
    ATL, 
    AZ, 
    CHC, 
    CIN, 
    COL,
    LAD, 
    MIA, 
    MIL, 
    NYM, 
    PHI, 
    PIT, 
    SD,
    SF,
    STL,
    TOR,
    WSH
}

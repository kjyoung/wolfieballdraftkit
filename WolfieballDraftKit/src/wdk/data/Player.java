package wdk.data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * A Player is a baseball player that will be added to the player pool,
 * and the fantasy teams. Every Player has a name, positions they are qualified
 * for, and other attributes. However, particular statistics such as RBI
 * are not shared by all baseball players, so those type of statistics are
 * not held in this class.
 * 
 * @author Kevin
 */
public class Player {
    
    SimpleStringProperty proTeam, lastName, firstName, notes, nationOfBirth, 
                         qualifiedPositions, contract;
    
    SimpleStringProperty position; // The position of the player on a team
    
    SimpleIntegerProperty H; // Hits, a hitter and pitcher statistic
    SimpleIntegerProperty salary; // How much this player is getting paid
    SimpleIntegerProperty estimatedValue; // The estimated value of this player
    SimpleIntegerProperty yearOfBirth;
    
    List<String> qualifiedPositionsList;
    
    int rank = 0;
    
    boolean isPlaceholder = false;
    
    public void incrementRank(int amount) {
        rank += amount;
    }
    
    public void resetRank() {
        rank = 0;
    }
    
    public int getRank() {
        return rank;
    }
    
    public static Comparator<Player> COMPARE_BY_RANK = (Player p1, Player p2) ->
            p1.getRank() - p2.getRank();
    
    public void setRank(int rank) {
        this.rank = rank;
    }
    
    public Player(String proTeam, String lastName, String firstName,
                  int H, String notes, int yearOfBirth, String nationOfBirth) {
        this.proTeam = new SimpleStringProperty(proTeam);
        this.lastName =  new SimpleStringProperty(lastName);
        this.firstName =  new SimpleStringProperty(firstName);
        this.H =  new SimpleIntegerProperty(H);
        this.notes =  new SimpleStringProperty(notes);
        this.yearOfBirth =  new SimpleIntegerProperty(yearOfBirth);
        this.nationOfBirth =  new SimpleStringProperty(nationOfBirth); 
        this.qualifiedPositions = new SimpleStringProperty("");
        this.qualifiedPositionsList = new ArrayList<String>();
        
        // These values aren't created until a player is drafted, but initialize
        // them now as to prevent future null exceptions
        this.salary = new SimpleIntegerProperty(0);
        this.contract = new SimpleStringProperty("");
        this.position = new SimpleStringProperty("");
        this.estimatedValue = new SimpleIntegerProperty(0);
    }
    
    public Player(String proTeam, String lastName, String firstName) {
        this.lastName =  new SimpleStringProperty(lastName);
        this.firstName =  new SimpleStringProperty(firstName);
        this.proTeam = new SimpleStringProperty(proTeam);
        
        this.H =  new SimpleIntegerProperty(0);
        this.notes =  new SimpleStringProperty("");
        this.yearOfBirth =  new SimpleIntegerProperty(0);
        this.nationOfBirth =  new SimpleStringProperty(""); 
        this.qualifiedPositions = new SimpleStringProperty("");
        this.qualifiedPositionsList = new ArrayList<String>();
        
        // These values aren't created until a player is drafted, but initialize
        // them now as to prevent future null exceptions
        this.salary = new SimpleIntegerProperty(0);
        this.contract = new SimpleStringProperty("");
        this.position = new SimpleStringProperty(""); 
        this.estimatedValue = new SimpleIntegerProperty(0);
    }
    
    public String getProfileImageKey() {
        return lastName.get() + firstName.get();
    }   
    
    public String getFlagImageKey() {
        return nationOfBirth.get();
    }    
    
    public String getQualifiedPositions() {
        return qualifiedPositions.get();
    }
    
    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }
    
    public void setEstimatedValue(int estimatedValue) {
        this.estimatedValue.set(estimatedValue);
    }
    
    public int getEstimatedValue() {
        return estimatedValue.get();
    }
    
    public void setQualifiedPositionsList(List<String> qualifiedPositionsList) {
        this.qualifiedPositionsList = qualifiedPositionsList;
    }
    
    /**
     * Searches the list of qualified positions and return true if this Player
     * is qualified for the given position.
     * 
     * @param position The position to search for a match
     * @return true is this Player is qualified for position
     */
    public boolean searchForPosition(String position) {
        for (String pos: qualifiedPositionsList) {
            if (position.equals(pos)) {
                return true;
            }
        }
        
        return false;
    }
    
    public List<String> getQualifiedPositionsList() {
        return qualifiedPositionsList;
    }

    public void setQualifiedPositions(String qualifiedPositions) {
        this.qualifiedPositions.set(qualifiedPositions);
    }
    
    public String getFirstName() {
        return firstName.get();
    }
    
    public String getLastName() {
        return lastName.get();
    }

    public String getProTeam() {
        return proTeam.get();
    }

    public void setProTeam(String proTeam) {
        this.proTeam.set(proTeam);
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getNotes() {
        return notes.get();
    }

    public void setNotes(String notes) {
        this.notes.set(notes);
    }

    public String getNationOfBirth() {
        return nationOfBirth.get();
    }

    public void setNationOfBirth(String nationOfBirth) {
        this.nationOfBirth.set(nationOfBirth);
    }

    public int getH() {
        return H.get();
    }

    public void setH(int H) {
        this.H.set(H);
    }

    public int getYearOfBirth() {
        return yearOfBirth.get();
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth.set(yearOfBirth);
    }
    
    public int getSalary() {
        return salary.get();
    }
    
    public void setSalary(int salary) {
        this.salary.set(salary);
    }
    
    public String getContract() {
        return this.contract.get();
    }
    
    public void setContract(String contract) {
        this.contract.set(contract);
    }
    
    public String getPosition() {
        return this.position.get();
    }
    
    public void setPosition(String position) {
        this.position.set(position);
    }
    
    /** A placeholder only has a particular position, and all other fields are
    * default. We use placeholders to clearly show a user what positions are 
    * missing on their fantasy team. So in the table of a fantasy team's roster,
    * this placeholder will appear to have no values besides a position,
    * indicating this position has not been filled yet.
    * 
    * @return True if this player is a placeholder
    */ 
    public boolean isPlaceholder() {
        return isPlaceholder;
    }
    
    public void setIsPlaceholder(boolean isPlaceholder) {
        this.isPlaceholder = isPlaceholder;
    }
    
    @Override 
    public String toString() {
        
        String playerString = "";
        playerString += "Professional Team: " + proTeam 
                + "\nLast name: " + lastName 
                + "\nFirst name: " + firstName
                + "\nYear of Birth: " + yearOfBirth
                + "\nNation of Birth: " + nationOfBirth
                + "\nQualified Positions: " + qualifiedPositionsList 
                + "\nNotes: " + notes
                + "\nH: " + H + "\n";
        return playerString;
    }
    
    // SORT THE ASSIGNMENTS BASED ON DATE
    public static Comparator<Player> COMPARE_BY_LAST_NAME = (Player p1, Player p2) -> 
            p1.getLastName().compareToIgnoreCase(p2.getLastName());
}

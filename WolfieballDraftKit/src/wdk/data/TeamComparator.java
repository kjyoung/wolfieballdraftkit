/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.Comparator;

/**
 *
 * @author Kevin
 */
public class TeamComparator implements Comparator<Team> {

    @Override
    public int compare(Team o1, Team o2) {
        if (o1 == null && o2 == null) {
            return 0;
        }
        if (o1 == null) {
            return -1;
        }
        if (o2 == null) {
            return 1;
        }
        
        String name1 = o1.getName();
        String name2 = o2.getName();
        if (name1.equals("")) {
            return -1;
        }
        if (name2.equals("")) {
            return 1;
        }
        
        return name1.compareTo(name2);
    }
    
}

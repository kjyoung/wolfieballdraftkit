package wdk.controller;

import java.io.File;
import java.io.IOException;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import static wdk.WDK_PropertyType.DRAFT_SAVED_MESSAGE;
import static wdk.WDK_PropertyType.NEW_DRAFT_CREATED_MESSAGE;
import static wdk.WDK_PropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static wdk.WDK_StartupConstants.PATH_DRAFTS;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.error.ErrorHandler;
import wdk.file.DraftFileManager;
import wdk.gui.MessageDialog;
import wdk.gui.WDK_GUI;
import wdk.gui.YesNoCancelDialog;

/**
 * This controller class provides responses to interactions with the buttons in
 * the file toolbar.
 *
 * @author Kevin Young
 */
public class FileController {
    
    // We'll use this to ask yes/no/cancel questions
    YesNoCancelDialog yesNoCancelDialog;
    
    boolean saved;
    
    // This will provide feedback to the user after
    // work by this class has completed
    MessageDialog messageDialog;
    
    // We'll use this to get our verification feedback
    PropertiesManager properties;
    
    // This will provide feedback to the user when something goes wrong
    ErrorHandler errorHandler;
    
    DraftFileManager draftIO;
    
    public FileController(YesNoCancelDialog initYesNoCancelDialog,
                          MessageDialog initMessageDialog,
                          DraftFileManager initDraftIO) {
        
        saved = true;
        
        this.draftIO = initDraftIO;
        errorHandler = ErrorHandler.getErrorHandler();
        yesNoCancelDialog = initYesNoCancelDialog;
        messageDialog = initMessageDialog;        
        properties = PropertiesManager.getPropertiesManager();
    }
    
    /**
     * This method starts the process of editing a new Draft. If a draft is
     * already being edited, it will prompt the user to save it first.
     * 
     * @param gui The user interface editing the Draft.
     */
    public void handleNewDraftRequest(WDK_GUI gui) {        
        
        try {
            // We may have to save current work
            boolean continueToMakeNew = true;
            if (!saved) {
                // The user can opt out here with a cancel
                continueToMakeNew = promptToSave(gui);
            }

            // If the user really wants to make a new Draft
            if (continueToMakeNew) {
                // Reset the data, which should trigger a reset of the ui
                DraftModelDataManager dataManager = gui.getDataManager();
                dataManager.reset();
                saved = false;

                // Refresh the gui, which will enable and disable
                // the appropriate controls
                gui.updateToolbarControls(saved);
                
                // Tell the user the Draft has been created
                messageDialog.show(properties.getProperty(NEW_DRAFT_CREATED_MESSAGE), false); 
            }
        } catch (Exception ioe) {
            System.out.println(ioe.getMessage());
            errorHandler.handleNewDraftError();
        }
    }
    
    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     * 
     * @param gui
     */
    public void handleExitRequest(WDK_GUI gui) {
        try {
            // We may have to save current work
            boolean continueToExit = true;
            if (!saved) {
                // The user can opt out here
                continueToExit = promptToSave(gui);
            }

            // If the user really wants to exit the app
            if (continueToExit) {
                // Exit the application
                System.exit(0);
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
            errorHandler.handleExitError();
        }
    }
    
    public void handleSaveDraftRequest(WDK_GUI gui, DraftModel draftToSave) {
        try {
            // Save it to a file
            draftIO.saveDraft(draftToSave);

            // Mark it as saved
            saved = true;

            // Tell the user the file has been saved
            messageDialog.show(draftToSave.getDraftName() + " saved", false);

            // And refresh the gui, which will enable and disable
            // the appropriate controls
            gui.updateToolbarControls(saved);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            errorHandler.handleSaveDraftError();
        }
    }
    
    /**
     * This method lets the user open a Draft saved to a file. It will also
     * make sure data for the current Draft is not lost.
     * 
     * @param gui The user interface editing the Draft.
     */
    public void handleLoadDraftRequest(WDK_GUI gui) {
        try {
            // We may have to save current work
            boolean continueToOpen = true;
            if (!saved) {
                // The user can opt out here with a cancel
                continueToOpen = promptToSave(gui);
            }

            // If the user really wants to open a Draft
            if (continueToOpen) {
                // Go ahead and proceed loading a Draft
                promptToOpen(gui);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            errorHandler.handleLoadDraftError();
        }
    }
    
    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen(WDK_GUI gui) {
        // And now ask the user for the draft to open
        FileChooser draftFileChooser = new FileChooser();
        draftFileChooser.setInitialDirectory(new File(PATH_DRAFTS));
        File selectedFile = draftFileChooser.showOpenDialog(gui.getWindow());

        // Only open a new file if the user says ok
        if (selectedFile != null) {
            try {
                DraftModel draftToLoad = gui.getDataManager().getDraftModel();
                draftIO.loadDraft(draftToLoad, selectedFile.getAbsolutePath());
                draftToLoad.calculateEstimatedValues();
                draftToLoad.calculateTotalPoints();
                gui.reloadDraftModel(draftToLoad);
                saved = true;
                gui.updateToolbarControls(saved);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                eH.handleLoadDraftError();
            }
        }
    }
    
    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * Draft, or opening another Draft. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is returned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave(WDK_GUI gui) throws IOException {
        // Prompt the user to save unsaved work
        yesNoCancelDialog.show(properties.getProperty(SAVE_UNSAVED_WORK_MESSAGE));
        
        // And now get the user's selection
        String selection = yesNoCancelDialog.getSelection();

        // If the user said yes, then save before moving on
        switch (selection) {
        // If the user said cancel, then we'll tell whoever
            case YesNoCancelDialog.YES:
                // Save the draft
                DraftModelDataManager dataManager = gui.getDataManager();
                draftIO.saveDraft(dataManager.getDraftModel());
                saved = true;
                break;
            case YesNoCancelDialog.CANCEL:
                return false;
        }

        // If the user said no, we just go on without saving
        // but for both yes and no we do whatever the user
        // had in mind in the first place
        return true;
    }
    
    /**
     * This method marks the appropriate variable such that we know
     * that the current Draft has been edited since it's been saved.
     * The UI is then updated to reflect this.
     * 
     * @param gui The user interface editing the Draft.
     */
    public void markFileAsDirty(WDK_GUI gui) {
        // The Draft object is now dirty 
        saved = false;
        
        // Let the UI know this
        gui.updateToolbarControls(saved);
    }
    
    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the draft is changed in some way.
     */
    public void markFileIsDirty() {
        saved = false;
    }
}

package wdk.controller;

import javafx.scene.control.TableColumn;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.error.ErrorHandler;
import wdk.gui.PlayersScreen;
import wdk.gui.WDK_GUI;

/**
 * This controller class handles the responses to all draft
 * editing input, including verification of data and binding of
 * entered data to the DraftModel object.
 * 
 * @author Kevin
 */
public class DraftEditController {
    // We use this to make sure our programmed updates of ui
    // values don't themselves trigger events
    private boolean enabled;
    
    DraftModelDataManager dataManager;
    DraftModel draft;
    
    TableColumn playerColumn;

    /**
     * Constructor that gets this controller ready, not much to
     * initialize as the methods for this function are sent all
     * the objects they need as arguments.
     */
    public DraftEditController(DraftModelDataManager dataManager) {
        this.dataManager = dataManager;
        this.draft = dataManager.getDraftModel();
        enabled = true;
    }

    /**
     * This mutator method lets us enable or disable this controller.
     * 
     * @param enableSetting If false, this controller will not respond to
     * Course editing. If true, it will.
     */
    public void enable(boolean enableSetting) {
        enabled = enableSetting;
    }

    /**
     * This controller function is called in response to the user changing
     * course details in the UI. It responds by updating the bound Course
     * object using all the UI values, including the verification of that
     * data.
     * 
     * @param gui The user interface that requested the change.
     */
    public void handleDraftChangeRequest(WDK_GUI gui) {
        if (enabled) {
            try {
                // The draft is now dirty, meaning it's been 
                // changed since it was last saved, so make sure
                // the save button is enabled
                gui.getFileController().markFileAsDirty(gui);
                playerColumn = (TableColumn) PlayersScreen.playerPoolTable.getColumns().get(0);
                playerColumn.setVisible(false);
                playerColumn.setVisible(true);
                draft.calculateEstimatedValues();
                draft.calculateTotalPoints();
                dataManager.refreshDraft();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                ErrorHandler eH = ErrorHandler.getErrorHandler();
                eH.handleUpdateDraftError();
            }
        }
    }

}

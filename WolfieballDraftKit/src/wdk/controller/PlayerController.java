package wdk.controller;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.event.EventType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import wdk.gui.MessageDialog;
import wdk.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.data.Player;
import wdk.data.Team;
import wdk.file.AssetManager;
import wdk.gui.AddPlayerDialog;
import wdk.gui.DraftScreen;
import wdk.gui.EditPlayerDialog;
import wdk.gui.FantasyTeamsScreen;
import wdk.gui.PlayersScreen;

/**
 *
 * @author Kevin
 */
public class PlayerController {
    AddPlayerDialog apd;
    EditPlayerDialog epd;
    YesNoCancelDialog yesNoCancelDialog;
    MessageDialog messageDialog;
    AssetManager assetManager;
    DraftModel draft;    
    DraftModelDataManager dataManager;
    List<Player> playerPoolList;
    Task barWorker;
    ReentrantLock progressLock;
    boolean keepAdding = false;
    TableColumn playerColumn;
    
    FilteredList<String> possiblePlayerPositions;
    
    private volatile boolean running = true; // Run unless told to pause
    
    public PlayerController(Stage initPrimaryStage, MessageDialog initMessageDialog,
                            YesNoCancelDialog initYesNoCancelDialog, 
                            DraftModelDataManager dataManager,
                            AssetManager assetManager) {
        this.dataManager = dataManager;
        this.draft = dataManager.getDraftModel();
        apd = new AddPlayerDialog(initPrimaryStage, initMessageDialog, dataManager);
        epd = new EditPlayerDialog(initPrimaryStage, initMessageDialog, dataManager, assetManager);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        this.assetManager = assetManager;
        
        progressLock = new ReentrantLock();
        
        // Initially add every fantasy team position to the list of this player's possible
        // positions
        possiblePlayerPositions = new FilteredList<String>(FXCollections.observableArrayList(DraftModel.getFantasyTeamPositions()), 
                                        pos -> true);
        
    }
    
    public PlayerController(DraftModelDataManager dataManager) {
        this.dataManager = dataManager;
        this.draft = dataManager.getDraftModel();  
        progressLock = new ReentrantLock();
        
        // Initially add every fantasy team position to the list of this player's possible
        // positions
        possiblePlayerPositions = new FilteredList<String>(FXCollections.observableArrayList(DraftModel.getFantasyTeamPositions()), 
                                        pos -> true);
        
        
    }

    /**
     * Displays a dialog for adding a fantasy team, and adds the new team to the
     * draft if the user fills the dialog.
     * 
     * @return  
     */
    public boolean handleAddPlayerRequest() {
        apd.showAddPlayerDialog();
        
        // Did the user confirm?
        if (apd.wasCompleteSelected()) {
            Player newPlayer = apd.getPlayer();
            
            // If there was a problem, the dialog will return null
            if (newPlayer != null) {
                // Add the new player to the player pool
                draft.addPlayerToPool(newPlayer);
                draft.sortPlayerPool();
                draft.calculateEstimatedValues();
                return true;
            }
        }
        else {
            // The user must have pressed cancel, so
            // we do nothing
        }
        
        return false;
    }
    
    public void startAutomatedDraft() {
        
        DraftScreen.pauseAutoDraftButton.setDisable(false);
        DraftScreen.pickPlayerButton.setDisable(true);
        DraftScreen.startAutoDraftButton.setDisable(true);
        running = true;
        
       // FantasyTeamsScreen.teamSelectionComboBox.valueProperty().removeListener(FantasyTeamsScreen.teamSelectionListener);
        
        Task task = new Task() {
            
            boolean keepAdding = true;
            @Override
            protected Void call() throws Exception {
                
                try {
                    progressLock.lock();
                    while (keepAdding && running) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                keepAdding = addRandomPlayerToDraft();               
                            }
                        });
                        
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }   System.out.println("DONE");
                    //FantasyTeamsScreen.teamSelectionComboBox.valueProperty().addListener(FantasyTeamsScreen.teamSelectionListener);
                }
               
                finally {
                    progressLock.unlock();
                }
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }
    
    public void pauseAutomatedDraft() {
         running = false;
         DraftScreen.pickPlayerButton.setDisable(false);
         DraftScreen.startAutoDraftButton.setDisable(false);
    }
    
    /**
     * Displays a dialog for adding a fantasy team, and adds the new team to the
     * draft if the user fills the dialog.
     * 
     * @param playerToRemove 
     * @return  
     */
    public boolean handleRemovePlayerRequest(Player playerToRemove) {
        yesNoCancelDialog.show("Do you really wish to remove the mighty " + playerToRemove.getFullName() + "?");
        
        // Did the user confirm?
        if (yesNoCancelDialog.wasYesSelected()) {
            // Remove the player from the player pool
            draft.removeFromPlayerPool(playerToRemove);
            draft.calculateEstimatedValues();
            return true;
        } else {
            // The user must have pressed cancel, so
            // we do nothing
        }
        
        return false;
    }
    
    public synchronized boolean addRandomPlayerToDraft() {
        
        playerPoolList = draft.getPlayerPool();
        String S2Contract = "S2";
        String XContract = "X";
        
        // Are we adding a taxi player?
        boolean addingTaxiPlayer = false;
        if (draft.allPositionsFilled()) {
            addingTaxiPlayer = true;
        }
        
        // Add one player to a team's roster
        if (!addingTaxiPlayer) {
            for (Team team : draft.getFantasyTeams()) {
                if (!team.hasFilledRoster()) {
                    for (Player player : playerPoolList) {
                        if (team.canAffordPlayer(1)) {
                            possiblePlayerPositions.setPredicate(pos -> {
                                    return player.getQualifiedPositionsList().contains(pos)
                                         && team.getUnfilledPositions().contains(pos);
                            });   
                            if (possiblePlayerPositions.isEmpty()) {
                                continue;
                            }
                            playerPoolList.remove(player);
                            player.setContract(S2Contract);
                            player.setSalary(1);
                            player.setPosition(possiblePlayerPositions.get(0));
                            team.addToRoster(player); 
                            team.decrementPurse(1);
                            draft.addPickToDraftOrder(player, team);
                            dataManager.refreshDraft();
                            draft.calculateTotalPoints();
                            draft.calculateEstimatedValues();
                            // As of this version of Java 8, table views do not always
                            // update as we would hope, so as a work-around, this will
                            // simply "refresh" the column so that any changed player data
                            // will be shown
                            playerColumn = (TableColumn) PlayersScreen.playerPoolTable.getColumns().get(0);
                            playerColumn.setVisible(false);
                            playerColumn.setVisible(true);
                            return true;
                        }        
                    }
                }
            }
        }
        // Add one player to a team's taxi squad
        else {
            for (Team team : draft.getFantasyTeams()) {
                if (!team.hasFilledTaxiSquad()) {
                    for (Player player : playerPoolList) {
                        if (team.canAffordTaxiPlayer()) {
                            playerPoolList.remove(player);
                            player.setContract(XContract);
                            player.setSalary(1);
                            player.setPosition(player.getQualifiedPositionsList().get(0));
                            team.addToTaxiSquad(player);
                            team.decrementPurse(1);
                            dataManager.refreshDraft();
                            draft.addPickToDraftOrder(player, team);
                            draft.calculateTotalPoints();
                            draft.calculateEstimatedValues();
                            // As of this version of Java 8, table views do not always
                            // update as we would hope, so as a work-around, this will
                            // simply "refresh" the column so that any changed player data
                            // will be shown
                            playerColumn = (TableColumn) PlayersScreen.playerPoolTable.getColumns().get(0);
                            playerColumn.setVisible(false);
                            playerColumn.setVisible(true);
                            return true;
                        }  
                    }
                }
            }
        }
        
       
        return false;
    }
    
    /**
     * Displays a dialog for editing a player not yet drafted to any fantasy
     * team, and either moves the player to the free agent pool
     *  
     * @param playerToEdit 
     * @param fantasyTeam 
     * @param automatedDraft 
     * @return  
     */
    public synchronized boolean handleEditFreeAgentRequest(Player playerToEdit, Team fantasyTeam, boolean automatedDraft) {
        epd.showEditPlayerDialog(playerToEdit, fantasyTeam, true);
        
        // Did the user confirm?
        if (epd.wasCompleteSelected() || automatedDraft) {
            Team teamToAddTo = epd.getFantasyTeam();            
            int salary = epd.getSalary();
            
            // Salary will be less than 0 if the input was invalid, and of course a
            // salary should not be negative unless this is slave labor
            if (salary < 0) {
               messageDialog.show("Invalid selection. Salary must be a positive integer value", automatedDraft);
               return false;
            } 
            
            // Check if we're adding to the taxi squad
            if (teamToAddTo.hasFilledRoster() || epd.getContract().equals("X")) {
                if (playerCanBeAddedToTaxiSquad(teamToAddTo, playerToEdit, epd.getContract())) {
                    playerToEdit.setSalary(1);     
                    teamToAddTo.decrementPurse(1);
                    playerToEdit.setPosition(epd.getPosition());
                    playerToEdit.setContract(epd.getContract());   
                    draft.removeFromPlayerPool(playerToEdit);
                    teamToAddTo.addToTaxiSquad(playerToEdit);
                    return true;
                } else {
                    messageDialog.show("Cannot add this player to taxi squad", automatedDraft);
                    return false;
                }
            }
            
            if (validateSalary(teamToAddTo, salary)) {
                
                teamToAddTo.decrementPurse(salary);
                // Change the values of the player based on the UI selections
                playerToEdit.setSalary(salary);            
                playerToEdit.setPosition(epd.getPosition());
                playerToEdit.setContract(epd.getContract());            

                // Take the player off the player pool and add him to the fantasy team
                draft.removeFromPlayerPool(playerToEdit);
                teamToAddTo.addToRoster(playerToEdit); 
                
                // If a player has contract S2, add it to the draft order
                if (epd.getContract().equals("S2")) {                    
                    draft.addPickToDraftOrder(playerToEdit, teamToAddTo);
                }
                return true;
            } else {
                messageDialog.show(teamToAddTo + " cannot afford " + playerToEdit.getFullName(), automatedDraft);
            }            
            
        } // Remove the player from this team and move him to the player pool
        else {
            // The user must have pressed cancel, so
            // we do nothing
        }
        
        return false;
    }
    
    /**
     * Displays a dialog for editing a player not yet drafted to any fantasy
     * team, and either moves the player to the free agent pool
     * 
     * @param playerToEdit 
     * @param oldTeam 
     * @return  
     */
    public boolean handleEditDraftedPlayerRequest(Player playerToEdit, Team oldTeam) {
        int previousSalary = playerToEdit.getSalary();
        epd.showEditPlayerDialog(playerToEdit, oldTeam, false);
        
        if (epd.wasFreeAgentSelected()) {            
                moveToFreeAgent(draft, oldTeam, playerToEdit);
                return true;
        }
        
        // Did the user confirm?
        if (epd.wasCompleteSelected()) {
            String oldContract = playerToEdit.getContract();
            boolean contractsChanged = !oldContract.equals(epd.getContract());
            
            Team newTeam = epd.getFantasyTeam();            
            int newSalary = epd.getSalary();
            String position = epd.getPosition();
            String playerName = playerToEdit.getFullName();
            
            // Salary will be less than 0 if the input was invalid
            if (newSalary < 0) {
               messageDialog.show("Invalid selection. Salary must be an integer value", false);
               return false;
            } 
            
            // A drafted player on the roster cannot be drafted to the taxi squad
            if (epd.getContract().equals("X")) {
                messageDialog.show("Cannot add player to taxi squad", false);
                return false;
            }
            
            // Player is changed from S2 to S1
//            if (!epd.getContract().equals("S2")) {
//                if (validateSalary(oldTeam, newSalary)) {
//                    playerToEdit.setContract(epd.getContract());
//                    playerToEdit.setSalary(newSalary); 
//                    oldTeam.incrementPurse(previousSalary);
//                    oldTeam.decrementPurse(newSalary);
//                    oldTeam.swapPlayerPosition(playerToEdit, playerToEdit.getPosition(), position); 
//                    draft.removeFromDraftOrder(playerToEdit);
//                    return true;
//                } else {
//                    messageDialog.show(oldTeam + " cannot afford " + playerName, false);
//                    return false;
//                }                
//            }
            
            playerToEdit.setContract(epd.getContract()); 
            
            // Take the player off the player pool and add him to the fantasy team
            if (epd.wasPlayerTraded()) {    
                if (validateSalary(newTeam, newSalary - previousSalary)) { 
                    if (tradePlayer(oldTeam, newTeam, playerToEdit, position, previousSalary, newSalary)) {                        
                         if (!epd.getContract().equals("S2")) {
                             draft.removeFromDraftOrder(playerToEdit);
                         } else {
                             if (contractsChanged) {
                                 draft.addPickToDraftOrder(playerToEdit, newTeam);
                             } else {
                                 draft.addTradeToDraftOrder(playerToEdit, newTeam);
                                 playerColumn = (TableColumn) DraftScreen.draftOrderTable.getColumns().get(0);
                                 playerColumn.setVisible(false);
                                 playerColumn.setVisible(true);                                 
                             }                
                             
                         }
                        return true;
                    } else {
                        messageDialog.show("Cannot add to roster or taxi squad", false);
                        return false;
                    }
                                     
                } else {
                    System.out.println("HERE1");
                    messageDialog.show(oldTeam + " cannot afford " + playerToEdit.getFullName(), false);
                    return false;
                }
            }  else if (epd.playerChangedPosition()) {
                if (validateSalary(oldTeam, newSalary - previousSalary)) {
                    swapPlayerPosition(oldTeam, previousSalary, newSalary, playerToEdit, position); 
                    if (!epd.getContract().equals("S2")) {
                        draft.removeFromDraftOrder(playerToEdit);
                    } else {
                        if (contractsChanged) {
                            draft.addPickToDraftOrder(playerToEdit, newTeam);
                        }
                    }
                    return true;
                } else {
                    messageDialog.show(oldTeam + " cannot afford " + playerName, false);
                    return false;
                }                               
            }  else { // Player didn't change team, but may have changed attributes
                if (validateSalary(oldTeam, newSalary - previousSalary)) {
                    playerToEdit.setSalary(newSalary); 
                    oldTeam.incrementPurse(previousSalary);
                    oldTeam.decrementPurse(newSalary);
                    oldTeam.swapPlayerPosition(playerToEdit, playerToEdit.getPosition(), position); 
                    if (!epd.getContract().equals("S2")) {
                        draft.removeFromDraftOrder(playerToEdit);
                    } else {
                        if (contractsChanged) {
                            draft.addPickToDraftOrder(playerToEdit, newTeam);
                        }
                    }
                    return true;
                } else {
                    messageDialog.show(oldTeam + " cannot afford " + playerName, false);
                    return false;
                }
            }
        } 
        else {
            // The user must have pressed cancel, so
            // we do nothing
        }
        
        return false;
    }
    
    private boolean playerCanBeAddedToTaxiSquad(Team team, Player player, String contract) {
        
        if (!team.canAffordTaxiPlayer()) {
            return false;
        }
        
        if (!draft.allPositionsFilled()) {
            return false;
        }
        
        if (team.hasFilledTaxiSquad() || !contract.equals("X")) {
            return false;
        } 
        
        return true;
    }
    
    private boolean tradePlayer(Team oldTeam, Team teamToAddTo, Player player, String position, int previousSalary, int salary) {
        
        teamToAddTo.decrementPurse(salary);
        player.setSalary(salary); 
        oldTeam.incrementPurse(previousSalary);
        oldTeam.removeFromRoster(player);
        player.setPosition(position); 
        teamToAddTo.addToRoster(player);
        
        return true;
    }
    private void moveToFreeAgent(DraftModel draft, Team team, Player player) {
        team.removeFromRoster(player);
        team.incrementPurse(player.getSalary());
        draft.addPlayerToPool(player);
        
    }
    private boolean swapPlayerPosition(Team oldTeam, int previousSalary, int newSalary, Player player, String position) {     
                
        player.setSalary(newSalary); 
        oldTeam.incrementPurse(previousSalary);
        oldTeam.decrementPurse(newSalary);
        oldTeam.swapPlayerPosition(player, player.getPosition(), position); 
        
        return true;
    }
    
    private boolean validateSalary(Team team, int salary) {
        return team.canAffordPlayer(salary);
    }
}
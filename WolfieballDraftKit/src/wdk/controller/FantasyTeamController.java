package wdk.controller;

import wdk.gui.MessageDialog;
import wdk.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import wdk.data.DraftModel;
import wdk.data.Team;
import wdk.gui.FantasyTeamDialog;
import wdk.gui.FantasyTeamsScreen;
import wdk.gui.PlayersScreen;

/**
 *
 * @author Kevin
 */
public class FantasyTeamController {
    FantasyTeamDialog ftd;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    String teamName;
    String teamOwner;
    DraftModel draft;
    
    public FantasyTeamController(Stage initPrimaryStage, DraftModel draft, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        this.draft = draft;
        ftd = new FantasyTeamDialog(initPrimaryStage, draft, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    /**
     * Displays a dialog for adding a fantasy team, and adds the new team to the
     * draft if the user fills the dialog.
     * 
     * @return 
     */
    public boolean handleAddFantasyTeamRequest() {
        //DraftModel draft = dataManager.getDraftModel();
        ftd.showAddFantasyTeamDialog();
        
        // Did the user confirm?
        if (ftd.wasCompleteSelected()) {            
            teamName = ftd.getTeamName();
            if (draft.fantasyTeamExists(teamName))  {
                messageDialog.show("That fantasy team already exists", false);
            } else {
                // Add the new fantasy team to the draft
                Team newTeam = new Team();
                teamOwner = ftd.getTeamOwner();
                newTeam.setName(teamName);
                newTeam.setOwner(teamOwner);
                draft.addFantasyTeam(newTeam);
                return true;
            }            
        }
        else {
            // The user must have pressed cancel, so
            // we do nothing
            return false;
        }
        
        return false;
    }
    
    public boolean handleEditFantasyTeamRequest(Team teamToEdit) {
        ftd.showEditFantasyTeamDialog(teamToEdit);
        
        // Did the user confirm?
        if (ftd.wasCompleteSelected()) {
            // Update the team
            teamToEdit.setName(ftd.getTeamName());
            teamToEdit.setOwner(ftd.getTeamOwner());
            
            refreshTeams();
            return true;
        } else {
            // The user must have pressed cancel, so
            // we do nothing
            return false;
        }        
    }
    
    public boolean handleRemoveFantasyTeamRequest(Team teamToRemove) {
        
        yesNoCancelDialog.show("Do you really wish to remove the mighty " + teamToRemove + "'s?");

        // If the user said yes, then remove the team
        if (yesNoCancelDialog.wasYesSelected()) { 
            //PlayersScreen.playerPoolTable
            draft.removeTeamAndReturnToPool(teamToRemove);
            return true;
        } else {
            // The user must have pressed cancel, so
            // we do nothing
            return false;
        }     
    }
    
    /**
     * At the time of writing this program, Java 8 doesn't update lists in GUIs,
     * but a workaround is done by adding and removing to the list which then 
     * updates the GUI.
     */
    private void refreshTeams() {
        draft.addFantasyTeam(null);
        draft.removeTeam(null);   
    }
    

}
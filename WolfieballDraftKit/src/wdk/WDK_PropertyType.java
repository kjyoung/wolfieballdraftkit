package wdk;

/**
 * These are properties that are to be loaded from properties.xml. They
 * will provide custom labels and other UI details for our Wolfieball Draft Kit
 * application. The reason for doing this is to swap out UI text and icons
 * easily without having to touch our code. It also allows for language
 * independence.
 * 
 * @author Kevin
 */
public enum WDK_PropertyType {
    
        // Loaded from properties.xml
        PROP_APP_TITLE,
        
        // Application icons for icons in the file toolbar
        NEW_DRAFT_ICON,
        LOAD_DRAFT_ICON,
        SAVE_DRAFT_ICON,
        EXPORT_SITE_ICON,
        
        // Application icons for icons in the screen toolbar
        FANTASY_TEAM_SCREEN_ICON,
        PLAYER_SCREEN_ICON,
        FANTASY_STANDINGS_SCREEN_ICON,
        DRAFT_SCREEN_ICON,
        MLB_TEAMS_SCREEN_ICON,
        
        // Application icons for icons not in a toolbar
        DELETE_ICON,
        EXIT_ICON,
        ADD_ICON,
        MINUS_ICON,
        EDIT_ICON,
        MOVE_UP_ICON,
        MOVE_DOWN_ICON,
        
        // Application tooltips for buttons in the file toolbar
        NEW_DRAFT_TOOLTIP,
        LOAD_DRAFT_TOOLTIP,
        SAVE_DRAFT_TOOLTIP,
        EXPORT_SITE_TOOLTIP,
        
        // Application tooltips for buttons in the screen toolbar
        FANTASY_TEAM_SCREEN_TOOLTIP,
        PLAYERS_SCREEN_TOOLTIP,
        FANTASY_STANDINGS_SCREEN_TOOLTIP,
        DRAFT_SCREEN_TOOLTIP,
        MLB_TEAMS_SCREEN_TOOLTIP, 
        
        // Fantasy Teams screen
        SELECT_FANTASY_TEAM_LABEL,
        ENTER_DRAFT_NAME_LABEL,
        STARTING_LINEUP_LABEL,
        TAXI_SQUAD_LABEL,
        ADD_TEAM_TOOLTIP,
        REMOVE_TEAM_TOOLTIP,
        EDIT_TEAM_TOOLTIP,
        
        // Fantasy Standings screen
        FANTASY_STANDINGS_LABEL,

        // Labels for the draft edit workspace
        FANTASY_STANDINGS_HEADING_LABEL,
        DRAFT_SCREEN_HEADING_LABEL,
        FANTASY_TEAM_HEADING_LABEL,
        MLB_TEAMS_HEADING_LABEL,
        
        // Draft screen
        DRAFT_ORDER_LABEL,
        PICK_ICON,
        START_ICON,
        PAUSE_ICON,
        PICK_PLAYER_TOOLTIP,
        START_AUTO_DRAFT_TOOLTIP,
        PAUSE_AUTO_DRAFT_TOOLTIP,
        
        // Players screen
        SEARCH_PLAYER_LABEL,
        PLAYER_SCREEN_HEADING_LABEL,
        DELETE_TOOLTIP,
        EXIT_TOOLTIP,
        ADD_PLAYER_TOOLTIP,
        REMOVE_PLAYER_TOOLTIP,  
        
        // MLB Teams Screen
        MLB_PLAYERS_LABEL,       
        SELECT_MLB_TEAM_LABEL,
        
        // Verification messages
        NEW_DRAFT_CREATED_MESSAGE,
        DRAFT_LOADED_MESSAGE,
        DRAFT_SAVED_MESSAGE,
        SITE_EXPORTED_MESSAGE,
        SAVE_UNSAVED_WORK_MESSAGE,
        REMOVE_ITEM_MESSAGE
        
        
}

 package wdk;

import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import wdk.data.DraftModelDataManager;
import wdk.file.JsonDraftFileManager;
import wdk.gui.WDK_GUI;
import properties_manager.PropertiesManager;
import xml_utilities.InvalidXMLFileFormatException;
import static wdk.WDK_StartupConstants.*;
import static wdk.WDK_PropertyType.*;
import wdk.data.Team;
import wdk.error.ErrorHandler;
import wdk.file.AssetManager;

/**
 * WolfieballDraftKit is a JavaFX application that can be used to build a draft
 * kit for fantasy baseball, which will help users to pick the best fantasy 
 * baseball team by giving them various player statistics, comparing teams, etc.
 * 
 * @author Kevin Young
 */
public class WolfieballDraftKit extends Application {
    
    // Set this to true only when we want information like 
    // player data, etc. printed to the console for debugging purposes
    public static boolean debug = false;
    
    // This is the full user interface, which will be initialized
    // after the properties file is loaded
    WDK_GUI gui;
    
    /**
     * This is where our Application begins its initialization, it will
     * create the GUI and initialize all of its components.
     * 
     * @param primaryStage This application's window.
     */
    @Override
    public void start(Stage primaryStage) {
        
        // Start by giving the primary stage to our error handler
        ErrorHandler eH = ErrorHandler.getErrorHandler();
        eH.initMessageDialog(primaryStage);
        
        // Load app settings into the gui and start it up
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(PROP_APP_TITLE);
            try {            
                // We will save our draft data using the json file
                // format so we'll let this object do this for us
                
                // The asset manage handles all images our program will need
                AssetManager assetManager = new AssetManager(PATH_PLAYER_PROFILE_IMAGES, PATH_FLAG_IMAGES);
                
                // Load all the appropriate assets to be held by the asset manager
                assetManager.loadPlayerProfileImages();
                assetManager.loadFlagImages();
                
                gui = new WDK_GUI(primaryStage);
                
                // Construct the data manager and give it the gui                
                DraftModelDataManager dataManager = new DraftModelDataManager(gui); 
                
                // We will save our Draft using the .json
                // format so we'll let this object do that for us
                JsonDraftFileManager jsonFileManager = new JsonDraftFileManager(dataManager, assetManager.getProfileImagesMap());
                
                // Load all of the players to the player pool
                jsonFileManager.loadPitchersToDefaultPlayerPool(JSON_FILE_PATH_PITCHERS);
                jsonFileManager.loadHittersToDefaultPlayerPool(JSON_FILE_PATH_HITTERS);                
                
                dataManager.getDraftModel().sortDefaultPlayerPool();
                
                gui.setDraftFileManager(jsonFileManager);
                gui.setDataManager(dataManager);

                // Initialize the UI components
                gui.initGUI(appTitle, assetManager);
                
                // Purely for debugging purposes.
                if (debug) {
                    // Print the players
                    System.out.println(dataManager.getDraftModel().playerPoolToString()+ "\n");
                }
                
            }
            catch(IOException ex) {
                System.out.println(ex.getMessage());
                eH = ErrorHandler.getErrorHandler();
                eH.handlePropertiesFileError();
            }
        }           
        
    }
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        try {
            // Load the settings for starting the app
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // Something went wrong initializing the xml file
            ErrorHandler eH = ErrorHandler.getErrorHandler();
            eH.handlePropertiesFileError();
            return false;
        }         
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

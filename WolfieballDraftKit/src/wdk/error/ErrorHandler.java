package wdk.error;

import static wdk.WDK_StartupConstants.CLOSE_BUTTON_LABEL;
import static wdk.WDK_StartupConstants.PROPERTIES_FILE_ERROR_MESSAGE;
import wdk.data.DraftModel;
import wdk.gui.MessageDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 * This class provides a response for every type of error we anticipate
 * inside of our program. It's convenient to do this in one place so
 * that we have a common format for dealing with problems.
 * 
 * @author Kevin
 */
public class ErrorHandler {
    
    // This class uses a singleton design patter, which is convenient
    // because it needs to be used by so many other classes
    static ErrorHandler singleton;
    
    // We'll make use of this dialog to provide our message feedback
    MessageDialog messageDialog;
    
    // The properties manager will give us the text to display
    PropertiesManager properties;

    /**
     * Note that this constructor is private and so can never be called
     * outside of this class.
     */
    private ErrorHandler() {
        // This helps us keep track of whether we need to
        // construct the singleton or not each time it's accessed
        singleton = null;
        
        // We only need to get the singleton once
        properties = PropertiesManager.getPropertiesManager();
    }
    
    /**
     * This method initializes this error handler's message dialog
     * so that it may provide feedback when errors occur.
     * 
     * @param owner The parent window for the modal message dialog.
     */
    public void initMessageDialog(Stage owner) {
        // We'll use this dialog to provide feedback when errors occur
        messageDialog = new MessageDialog(owner, CLOSE_BUTTON_LABEL);        
    }

    /**
     * Accessor method for getting this singleton.
     * 
     * @return The singleton ErrorHandler used by the entire
     * application for responding to error conditions.
     */
    public static ErrorHandler getErrorHandler() {
        // Initialize the singleton only the first time
        if (singleton == null)
            singleton = new ErrorHandler();
        
        // But always return it
        return singleton;
    }
    
    public void handleNewDraftError() {
        
    }
    
    public void handleLoadDraftError() {
        
    }
    
    public void handleSaveDraftError() {
        messageDialog.show("There was an error saving the draft", false);
    }
     
    public void handleExportDraftError(DraftModel draftBeingExported) {
        
    }
        
    public void handleExitError() {
        
    }

    public void handleUpdateDraftError() {
        
    }

    /**
     * This function provides feedback to the user when the properties.xml
     * file cannot be loaded.
     */
    public void handlePropertiesFileError() {
        messageDialog.show(properties.getProperty(PROPERTIES_FILE_ERROR_MESSAGE), false);
    }
}

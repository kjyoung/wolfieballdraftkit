package xml_utilities;

/**
 * The InvalidXMLFileFormatException is a checked exception that
 * represents the occasion where an XML document does not validate
 * against its schema (XSD).
 * 
 * @author  Kevin
 *          Debugging Enterprises
 * @version 1.0
 */
public class InvalidXMLFileFormatException extends Exception
{
    // Name of xml file that did not validate
    private String xmlFileWithError;
    
    // Name of xml schema used for validation
    private String xsdFile;

    /**
     * Constructor for this exception, these are simple objects,
     * we'll just store some info about the error.
     * 
     * @param initXMLFileWithError XML doc file name that didn't validate
     * 
     * @param initXSDFile XML schema file used in validation     
     */    
    public InvalidXMLFileFormatException(   String initXMLFileWithError,
                                            String initXSDFile)
    {
        // Keep it for later
        xmlFileWithError = initXMLFileWithError;
        xsdFile = initXSDFile;
    }
    
    /**
     * Constructor that records which xml file produced the error, but
     * not the schema.
     * 
     * @param initXMLFileWithError 
     */
    public InvalidXMLFileFormatException(   String initXMLFileWithError)
    {
        xmlFileWithError = initXMLFileWithError;
    }

    /**
     * This method builds and returns a textual description of this
     * object, which basically summarizes what went wrong.
     * 
     * @return This message will be useful for describing where
     * validation failed.
     */
    public String toString()
    {
        return "XML Document (" + xmlFileWithError 
                + ") does not conform to Schema (" + xsdFile + ")";
    }
}
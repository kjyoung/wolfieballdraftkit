/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk;

/**
 *
 * @author Kevin
 */
public class WDK_StartupConstants {
    
    public static final String PATH_DATA = "./data/";
    public static final String PATH_PLAYERS = PATH_DATA + "players/";   
    public static final String PATH_HITTERS = PATH_DATA + "hitters/"; 
    
    // THESE ARE THE DATA FILES WE WILL LOAD AT STARTUP
    public static final String JSON_FILE_PATH_PITCHERS = PATH_PLAYERS + "Pitchers.json";
    public static final String JSON_FILE_PATH_HITTERS = PATH_PLAYERS + "Hitters.json";
    
}

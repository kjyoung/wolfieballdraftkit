/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import wdk.data.DraftModel;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import wdk.data.DraftModelDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;

/**
 *
 * @author Kevin
 */
public class JsonDraftFileManager implements DraftFileManager {
    
    // JSON FILE READING AND WRITING CONSTANTS
    String JSON_PITCHERS = "Pitchers";
    String JSON_HITTERS = "Hitters";
    
    DraftModelDataManager dataManager;
     
    public JsonDraftFileManager (DraftModelDataManager dataManager) {
        this.dataManager = dataManager;
    }
    
    @Override
    public void saveDraft(DraftModel draftToSave) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadDraft(DraftModel draftToLoad, String draftPath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void savePlayers(List<Object> players, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadPitchersToNonDraftedPlayers(String pitchersFilePath) throws IOException {
        loadPitchersToNonDraftedPlayers(pitchersFilePath, JSON_PITCHERS);
    }
    
    @Override
    public void loadHittersToNonDraftedPlayers(String hittersFilePath) throws IOException {
        loadHittersToNonDraftedPlayers(hittersFilePath, JSON_HITTERS);
    }
    
    @Override
    public ArrayList<String> loadHitters(String hittersFilePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Loads a Json file as a single object and returns it.
     * 
     * @param jsonFilePath The location of the json file to load
     * 
     * @return The json file as a single object
     * @throws IOException 
     */
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        
        InputStream is = new FileInputStream(jsonFilePath);
        
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        
        jsonReader.close();
        is.close();
        
        return json;
    }    
    
    /**
     * Loads from the file containing all the pitchers, and loads each one to 
     * the collection of players who have not been drafted to a fantasy team
     * yet
     * 
     * @param pitchersJsonFilePath The location of the json file containing
     *                            all the pitchers data
     * 
     * @param pitchersArrayName    The name of the array that the data will be 
     *                            extracted from
     * @throws IOException 
     */
    private void loadPitchersToNonDraftedPlayers(String pitchersJsonFilePath, String pitchersArrayName) throws IOException {
        
        JsonObject jsonObj = loadJSONFile(pitchersJsonFilePath);
        JsonArray jsonArray = jsonObj.getJsonArray(pitchersArrayName);        
        
        for (int i = 0; i < jsonArray.size(); i++) {
            String teamName = jsonArray.getJsonObject(i).getString("TEAM");
            Pitcher pitcher = new Pitcher(  teamName,
                                            jsonArray.getJsonObject(i).getString("LAST_NAME"),
                                            jsonArray.getJsonObject(i).getString("FIRST_NAME"), 
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("H")),
                                            jsonArray.getJsonObject(i).getString("NOTES"),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("YEAR_OF_BIRTH")),
                                            jsonArray.getJsonObject(i).getString("NATION_OF_BIRTH"),
                                            "P",
                                            Double.parseDouble(jsonArray.getJsonObject(i).getString("IP")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("ER")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("W")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("SV")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("BB")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("K")));
            
           String playerName = pitcher.getFirstName() + " " + pitcher.getLastName();
           
           dataManager.getDraftModel().addToPlayerPool(pitcher);
        }
    }
    
    /**
     * Loads from the file containing all the hitters, and loads each one to 
     * the collection of players who have not been drafted to a fantasy team
     * yet
     * 
     * @param hittersJsonFilePath The location of the json file containing
     *                            all the hitters data
     * 
     * @param hittersArrayName    The name of the array that the data will be 
     *                            extracted from
     * @throws IOException 
     */
    private void loadHittersToNonDraftedPlayers(String hittersJsonFilePath, String hittersArrayName) throws IOException {
        
        JsonObject jsonObj = loadJSONFile(hittersJsonFilePath);
        JsonArray jsonArray = jsonObj.getJsonArray(hittersArrayName);        
        
        for (int i = 0; i < jsonArray.size(); i++) {
            String teamName = jsonArray.getJsonObject(i).getString("TEAM");
            
            Hitter hitter = new Hitter(     teamName,
                                            jsonArray.getJsonObject(i).getString("LAST_NAME"),
                                            jsonArray.getJsonObject(i).getString("FIRST_NAME"), 
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("H")),
                                            jsonArray.getJsonObject(i).getString("NOTES"),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("YEAR_OF_BIRTH")),
                                            jsonArray.getJsonObject(i).getString("NATION_OF_BIRTH"),
                                            jsonArray.getJsonObject(i).getString("QP"),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("AB")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("R")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("HR")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("RBI")),
                                            Integer.parseInt(jsonArray.getJsonObject(i).getString("SB")));
            
           String playerName = hitter.getFirstName() + " " + hitter.getLastName();
           
           dataManager.getDraftModel().addToPlayerPool(hitter);
        }
    }

    @Override
    public void loadPitchers(String pitchersFilePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
    
}

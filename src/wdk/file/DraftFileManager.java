package wdk.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import wdk.data.DraftModel;

/**
 * This interface provides an abstraction of what a file manager should do. Note
 * that file managers know how to read and write courses, instructors, and subjects,
 * but now how to export sites.
 * 
 * @author Richard McKenna
 */
public interface DraftFileManager {
    public void                 saveDraft(DraftModel draftToSave) throws IOException;
    public void                 loadDraft(DraftModel draftToLoad, String draftPath) throws IOException;
    public void                 savePlayers(List<Object> players, String filePath) throws IOException;
    public void                 loadPitchers(String pitchersFilePath) throws IOException;
    public ArrayList<String>    loadHitters(String hittersFilePath) throws IOException;
    public void                 loadPitchersToNonDraftedPlayers(String pitchersFilePath) throws IOException;
    public void                 loadHittersToNonDraftedPlayers(String hittersFilePath) throws IOException;
    
}


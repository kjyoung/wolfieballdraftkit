/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Kevin
 */
public class Pitcher extends Player {
    
    private SimpleDoubleProperty IP;
    private SimpleIntegerProperty ER;
    private SimpleIntegerProperty W;
    private SimpleIntegerProperty SV;
    private SimpleIntegerProperty BB;
    private SimpleIntegerProperty K;
    private SimpleDoubleProperty WHIP;
    private SimpleDoubleProperty ERA;
    
    /**
     *
     * @param profTeam
     * @param lastName
     * @param firstName
     * @param H
     * @param notes
     * @param IP
     * @param ER
     * @param W
     * @param qualifiedPositions
     * @param SV
     * @param yearOfBirth
     * @param nationOfBirth
     * @param BB
     * @param K
     */
    public Pitcher(String  profTeam, String lastName, String firstName, int H, 
            String notes, int yearOfBirth, String nationOfBirth, String qualifiedPositions,
            double IP, int ER, int W, int SV, int BB, int K) {
        
        super(profTeam, lastName, firstName, H, notes, yearOfBirth, nationOfBirth, qualifiedPositions);
        this.IP = new SimpleDoubleProperty(IP);
        this.ER = new SimpleIntegerProperty(ER);
        this.W = new SimpleIntegerProperty(W);
        this.SV = new SimpleIntegerProperty(SV);
        this.BB = new SimpleIntegerProperty(BB);
        this.K = new SimpleIntegerProperty(K); 
        
        // Make sure we are not dividing by zero
        if (IP == 0) {                
            this.WHIP = new SimpleDoubleProperty(0);
            this.ERA = new SimpleDoubleProperty(0);
        } else {
            // WHIP is calculated by (Walks + Hits)/Innings Pitched
            this.WHIP = new SimpleDoubleProperty((W + H)/IP); 
            
            // ERA is calculated by 9 × Earned Runs Allowed / Innings Pitched
            this.ERA = new SimpleDoubleProperty(9 * (ER/IP)); 
        }
    }
    
    public SimpleIntegerProperty getWProperty() {
        return W;
    }
    
    public double getERA() {
        return ERA.get();
    }
    
    public double getIP() {
        return IP.get();
    }
    
    public int getER() {
        return ER.get();
    }
    
    public int getW() {
        return W.get();
    }
    
    public int getSV() {
        return SV.get();
    }
    
    public int getBB() {
        return BB.get();
    }
    
    public double getK() {
        return K.get();
    }
    
    public double getWHIP() {
        return WHIP.get();
    }

    @Override
    public String toString() {
        
        String pitcherString = "PITCHER\n";
        pitcherString += super.toString();
        pitcherString += "IP: " + IP
                + "\nER: " + ER
                + "\nWHIP: " + WHIP
                + "\nSV: " + SV
                + "\nBB: " + BB
                + "\nK: " + K;
        
       return pitcherString;                       
    }
    
}

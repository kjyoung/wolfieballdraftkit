/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Kevin
 */
public class Player {
    
    //Below we will define simpleStringProperties to Populate the tableView
    SimpleStringProperty profTeam, lastName, firstName, notes, nationOfBirth, 
                         qualifiedPositions;
    SimpleIntegerProperty H, yearOfBirth;
    
    public Player(String profTeam, String lastName, String firstName,
                  int H, String notes, int yearOfBirth, String nationOfBirth,
                  String qualifiedPositions) {
        this.profTeam = new SimpleStringProperty(profTeam);
        this.lastName =  new SimpleStringProperty(lastName);
        this.firstName =  new SimpleStringProperty(firstName);
        this.H =  new SimpleIntegerProperty(H);
        this.notes =  new SimpleStringProperty(notes);
        this.yearOfBirth =  new SimpleIntegerProperty(yearOfBirth);
        this.nationOfBirth =  new SimpleStringProperty(nationOfBirth);   
        this.qualifiedPositions = new SimpleStringProperty(qualifiedPositions);
    }
    
    /**
     * Return the first and last name concatenated together
     * 
     * @return This Player's name as "FirstName  LastName"
     */
    public String getFullName() {
        return firstName.get() + " " + lastName.get();
    }    
    
    public String getQualifiedPositions() {
        return qualifiedPositions.get();
    }

    public void setQualifiedPositions(String qualifiedPositions) {
        this.qualifiedPositions.set(qualifiedPositions);
    }
    
    public String getFirstName() {
        return firstName.get();
    }
    
    public String getLastName() {
        return lastName.get();
    }

    public String getProfTeam() {
        return profTeam.get();
    }

    public void setProfTeam(String profTeam) {
        this.profTeam.set(profTeam);
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getNotes() {
        return notes.get();
    }

    public void setNotes(String notes) {
        this.notes.set(notes);
    }

    public String getNationOfBirth() {
        return nationOfBirth.get();
    }

    public void setNationOfBirth(String nationOfBirth) {
        this.nationOfBirth.set(nationOfBirth);
    }

    public int getH() {
        return H.get();
    }

    public void setH(int H) {
        this.H.set(H);
    }

    public int getYearOfBirth() {
        return yearOfBirth.get();
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth.set(yearOfBirth);
    }
    
    @Override 
    public String toString() {
        
        String playerString = "";
        playerString += "Professional Team: " + profTeam 
                + "\nLast name: " + lastName 
                + "\nFirst name: " + firstName
                + "\nYear of Birth: " + yearOfBirth
                + "\nNation of Birth: " + nationOfBirth
                + "\nQualified Positions: " + qualifiedPositions.toString() 
                + "\nNotes: " + notes
                + "\nH: " + H + "\n";
        return playerString;
    }
    
    
}

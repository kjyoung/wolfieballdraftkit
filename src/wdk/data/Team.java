/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Kevin
 */
public class Team {
    
    private String name;
    
    private String owner;
    
    // All the players that are part of this team
    private HashMap<String, Player> roster;
    
    private HashMap<Position, Player> taxiSquad;
    
    public Team(String name) {
        this.name = name;
        
        roster = new HashMap();
        taxiSquad = new HashMap();
    }
    
    public void addToRoster(Player newPlayer, String newPlayerName) {
        roster.put(newPlayerName, newPlayer);
    }
    
    @Override
    public String toString() {
        String teamString = name;
        for (Map.Entry<String, Player> entry : roster.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            teamString += "\nPlayer: " + key + "\nStats:\n"
                    + value.toString();
        }
        return teamString;
    }
    
    
    
    
    
    
}

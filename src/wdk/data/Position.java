package wdk.data;

/**
 * This type represents the various positions that a baseball player may have
 *
 * @author Kevin
 */
public enum Position {
    C,                  // Catcher
    FIRSTBASEMAN,
    THIRDBASEMAN,
    CI,                 // Corner Infielder
    SECONDBASEMAN,
    SS,                 // Shortstop
    MI,                 // Middle Infielder
    OF,                 // Outfielder
    U                   // Utility
}

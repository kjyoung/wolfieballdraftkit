/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Kevin
 */
public class DraftModel {
    
    // All the players that are part of this team
    HashMap<String, Team> teams;
    
    ObservableList<Player> playerPool;
    
    // Arraylist because we want to keep track of the order that players are
    // drafted
    ObservableList<Player> draftedPlayers;
    
    public DraftModel() {
        teams = new HashMap<>();
        playerPool = FXCollections.observableArrayList();
        draftedPlayers = FXCollections.observableArrayList();
    }
    
    public ObservableList<Player> getPlayerPool() {
        return playerPool;
    }
    
    public void addTeam(String teamName, Team team) {
        teams.put(teamName, team);
    }
    
    public Team removeTeam(String teamName) {
        return teams.remove(teamName);
    }
    
    public void addPlayerToTeam(String playerName, Player player, String teamName) {
        
        if (!teams.containsKey(teamName)) {
            addTeam(teamName, new Team(teamName));
        } 
        
        teams.get(teamName).addToRoster(player, playerName);
        
    }
    
    public void removeFromPlayerPool(Player player) {
        playerPool.remove(player);
    }  
    
    public void addToPlayerPool(Player player) {
        playerPool.add(player);
    }
    
    public String teamsToString() {
        return teams.toString();
    }
    
    public String playerPoolToString() {
        
        String playersString = "";
        for (Player player : playerPool) {
            playersString += player.toString();
        }
        return playersString;
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Kevin
 */
public class Hitter extends Player {
    
    private SimpleIntegerProperty AB, R, HR, RBI, SB;
    
    private SimpleDoubleProperty BA;
    
    public Hitter(String profTeam, String lastName, String firstName, int H, 
            String notes, int yearOfBirth, String nationOfBirth, String qualifiedPositions,
            int AB, int R, int HR, int RBI, int SB) {
        super(profTeam, lastName, firstName, H, notes, yearOfBirth, nationOfBirth, qualifiedPositions);
        this.AB = new SimpleIntegerProperty(AB);
        this.R = new SimpleIntegerProperty(R);
        this.HR = new SimpleIntegerProperty(HR);
        this.RBI = new SimpleIntegerProperty(RBI);
        this.SB = new SimpleIntegerProperty(SB);
        
         // Make sure we are not dividing by zero
        if (AB == 0) {
            this.BA = new SimpleDoubleProperty(0);
        } else {
            
            // Batting average is calculated as Hits/At Bats
            this.BA = new SimpleDoubleProperty((double)H / AB); 
        }
    }
    
    public int getAB() {
        return AB.get();
    }
    
    public int getR() {
        return R.get();
    }
    
    public int getHR() {
        return HR.get();
    }
    
    public int getRBI() {
        return RBI.get();
    }
    
    public int getSB() {
        return SB.get();
    }
    
    public double getBA() {
        return BA.get();
    }
    
    @Override
    public String toString() {
        String hitterString = "HITTER\n";
        hitterString += super.toString();
        hitterString += "BA: " + BA
                + "\nR: " + R
                + "\nHR: " + HR
                + "\nRBI: " + RBI
                + "\nSB: " + SB;
        
       return hitterString;                       
    }
 }
    


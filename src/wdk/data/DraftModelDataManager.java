package wdk.data;

/**
 * This class manages a DraftModel, which means it knows how to
 * reset one with default values. Whenever any other class tries
 * to access the data of the DraftModel being worked on,
 * it should access the DraftModel from this class.
 * 
 * @author Kevin
 */
public class DraftModelDataManager {
    
    // This is the UI, which must be updated
    // whenever our model's data changes
    DraftModelDataView view;
    
    // The DraftModel which this class will manager
    DraftModel draftModel;
    
    public DraftModelDataManager(DraftModelDataView initView) {
        view = initView;
        draftModel = new DraftModel();
    }
    
    public void reset() {
        // For the UI to reload the updated DraftModel
        view.reloadDraftModel(draftModel);  
    }
    
    public DraftModel getDraftModel() {
        return draftModel;
    }
    
}

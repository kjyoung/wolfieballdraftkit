package wdk.controller;

import wdk.data.DraftModelDataManager;
import wdk.gui.WDK_GUI;
import wdk.gui.YesNoCancelDialog;

/**
 * This controller class provides responses to interactions with the buttons in
 * the file toolbar.
 *
 * @author Kevin Young
 */
public class FileController {
    
    YesNoCancelDialog yesNoCancelDialog;
    
    public FileController(YesNoCancelDialog initYesNoCancelDialog) {
        yesNoCancelDialog = initYesNoCancelDialog;
    }
    
    /**
     * This method starts the process of editing a new Course. If a course is
     * already being edited, it will prompt the user to save it first.
     * 
     * @param gui The user interface editing the Course.
     */
    public void handleNewDraftModelRequest(WDK_GUI gui) {
        // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                DraftModelDataManager dataManager = gui.getDataManager();
                dataManager.reset();
        /*try {
             WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
             if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                CourseDataManager dataManager = gui.getDataManager();
                dataManager.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                gui.updateToolbarControls(saved);
                
                // TELL THE USER THE COURSE HAS BEEN CREATED
                messageDialog.show(properties.getProperty(NEW_COURSE_CREATED_MESSAGE));
                System.out.println("SHOW NEW COURSE");
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG, PROVIDE FEEDBACK
            errorHandler.handleNewCourseError();
        }
                    */
    }
}

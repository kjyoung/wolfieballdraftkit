/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import static wdk.WDK_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wdk.WDK_StartupConstants.JSON_FILE_PATH_PITCHERS;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.data.Player;
import wdk.file.JsonDraftFileManager;
import wdk.gui.PlayerScreen;
import wdk.gui.WDK_GUI;

/**
 *
 * @author Kevin
 */
public class WolfieballDraftKit extends Application {
    
    public static boolean debug = false;
    
    WDK_GUI gui;
    
    @Override
    public void start(Stage primaryStage) {
                  
        // Our graphical user interface
        gui = new WDK_GUI(primaryStage);
        
        // Construct the data manager and give it the gui
        DraftModelDataManager dataManager = new DraftModelDataManager(gui); 
        gui.setDataManager(dataManager);
        
        // We will save our Draft Model using the .json
        // format so we'll let this object do that for us
        JsonDraftFileManager jsonFileManager = new JsonDraftFileManager(dataManager);
        
        try {            
            // Load all of the players to the player pool
            jsonFileManager.loadPitchersToNonDraftedPlayers(JSON_FILE_PATH_PITCHERS);
            jsonFileManager.loadHittersToNonDraftedPlayers(JSON_FILE_PATH_HITTERS);
        } catch (IOException e) {
            
        }
        
        // Initialize the UI components
        gui.initGUI("WDK!");
        
        if (debug) {
            // Print the players
            System.out.println(dataManager.getDraftModel().playerPoolToString()+ "\n");
        }
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import javafx.scene.text.Font;
import java.text.DecimalFormat;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import wdk.data.DraftModelDataManager;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;

/**
 *
 * @author Kevin
 */
public class PlayerScreen {
    
    // The primary pane of this screen
    VBox playerScreenPane;
    
    // Manages and holds the data of the Draft we are currently editing
    DraftModelDataManager dataManager;
    
    // Will prompt the user to confirm or back out of a selection
    YesNoCancelDialog yesNoCancelDialog;
    
    // The table of all the players in the player pool and its columns
    TableView<Player> playerPoolTable;
    
    // The columns for the player pool table
    TableColumn firstNameCol;
    TableColumn lastNameCol;
    TableColumn proTeamCol;
    TableColumn posCol;
    TableColumn birthYearCol;
    TableColumn R_WCol;
    TableColumn HR_SVCol;
    TableColumn RBI_KCol;
    TableColumn SB_ERACol;
    TableColumn BA_WHIPCol;
    TableColumn estValueCol;
    TableColumn notesCol;
    
    // The text that the table columns display
    static final String COL_FIRST_NAME = "First";
    static final String COL_LAST_NAME = "Last";
    static final String COL_PRO_TEAM = "Pro Team";
    static final String COL_POSITIONS = "Positions";
    static final String COL_BIRTH_YEAR = "Year of Birth";
    static final String COL_R_W = "R/W";
    static final String COL_HR_SV = "HR/SV";
    static final String COL_RBI_K = "RBI/K";
    static final String COL_SB_ERA = "SB/ERA";
    static final String COL_BA_WHIP = "BA/WHIP";
    static final String COL_EST_VALUE = "Estimated Value";
    static final String COL_NOTES = "Notes";
    
    // The RadioButtons that when clicked, will determine what type of Players
    // to show in the table based on the Player's position    
    HBox paneForRadioButtons;
    RadioButton rbAll;
    RadioButton rbC;
    RadioButton rb1B;
    RadioButton rbCl;
    RadioButton rb3B;
    RadioButton rb2B;
    RadioButton rbMI;
    RadioButton rbSS;
    RadioButton rbOF;
    RadioButton rbU; // All hitters
    RadioButton rbP; // All pitchers 
    
    // The textual representation of the position radiobuttons
    String RB_ALL = "All";
    String RB_C = "C";
    String RB_1B = "1B";
    String RB_Cl = "Cl";
    String RB_3B = "3B";
    String RB_2B = "2B";
    String RB_MI = "MI";
    String RB_SS = "SS";
    String RB_OF = "OF";
    String RB_U = "U";
    String RB_P = "P";
    
    // Will control all of the position radiobuttons
    ToggleGroup positionSelectionGroup;
    
    // By typing in the search text field, the table should only display players
    // whose first or last names start with that text (ignore case)
    TextField playerSearchBar;    
    static String playerSearchBarText = "";   
    
    // This will make it easier to filter out the players we don't want to show
    // based on the search bar and position buttons
    FilteredList<Player> filteredPlayerList;
    
    // These will format the numbers of certain player stats so they look 
    // nice in the table
    DecimalFormat BA_FORMAT; 
    DecimalFormat WHIP_FORMAT; 
    DecimalFormat ERA_FORMAT; 
    
    // Will hold the player pool table
    VBox playerPoolBox;
    
    Label playerPoolLabel;
    
    Button showDialogButton;
    
    public PlayerScreen(YesNoCancelDialog initYesNoCancelDialog, 
                        DraftModelDataManager dataManager) {
        
       // playerScreenPane = new GridPane();
       playerScreenPane = new VBox();
       
       this.dataManager = dataManager;
       
       yesNoCancelDialog = initYesNoCancelDialog;
       
       BA_FORMAT = new DecimalFormat("#.###");
       WHIP_FORMAT = new DecimalFormat("#.##");
       ERA_FORMAT = new DecimalFormat("#.##"); 
       
       // Wrap the ObservableList in a FilteredList (initially display all data).
       filteredPlayerList = new FilteredList<>(dataManager.getDraftModel().
                                                    getPlayerPool(), p -> true);
    }
    
    public void initGUI() {
        
       initButtons();
       
       initRadioButtons();
       
       initPlayerPoolTable();
       
       initPlayerSearchBar();  
       
       initEventHandlers();       
       
       GridPane topPane = new GridPane();
       Label l = new Label("Available Players");
       Label searchLabel = new Label("Search: ");
       l.setFont(new Font("Arial", 30));
       ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(20);
         ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(100);
        topPane.getColumnConstraints().add(column1);
        topPane.getColumnConstraints().add(column2);
       topPane.add(l, 0, 0);
       topPane.add(searchLabel, 0, 1);
       topPane.add(playerSearchBar, 1, 1);
       
       
       playerScreenPane.getChildren().add(topPane);       
       playerScreenPane.getChildren().add(paneForRadioButtons);       
       playerScreenPane.getChildren().add(playerPoolBox);  
    }
    
    private void initRadioButtons() {
        
        paneForRadioButtons = new HBox(100);
        paneForRadioButtons.setPadding(new Insets(5,5,5,5));
        
        // Initialize all the radio buttons
        rbAll = new RadioButton(RB_ALL);
        rbC = new RadioButton(RB_C);
        rb1B = new RadioButton(RB_1B);
        rbCl = new RadioButton(RB_Cl);
        rb3B = new RadioButton(RB_3B);
        rb2B = new RadioButton(RB_2B);
        rbMI = new RadioButton(RB_MI);
        rbSS  = new RadioButton(RB_SS);
        rbOF = new RadioButton(RB_OF);
        rbU = new RadioButton(RB_U);
        rbP = new RadioButton(RB_P); 
        
        /* Set the user data of each button to the position it corresponds to
         this is so when we do the event handle, we'll have an easier way
         of knowing what position player we want when a button is toggled */
        rbAll.setUserData(RB_ALL);
        rbC.setUserData(RB_C);
        rb1B.setUserData(RB_1B);
        rbCl.setUserData(RB_Cl);
        rb3B.setUserData(RB_3B);
        rb2B.setUserData(RB_2B);
        rbMI.setUserData(RB_MI);
        rbSS.setUserData(RB_SS);
        rbOF.setUserData(RB_OF);
        rbU.setUserData(RB_U);
        rbP.setUserData(RB_P);        
        
        // Now add all of the radio buttons to the toggle group
        positionSelectionGroup = new ToggleGroup();
        rbAll.setToggleGroup(positionSelectionGroup);
        rbC.setToggleGroup(positionSelectionGroup);
        rb1B.setToggleGroup(positionSelectionGroup);
        rbCl.setToggleGroup(positionSelectionGroup);
        rb3B.setToggleGroup(positionSelectionGroup);
        rb2B.setToggleGroup(positionSelectionGroup);
        rbMI.setToggleGroup(positionSelectionGroup);
        rbSS.setToggleGroup(positionSelectionGroup);
        rbOF.setToggleGroup(positionSelectionGroup);
        rbU.setToggleGroup(positionSelectionGroup);
        rbP.setToggleGroup(positionSelectionGroup);
        
        // Finally add the radio buttons to the pane
        paneForRadioButtons.getChildren().addAll(rbAll, rbC, rb1B, rbCl, rb3B, 
                rb2B, rbMI, rbSS, rbOF, rbU, rbP);
    }
    
    private void initButtons() {
        showDialogButton = new Button("Show yes no dialog");
    }
    
    private void initEventHandlers() {
        showDialogButton.setOnAction(e -> showDialog());
        
        /* When the notes column is clicked, the user can edit the field
         which will change that Player's notes value */
        notesCol.setOnEditCommit(new EventHandler<CellEditEvent<Player, String>>() {                
            @Override
            public void handle(CellEditEvent<Player, String> player) {
                ((Player) player.getTableView().getItems().get(
                        player.getTablePosition().getRow())).
                        setNotes(player.getNewValue());
            }                  
        });                
                
        /* This search bar acts in real time, and when the user inputs text,
         the table will display all the Players with a first or last name that 
         matches that text */
        playerSearchBar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredPlayerList.setPredicate(player -> {
                
                // Compare the first and last name of every player in the table 
                // with the text entered
                boolean nameMatchesSearch = (player.getFirstName().startsWith(newValue)
                                || player.getLastName().startsWith(newValue));
                
                playerSearchBarText = newValue;
                
                // The currently selected position button
                Toggle currentPositionToggle = positionSelectionGroup.getSelectedToggle();
                
                if (currentPositionToggle == null) {
                    
                    // If filter text is empty, display all of the players
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    
                    return nameMatchesSearch;
                    
                } else {
                    
                    String selectedPosition = currentPositionToggle.getUserData().toString();
                    
                    switch (selectedPosition) {
                        case "All":
                            return nameMatchesSearch;
                        case "U":
                            return nameMatchesSearch && (player instanceof Hitter);
                        default:
                            return nameMatchesSearch && player.getQualifiedPositions().
                                                    contains(selectedPosition);
                    }
                    
                }
            });
        });        
        
        // Now trap the FilteredList of Players in a SortedList.
        SortedList<Player> sortedPlayerData = new SortedList<>(filteredPlayerList);

        // And bind the SortedList comparator to the table comparator.
        sortedPlayerData.comparatorProperty().bind(playerPoolTable.comparatorProperty());

        // Finally add the sorted (and filtered) data to the table
        playerPoolTable.setItems(sortedPlayerData);        
        
        /* When one of the buttons is toggled, the player pool table contents
        change to show only the players whose qualified positions corresponds
        to the toggled button.      
        Keep in mind we also have a search bar, so we have to handle that too */
        positionSelectionGroup.selectedToggleProperty().addListener((
                ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
                    
            /* If one of the Toggles are clicked, then we have to change what type
             of Players are shown in the table, based on their qualified positions */
            if (positionSelectionGroup.getSelectedToggle() != null) {

                String selectedPosition = positionSelectionGroup.getSelectedToggle().
                                                getUserData().toString();               

                filteredPlayerList.setPredicate(player -> {
                    boolean nameMatchesSearch = (player.getFirstName().startsWith(playerSearchBarText)
                            || player.getLastName().startsWith(playerSearchBarText));

                    /* If there is nothing in the search bar, then we only 
                    have to worry about the qualified positions of the player */
                    if (playerSearchBarText.equals("")) {
                        switch (selectedPosition) {
                            case "All":
                                return true;
                            case "U":
                                return (player instanceof Hitter);
                            default: 
                                return player.getQualifiedPositions().contains(selectedPosition)
                                       && nameMatchesSearch;
                        } 
                    } // In this case, we have to worry about the positions AND the player name
                    else {  
                        switch (selectedPosition) {
                            case "All": 
                               return nameMatchesSearch;
                            case "U":
                               return (player instanceof Hitter) && nameMatchesSearch;
                            default:       
                               return player.getQualifiedPositions().contains(selectedPosition) 
                                      && nameMatchesSearch;
                        }  
                    }
                }); // closes the filteredlist predicate
            } 
                            
        });        
    }
    
    private void showDialog() {
        yesNoCancelDialog.show("On player screen");        
    }
    
    /**
     * Initialize the search bar that will search 
     */
    private void initPlayerSearchBar() {
        
        // Initialize the search bar
        playerSearchBar = new TextField("");
        playerSearchBar.setPromptText("Search:");      
    }
    
    @SuppressWarnings("Convert2Lambda")
    private void initPlayerPoolTable() {
        
        playerPoolTable = new TableView();  
        
        //playerPoolBox.getStyleClass().add(CLASS_BORDERED_PANE);
        //lecturesToolbar = new HBox();
        playerPoolBox = new VBox();
        playerPoolLabel = initLabel("Available Players", "needs to be implemented");
        
        playerPoolBox.getChildren().add(playerPoolLabel);
        playerPoolBox.getChildren().add(playerPoolTable);
        
        // We want the notes cell to be editable so set the table to editable
        playerPoolTable.setEditable(true);
        
        // Initialize all of the Table columns
        firstNameCol = new TableColumn(COL_FIRST_NAME);
        lastNameCol = new TableColumn(COL_LAST_NAME);
        proTeamCol = new TableColumn(COL_PRO_TEAM);
        posCol = new TableColumn(COL_POSITIONS);
        birthYearCol = new TableColumn(COL_BIRTH_YEAR);
        R_WCol = new TableColumn(COL_R_W);
        HR_SVCol = new TableColumn(COL_HR_SV);
        RBI_KCol = new TableColumn(COL_RBI_K);
        SB_ERACol = new TableColumn(COL_SB_ERA);
        BA_WHIPCol = new TableColumn(COL_BA_WHIP);
        estValueCol = new TableColumn(COL_EST_VALUE);
        notesCol = new TableColumn(COL_NOTES);
        
        // Now link the columns to the data
        firstNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        proTeamCol.setCellValueFactory(new PropertyValueFactory<String, String>("profTeam"));
        posCol.setCellValueFactory(new PropertyValueFactory<String, String>("qualifiedPositions"));
        birthYearCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("yearOfBirth"));        
        estValueCol.setCellValueFactory(new PropertyValueFactory<Integer, String>("estValue"));
        notesCol.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));   
        
        // Make the notes column editable
        notesCol.setCellFactory(TextFieldTableCell.forTableColumn());        
        
        /* The following columns hold one of two data fields of a Player, depending on
         if the Player is a Hitter or a Pitcher. So for each column field, we must
         link the column to the data depending on what instance of Player
         the TableView row contains. */
        R_WCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, String>, ObservableValue<String>>() {
             
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Player, String> player) {
                if (player.getValue() instanceof Pitcher) {
                     return new SimpleStringProperty(((Pitcher)player.getValue()).getW() + "");
                }
                else {  
                    return new SimpleStringProperty(((Hitter)player.getValue()).getH() + "");
                }
            }
        });
        
        HR_SVCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, String>, ObservableValue<String>>() {
             
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Player, String> player) {
                if (player.getValue() instanceof Pitcher) {
                     return new SimpleStringProperty(((Pitcher)player.getValue()).getSV() + "");
                }
                else {
                    return new SimpleStringProperty(((Hitter)player.getValue()).getHR() + "");
                }
            }
        });
        
        RBI_KCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, String>, ObservableValue<String>>() {
             
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Player, String> player) {
                if (player.getValue() instanceof Pitcher) {
                     return new SimpleStringProperty(((Pitcher)player.getValue()).getK() + "");
                }
                else {
                    return new SimpleStringProperty(((Hitter)player.getValue()).getRBI() + "");
                }
            }
        });
        
        SB_ERACol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, String>, ObservableValue<String>>() {
             
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Player, String> player) {
                if (player.getValue() instanceof Pitcher) {
                     return new SimpleStringProperty(ERA_FORMAT.format(((Pitcher)player.getValue()).getERA()));
                }
                else {
                    return new SimpleStringProperty(((Hitter)player.getValue()).getSB() + "");
                }
            }
        });
        
        BA_WHIPCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Player, String>, ObservableValue<String>>() {
             
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Player, String> player) {
                if (player.getValue() instanceof Pitcher) {
                    return new SimpleStringProperty(WHIP_FORMAT.format(((Pitcher)player.getValue()).getWHIP()));
                }
                else {                    
                    return new SimpleStringProperty(BA_FORMAT.format(((Hitter)player.getValue()).getBA()));
                }
            }
        });
        
        // Finally add all the columns to the Player pool Table        
        playerPoolTable.getColumns().addAll(firstNameCol, lastNameCol, proTeamCol, 
                                            posCol, birthYearCol, R_WCol, HR_SVCol,
                                            RBI_KCol, SB_ERACol, BA_WHIPCol, estValueCol,
                                            notesCol); 
        
        /* The table contents is binded to the list of all the players in the
         Player pool */
        playerPoolTable.setItems(dataManager.getDraftModel().getPlayerPool()); 
        
        
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(String labelText, String styleClass) {
        Label label = new Label(labelText);
       //label.getStyleClass().add(styleClass);
        return label;
    }

    public Pane getPane() {
        return playerScreenPane;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.util.ArrayList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import wdk.controller.FileController;
import wdk.data.DraftModel;
import wdk.data.DraftModelDataManager;
import wdk.data.DraftModelDataView;

/**
 *
 * @author Kevin
 */
public class WDK_GUI implements DraftModelDataView {
    
    private boolean workspaceActivated;
    
    // Controls how the program responds to user interactions with the
    // file toolbar 
    FileController fileController;
    
    // The buttons that are part of the file toolbar
    Button newDraftButton;
    
    // Holds the contents of the current screen that the user selects 
    ScrollPane workspaceScrollPane;
    
    DraftModelDataManager dataManager;
    
    // Holds all the options of screens to select from
    FlowPane screenToolbarPane;
    
    // The buttons that are part of the screen toolbar
    Button fantasyTeamScreenButton;
    Button playerScreenButton;
    Button fantasyStandingsScreenButton;
    Button draftScreenButton;
    Button MLBTeamsScreenButton;  
    
    // Holds all the options of file management such as save draft, load draft
    FlowPane fileToolbarPane;
    
    // The top and bottom workspace will always be a toolbar, it is only the
    // center workspace that changes. That's why a BorderPane makes the most sense 
    BorderPane wdkPane;
    
    Scene primaryScene;
    Stage primaryStage;
    
    // Keep a list of all the buttons to iterate through, so when we switch
    // screens we can seamlessly disable the button just pressed, and enable
    // the rest of the buttons 
    ArrayList<Button> screenButtonList;
    
    // The screens that are part of this application
    FantasyTeamScreen fantasyTeamScreen;
    PlayerScreen playerScreen;
    FantasyStandingsScreen fantasyStandingsScreen;
    DraftScreen draftScreen;
    MLBTeamsScreen MLBTeamsScreen;    
    
    // The screen contents to be displayed 
    Pane fantasyTeamScreenPane;
    Pane playerScreenPane;
    Pane fantasyStandingsScreenPane;
    Pane draftScreenPane;
    Pane MLBTeamsScreenPane;      
    
    String currentScreen;
    
    // HERE ARE OUR DIALOGS
    YesNoCancelDialog yesNoCancelDialog;
    
    public WDK_GUI(Stage initPrimaryStage) {
        
        primaryStage = initPrimaryStage;
    }
   
    public void initGUI(String windowTitle) {
        
        initDialogs();
        
        initScreens();            
        
        initScreenToolbar();  
        
        initFileToolbar();        
        
        initEventHandlers();
                
        initWindow(windowTitle);       
    }
    
    private void initDialogs() {
        
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }
    
    private void initWindow(String windowTitle) {
        
        workspaceScrollPane = new ScrollPane();  
        
        wdkPane = new BorderPane();
        
        // Add the file toolbar to the top of the screen
        wdkPane.setTop(fileToolbarPane);
        
        primaryScene = new Scene(wdkPane);
        
        // Set the window title
        primaryStage.setTitle(windowTitle);
        
        // Get the size of the screen
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // And use it to size the window
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        
        primaryStage.setScene(primaryScene);
        primaryStage.show();        
    }
    
    /**
     * When this method is called, the center workspace will be shown.
     * 
     * @param draftToReload This may be a brand new DraftModel, 
     *                      or a previously edited one.
     */
    public void reloadDraft(DraftModel draftToReload) {
        
        if (!workspaceActivated) {
            activateWorkspace();
        }
    }
    
    private void initScreens() {
        
        fantasyTeamScreen = new FantasyTeamScreen();
        playerScreen = new PlayerScreen(yesNoCancelDialog, dataManager);
        fantasyStandingsScreen = new FantasyStandingsScreen();
        draftScreen = new DraftScreen();  
        MLBTeamsScreen = new MLBTeamsScreen();
        
        playerScreen.initGUI();
        
        fantasyTeamScreenPane = fantasyTeamScreen.getPane();
        playerScreenPane = playerScreen.getPane();
        fantasyStandingsScreenPane = fantasyStandingsScreen.getPane();
        draftScreenPane = draftScreen.getPane();
        MLBTeamsScreenPane = MLBTeamsScreen.getPane(); 
    }
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        
        fileToolbarPane = new FlowPane();
        
        newDraftButton = initToolbarButton(fileToolbarPane, false, "New draft");
    }
    
    /**
     * This function initializes all the buttons in the toolbar at the bottom of
     * the application window. These are related to screen selection.
     */
    private void initScreenToolbar() {
        
        screenToolbarPane = new FlowPane();
        
        screenButtonList = new ArrayList<Button>();
        
        /* The screen buttons are only enabled when a new draft is selected
          or a draft is loaded. So they are disabled initially */
        fantasyTeamScreenButton = initToolbarButton(screenToolbarPane, true, "Fantasy Team Screen");
        playerScreenButton = initToolbarButton(screenToolbarPane, true, "Player Screen");
        fantasyStandingsScreenButton = initToolbarButton(screenToolbarPane, true, "Fantasy Standings Screen");
        draftScreenButton = initToolbarButton(screenToolbarPane, true, "Draft Screen");
        MLBTeamsScreenButton = initToolbarButton(screenToolbarPane, true, "MLB Teams Screen");        
        
    }  
    
    private Button initToolbarButton(Pane toolbar, boolean disabled, String text) {
        
        Button screenButton = new Button();
        screenButton.setLayoutX(100);
        screenButton.setLayoutY(80);
        screenButton.setText(text);
        screenButton.setDisable(disabled);
        
        // Add the button to the toolbar
        toolbar.getChildren().add(screenButton);
        
        // Add it to the list of buttons
        screenButtonList.add(screenButton);
        
        return screenButton;
    }
    
    private void initEventHandlers() {
        
        // First the file controls
        fileController = new FileController(yesNoCancelDialog);
        
        newDraftButton.setOnAction(e -> 
            fileController.handleNewDraftModelRequest(this)
        );
        
        /* Now the screen controls */
        
        /* When a screen toolbar button is pressed, it changes the center workspace to a
        new screen */
        fantasyTeamScreenButton.setOnAction(e -> 
                changeScreen(fantasyTeamScreenPane, fantasyTeamScreenButton)
        ); 
        playerScreenButton.setOnAction(e -> 
                changeScreen(playerScreenPane, playerScreenButton)
        );
        fantasyStandingsScreenButton.setOnAction(e -> 
                changeScreen(fantasyStandingsScreenPane, fantasyStandingsScreenButton)
        );
        draftScreenButton.setOnAction(e -> 
                changeScreen(draftScreenPane, draftScreenButton)
        );
        MLBTeamsScreenButton.setOnAction(e -> 
                changeScreen(MLBTeamsScreenPane, MLBTeamsScreenButton)
        );
    }
    
    public void activateWorkspace() {
        
        // Put the workspace in the GUI
        if (!workspaceActivated) {
            
            // Show the screen toolbar 
            wdkPane.setBottom(screenToolbarPane);
            
            // The screen that shows at the start is the fantasy standings screen
            wdkPane.setCenter(fantasyStandingsScreenPane); 
            
            // Set the workspace inside a scroll pane        
            workspaceScrollPane.setContent(wdkPane);
            
            workspaceActivated = true;
            
            // Activate the screen buttons
            enableAllScreenButtons();
        }
    }
    
    /**
     * Change to the screen specified, and disable the button that was pressed
     * to change to that screen
     * 
     * @param screen The screen that will appear in the center workspace
     * @param screenButton The button pressed that brought us to the new screen
     */
    private void changeScreen(Pane screen, Button screenButton) {
        
        updateScreenToolbar(screenButton);
        wdkPane.setCenter(screen);        
        workspaceScrollPane.setContent(wdkPane);
    }
    
    /**
     * Enable all the buttons of the screen toolbar except the one specified
     * to avoid reopening the screen that is already opened
     * 
     * @param screenButton The only button that is not enabled 
     */
    private void updateScreenToolbar(Button screenButton) {
        
        screenButton.setDisable(true);
        for (Button button : screenButtonList) {
            if (button != screenButton) {
                button.setDisable(false);
            }
        }                
    }
    
    /**
     * Enable every button of the screen toolbar
     */
    private void enableAllScreenButtons() {
        
        for (Button button : screenButtonList) {
                button.setDisable(false);
        }
        
    }

    public void setDataManager(DraftModelDataManager dataManager) {
        this.dataManager = dataManager;
    }
    
    public DraftModelDataManager getDataManager() {
        return dataManager;
    }

    @Override
    public void reloadDraftModel(DraftModel draftModelToReload) {
         if (!workspaceActivated) {
            activateWorkspace();
         }
    }
}

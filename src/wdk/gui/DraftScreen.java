/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author Kevin
 */
public class DraftScreen {
    
     private GridPane draftScreenPane;
    
    public DraftScreen() {
       draftScreenPane = new GridPane();
       Label l1 = new Label("On draft screen!");
       draftScreenPane.getChildren().add(l1);
    }

    public Pane getPane() {
        return draftScreenPane;
    }
    
}

package properties_manager;

import java.util.ArrayList;
import java.util.HashMap;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import xml_utilities.InvalidXMLFileFormatException;
import xml_utilities.XMLUtilities;

/**
 * This class is used for loading properties from XML files that can
 * then be used throughout an application. Note that this class is
 * a singleton, and so can be accessed from anywhere. To get the
 * singleton properties manager, just use the static accessor:
 * 
 * PropertiesManager props = PropertiesManager.getPropertiesManager();
 * 
 * Now you can access all the properties it currently stores using
 * the getProperty method. Note that the properties_schema.xsd file
 * specifies how these files are to be constructed.
 * 
 * @author Kevin
 */
public class PropertiesManager
{    
    // This class is a singleton, and here is the only object
    private static PropertiesManager singleton = null;

    // We'll store properties here
    private HashMap<String, String> properties;
    
    // Lists of property options can be stored here
    private HashMap<String, ArrayList<String>> propertyOptionsLists;
    
    // This will load the xml for us
    private XMLUtilities xmlUtil;
    
    // These constants are used for loading properties as they are
    // the essential elements and attributes
    public static final String PROPERTY_ELEMENT                 = "property";
    public static final String PROPERTY_LIST_ELEMENT            = "property_list";
    public static final String PROPERTY_OPTIONS_LIST_ELEMENT    = "property_options_list";
    public static final String PROPERTY_OPTIONS_ELEMENT         = "property_options";    
    public static final String OPTION_ELEMENT                   = "option";
    public static final String NAME_ATT                         = "name";
    public static final String VALUE_ATT                        = "value";
    public static final String DATA_PATH_PROPERTY               = "DATA_PATH";

    /**
     * The constructor is private because this is a singleton.
     */
    private PropertiesManager() 
    {
        properties = new HashMap();
        propertyOptionsLists = new HashMap();
        xmlUtil = new XMLUtilities();
    }

    /**
     * This is the static accessor for the singleton.
     * 
     * @return The singleton properties manager object.
     */
    public static PropertiesManager getPropertiesManager()
    {
        // If it's never been retrieved before then
        // first we must construct it
        if (singleton == null) {
            singleton = new PropertiesManager();
        }
        
        return singleton;
    }

    /**
     * This function adds the (property, value) tuple to the 
     * properties manager.
     * 
     * @param property Key, i.e. property type for this pair.
     * 
     * @param value The data for this pair.
     */
    public void addProperty(Object property, String value)
    {
        String propName = property.toString();
        properties.put(propName, value);
    }
 
    /**
     * Accessor method for getting a property from this manager.
     * 
     * @param propType The key for getting a property.
     * 
     * @return The value associated with the key.
     */
    public String getProperty(Object propType)
    {
        String propName = propType.toString();
        return properties.get(propName);
    }

    /**
     * Accessor method for getting a property options list associated
     * with the property key.
     * 
     * @param property The key for accessing the property options list.
     * 
     * @return The property options list associated with the key.
     */
    public ArrayList<String> getPropertyOptionsList(Object property)
    {
        String propName = property.toString();
        return propertyOptionsLists.get(propName);
    }

    /**
     * This function loads the xmlDataFile in this property manager, first
     * make sure it's a well formed document according to the rules specified
     * in the xmlSchemaFile.
     * 
     * @param xmlDataFile XML document to load.
     * 
     * @param xmlSchemaFile Schema that the XML document should conform to.
     * 
     * @throws InvalidXMLFileFormatException This is thrown if the XML file
     * is invalid.
     */
    public void loadProperties(String xmlDataFile, String xmlSchemaFile)
            throws InvalidXMLFileFormatException
    {

        String dataPath = getProperty(DATA_PATH_PROPERTY);
 
        // Add the data path
        xmlDataFile = dataPath + xmlDataFile;
        xmlSchemaFile = dataPath + xmlSchemaFile;
        
        // First load the file
        Document doc = xmlUtil.loadXMLDocument(xmlDataFile, xmlSchemaFile);
        
        // Now load all the properties
        Node propertyListNode = xmlUtil.getNodeWithName(doc, PROPERTY_LIST_ELEMENT);
        ArrayList<Node> propNodes = xmlUtil.getChildNodesWithName(propertyListNode, PROPERTY_ELEMENT);
        for(Node n : propNodes)
        {
            NamedNodeMap attributes = n.getAttributes();
            for (int i = 0; i < attributes.getLength(); i++)
            {
                Node att = attributes.getNamedItem(NAME_ATT);
                String attName = attributes.getNamedItem(NAME_ATT).getTextContent();
                String attValue = attributes.getNamedItem(VALUE_ATT).getTextContent();
                properties.put(attName, attValue);
            }
        }
        
        // And the properties from option lists
        Node propertyOptionsListNode = xmlUtil.getNodeWithName(doc, PROPERTY_OPTIONS_LIST_ELEMENT);
        if (propertyOptionsListNode != null)
        {
            ArrayList<Node> propertyOptionsNodes = xmlUtil.getChildNodesWithName(propertyOptionsListNode, PROPERTY_OPTIONS_ELEMENT);
            for (Node n : propertyOptionsNodes)
            {
                NamedNodeMap attributes = n.getAttributes();
                String name = attributes.getNamedItem(NAME_ATT).getNodeValue();
                ArrayList<String> options = new ArrayList();
                propertyOptionsLists.put(name, options);
                ArrayList<Node> optionsNodes = xmlUtil.getChildNodesWithName(n, OPTION_ELEMENT);
                for (Node oNode : optionsNodes)
                {
                    String option = oNode.getTextContent();
                    options.add(option);
                }
            }
        }
    }
}